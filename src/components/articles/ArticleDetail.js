import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {ButtonReturn} from '../buttons';

import {htmlToJSX} from '../../helpers/htmlToJSX';

import Styles from './styles/Styles-ArticleDetail';

const ArticleDetail = (props) => {
  const items = props.article.raw;

  return (
    <View style={Styles.container}>
      <View style={Styles.header}>
        <ButtonReturn text={props.btnRetText} onPress={() => props.onBack()} />
      </View>
      <View style={Styles.body}>
        <Text style={Styles.title}>{props.article.title}</Text>
        <SafeAreaView style={Styles.bodyContainer}>
          <FlatList
            data={items}
            //renderItem={({ item }) => <Item item={item} />}
            renderItem={({item}) => htmlToJSX(item)}
            // keyExtractor={(item) => item.id}
            keyExtractor={(item, index) => index.toString()}
          />
        </SafeAreaView>
      </View>
    </View>
  );
};

export default ArticleDetail;
