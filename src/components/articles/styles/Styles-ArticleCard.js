import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics, AppStyles} from '../../../styles/index';

const MARGIN_VERTICAL = Metrics.Height * 6;

const CARD_WIDTH = Metrics.Width * 330;
const CARD_LEFT_WIDTH = Metrics.Width * 230;
const CARD_RIGHT_WIDTH = Metrics.Width * 100;
const CARD_FOOTER_HEIGHT = Metrics.Height * 28;
const CARD_TEXT_WIDTH = Metrics.Width * 210;

const TEXT_MARGIN_TOP = Metrics.Height * 15;
const TEXT_MARGIN_BETWEEN = Metrics.Width * 7;
const TEXT_MARGIN_LEFT = Metrics.Width * 20;

const IMAGE_HEIGHT = Metrics.Width * 80;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,
    paddingVertical: MARGIN_VERTICAL,
  },

  card: {
    justifyContent: 'center',
    alignItems: 'center',

    width: CARD_WIDTH,

    backgroundColor: '#FFF',
    // ...AppStyles.borderRadius,
    // borderWidth: Platform.OS === 'ios' ? 0 : 1,
    borderRadius: 10,
    borderColor: Platform.OS === 'ios' ? Colors.grayLighter : '#E0E0E0',
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    elevation: Platform.OS === 'ios' ? 0 : 5,
  },

  cardTop: {
    flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',

    minHeight: Metrics.Height * 100,

    width: CARD_WIDTH,
  },

  cardLeft: {
    // justifyContent: 'center',
    // alignItems: 'center',

    // backgroundColor: 'yellow',
    width: CARD_LEFT_WIDTH,
  },

  cardRight: {
    justifyContent: 'center',
    alignItems: 'center',

    width: CARD_RIGHT_WIDTH,
  },

  cardFooter: {
    flexDirection: 'row',
    alignItems: 'center',

    width: CARD_WIDTH,
    height: CARD_FOOTER_HEIGHT,
    // marginBottom: TEXT_MARGIN_BETWEEN,
    // backgroundColor: 'blue',
  },

  cardTitle: {
    // backgroundColor: 'red',
    marginTop: TEXT_MARGIN_TOP,
    marginLeft: TEXT_MARGIN_TOP,
  },

  title: {
    ...Fonts.style.blogH3,
    width: CARD_TEXT_WIDTH,
    fontWeight: 'bold',
    color: '#000',
  },

  description: {
    ...Fonts.style.normal,
    width: CARD_TEXT_WIDTH,
    color: Colors.charcoal,
    marginTop: TEXT_MARGIN_BETWEEN,
    marginLeft: TEXT_MARGIN_LEFT,
  },

  image: {
    height: IMAGE_HEIGHT,
    width: IMAGE_HEIGHT,

    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#F0F0F0',
  },

  footerView: {
    flexDirection: 'row',
    alignItems: 'center',
    height: CARD_FOOTER_HEIGHT,
    width: CARD_LEFT_WIDTH,
  },

  footer: {
    height: CARD_FOOTER_HEIGHT,
    width: CARD_WIDTH,
  },

  iconsSet: {
    height: CARD_FOOTER_HEIGHT,
    width: CARD_RIGHT_WIDTH,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: TEXT_MARGIN_BETWEEN,
  },

  iconView: {
    width: Metrics.Width * 20,
    height: Metrics.Width * 20,
  },

  footerText: {
    ...Fonts.style.normal,
    marginLeft: TEXT_MARGIN_LEFT,
    color: Colors.footerText,
  },
});
