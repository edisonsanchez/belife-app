import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {ButtonReturn} from '../../components/buttons';
import {Images, Metrics} from '../../styles';
import {htmlToJsx} from '../../helpers/htmlToJsx';
import * as rssParser from 'react-native-rss-parser';
import {parse} from 'himalaya';

import Styles from './styles/Styles-ArticleNew';
import {object} from 'prop-types';

const ArticleNew = (props) => {
  const items = props.article.description;

  return (
    <View>
      <ButtonReturn text={props.btnRetText} onPress={() => props.onBack()} />
      <Text style={Styles.title}>{props.article.title}</Text>
      <SafeAreaView style={Styles.container}>
        <FlatList
          data={items}
          //renderItem={({ item }) => <Item item={item} />}
          renderItem={({item}) => htmlToJsx(item)}
          keyExtractor={(item) => item.id}
        />
      </SafeAreaView>
    </View>
  );
};

export default ArticleNew;
