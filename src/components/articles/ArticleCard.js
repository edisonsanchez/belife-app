import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import {Images, Metrics, Colors} from '../../styles';

import Styles from './styles/Styles-ArticleCard';
import {object} from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome';

const ArticleCard = (props) => {
  return (
    <View style={[Styles.container, props.styleContainer]}>
      <View style={[Styles.card, props.styleContainer]}>
        <TouchableOpacity
          style={[Styles.cardTop, props.styleBodyContainer]}
          onPress={props.onPress}>
          <View style={[Styles.cardLeft, props.styleLeftContainer]}>
            <View style={[Styles.cardTitle, props.styleLeftContainer]}>
              <Text style={[Styles.title]}>{props.title}</Text>
            </View>

            <Text style={[Styles.description]}>{props.description}</Text>
          </View>
          <View style={[Styles.cardRight, props.styleLeftContainer]}>
            <Image source={{uri: props.image}} style={Styles.image} />
          </View>
        </TouchableOpacity>
        <View style={[Styles.cardFooter, props.styleFooter]}>
          <View style={[Styles.footerView, props.styleFooter]}>
            <Text style={[Styles.footerText, props.styleFooter]}>
              {props.date}
            </Text>
          </View>
          <View style={[Styles.iconsSet, props.styleFooter]}>
            <View style={[Styles.iconView, props.styleFooter]}>
              <Icon
                name={'share-alt'}
                size={18}
                color={Colors.footerText}
                style={[Styles.icon, props.styleFooter]}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ArticleCard;
