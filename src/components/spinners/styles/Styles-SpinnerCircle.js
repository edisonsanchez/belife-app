import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors, Metrics } from "../../../styles/index";

//
export default StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",

    //height: Metrics.Height * 55,
    width: Metrics.ScreenWidth,
    marginBottom: Metrics.Height * 16,
  },

  title: {
    ...Fonts.style.blogH1,
    width: Metrics.ScreenWidth,
    textAlign: "center",
    fontWeight: "bold",
    marginTop: Metrics.Height * 10,
  },

  itemImage: {
    height: Metrics.Height * 270,
    width: Metrics.ScreenWidth,
  },

  itemBr: {
    height: Metrics.Height * 1,
    width: Metrics.ScreenWidth,
  },

  itemH2: {
    ...Fonts.style.blogH2,
    paddingHorizontal: 5,
  },

  itemH3: {
    ...Fonts.style.blogH3,
    paddingHorizontal: 5,
  },

  itemH4: {
    ...Fonts.style.blogH4,
    paddingHorizontal: 5,
  },

  itemText: {
    ...Fonts.style.normal,
    paddingHorizontal: 5,
  },

  icon: {
    marginHorizontal: Metrics.Width * 10,
  },
});
