import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const CONTAINER_HEIGHT = Metrics.Height * 20;
//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,
    height: CONTAINER_HEIGHT,
  },

  separatorContainer: {
    width: Metrics.Width * 125,
  },

  separator: {
    width: Metrics.Width * 125,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
  },

  text: {
    ...Fonts.style.text,
    textAlign: 'center',
    width: Metrics.Width * 60,
    color: Colors.text,
    fontSize: Fonts.size.regular,
  },
});
