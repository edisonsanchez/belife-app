import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {Images, Metrics} from '../../styles';

import Styles from './styles/Styles-DividerText';
import {object} from 'prop-types';

const DividerText = (props) => {
  return (
    <View style={[Styles.container, props.styleContainer]}>
      <View style={[Styles.separatorContainer]}>
        <View style={[Styles.separator]} />
      </View>

      <Text style={[Styles.text, props.styleText]}>{props.text}</Text>

      <View style={[Styles.separatorContainer]}>
        <View style={[Styles.separator]} />
      </View>
    </View>
  );
};

export default DividerText;
