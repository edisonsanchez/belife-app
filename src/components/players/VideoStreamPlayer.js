import React, {Component, useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {ButtonReturn} from '../buttons';
import {Images, Metrics} from '../../styles';
import {htmlToJsx} from '../../helpers/htmlToJsx';
import * as rssParser from 'react-native-rss-parser';
import {parse} from 'himalaya';

import Styles from './styles/Styles-AudioStreamPlayer';
import {object} from 'prop-types';
import Player from 'react-native-streaming-audio-player';
import {Button} from 'react-native-elements';

const URL = 'http://lacavewebradio.chickenkiller.com:8000/stream.mp3';

export default class AudioStreamPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {currentTime: 0};
    // this.onUpdatePosition = this.onUpdatePosition.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <Video
          source={{uri: URL}} // Can be a URL or a local file.
          ref={(ref) => {
            this.player = ref;
          }} // Store reference
          onBuffer={this.onBuffer} // Callback when remote video is buffering
          onError={this.videoError} // Callback when video cannot be loaded
          style={styles.backgroundVideo}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
