import React, { Component, useState } from "react";
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import { ButtonReturn } from "../buttons";
import { Images, Metrics } from "../../styles";
import { htmlToJsx } from "../../helpers/htmlToJsx";
import * as rssParser from "react-native-rss-parser";
import { parse } from "himalaya";

import Styles from "./styles/Styles-AudioStreamPlayer";
import { object } from "prop-types";
import Player from "react-native-streaming-audio-player";
import { Button } from "react-native-elements";

const URL = "http://lacavewebradio.chickenkiller.com:8000/stream.mp3";

export default class AudioStreamPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = { currentTime: 0 };
    // this.onUpdatePosition = this.onUpdatePosition.bind(this);
  }

  onPlay() {
    Player.play(URL, {
      title: "title",
      artist: "Artists",
      album_art_uri:
        "http://www.no-daze-off.com/wp-content/uploads/2013/06/nodazeoff-song-artwork-for-bangobnixiousrockband.jpg",
    });
  }

  onPause() {
    Player.pause();
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            alignSelf: "stretch",
            justifyContent: "space-around",
          }}
        >
          <Button title="Play" onPress={() => this.onPlay()} color="red" />
          <Button title="Pause" onPress={() => this.onPause()} color="red" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
