import * as React from 'react';
import {Image} from 'react-native';
import {Images} from '../../styles';

import Styles from './styles';

const Logo = (props) => {
  return <Image style={Styles.logo} source={Images.logo} />;
};

export default Logo;
