import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics} from '../../styles';

//
export default StyleSheet.create({
  container: {
    width: Metrics.Height * 150,
    height: Metrics.Height * 150,
  },

  logo: {
    width: Metrics.Height * 150,
    height: Metrics.Height * 150,
  },
});
