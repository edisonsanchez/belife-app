import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {Images, Metrics} from '../../styles';
import Styles from './styles/Styles-FormSignup';

import {Logo} from '../../components/logo';
import {InputIcon} from '../../components/inputs';
import {ButtonIcon, ButtonSignup} from '../../components/buttons';
import {LoadingScreen} from '../../containers/loadings';

import {useInputValue} from '../../hooks/useInputValue';

//
const handleSignup = ({onSubmit}) => {
  onSubmit();
};

//
const handleCancel = ({onCancel}) => {
  onCancel();
};

const FormSignup = ({onSubmit, title}) => {
  const [loading, setLoading] = useState(true);
  const [processing, setProcessing] = useState(false);

  const email = useInputValue('');
  const confirm_email = useInputValue('');
  const password = useInputValue('');

  useEffect(function () {
    setLoading(false);
  }, []);

  return (
    <View>
      <Text>{title}</Text>
      <InputIcon icon="user" placeholder="Correo Electronico" {...email} />
      <InputIcon
        icon="user"
        placeholder="Confirmar Correo"
        {...confirm_email}
      />
      <InputIcon icon="lock" placeholder="Contraseña" {...password} />
      <ButtonIcon
        icon="login"
        text="Acceder"
        onPress={() => {
          handleSignup(onSubmit);
        }}
        disabled={!processing}
        isLoading={loading}
        loadingText="Registrarse"
      />
      <ButtonSignup
        styleContainer={Styles.signupButton}
        disclaimer="Si ya tiene una cuenta, Ingrese ahora..."
        text="ACCEDER A BE.LIFE"
        disabled={!processing}
        onPress={() => {
          handleCancel();
        }}
      />
    </View>
  );
};

export default FormSignup;
