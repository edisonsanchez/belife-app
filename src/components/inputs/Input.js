import React, {useState} from 'react';
import {View, TextInput, Platform} from 'react-native';
import {Colors} from '../../styles';

import Styles from './styles/Styles-Input';

const Input = (props) => {
  return (
    <View style={[Styles.container, props.styleContainer]}>
      <View style={[Styles.box, props.styleBox]}>
        <TextInput
          props
          style={[Styles.input, props.styleInput]}
          autoCapitalize="none"
          secureTextEntry={
            props.password
              ? true
              : props.secureTextEntry
              ? props.secureTextEntry
              : false
          }
          autoCompleteType={
            props.password
              ? 'password'
              : props.email
              ? 'email'
              : props.autoCompleteType
              ? props.autoCompleteType
              : 'off'
          }
          autoCorrect={
            props.password || props.email
              ? false
              : props.autoCorrect
              ? props.autoCorrect
              : false
          }
          keyboardType={
            props.password
              ? Platform.OS === 'android'
                ? 'visible-password'
                : 'default'
              : props.email
              ? 'email-address'
              : props.autoCompleteType
              ? props.keyboardType
              : 'default'
          }
          placeholder={props.placeholder}
          placeholderTextColor={
            props.placeholderTextColor
              ? props.placeholderTextColor
              : Colors.placeholder
          }
          onChangeText={props.onChangeText}
          value={props.value}
        />
      </View>
    </View>
  );
};

export default Input;
