import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const INPUT_HEIGHT = Metrics.Height * 40;
const INPUT_WIDTH = Metrics.Width * 310;
const INPUT_PADDING = Metrics.Width * 15;
const INPUT_MARGIN = Metrics.Height * 11;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,

    marginBottom: INPUT_MARGIN,
  },

  box: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    height: INPUT_HEIGHT,
    width: INPUT_WIDTH,

    backgroundColor: '#FFFFFF',
    borderColor: Colors.border,
    borderWidth: 1,
  },

  iconView: {
    justifyContent: 'center',
    alignItems: 'center',

    height: INPUT_HEIGHT,
    width: INPUT_HEIGHT,
  },

  input: {
    ...Fonts.style.input,

    height: INPUT_HEIGHT,
    width: INPUT_WIDTH - INPUT_HEIGHT,
    paddingLeft: INPUT_PADDING,
  },

  icon: {
    marginHorizontal: Metrics.Width * 10,
  },
});
