import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const INPUT_HEIGHT = Metrics.Height * 40;
const INPUT_WIDTH = Metrics.Width * 320;
const INPUT_PADDING = Metrics.Width * 15;
const INPUT_MARGIN = Metrics.Height * 10;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,

    marginBottom: INPUT_MARGIN,
  },

  box: {
    justifyContent: 'center',
    alignItems: 'center',

    height: INPUT_HEIGHT,
    width: INPUT_WIDTH,

    borderRadius: Metrics.InputRadius,
    borderColor: Colors.border,
    borderWidth: 1,

    backgroundColor: Colors.inputBackground,
  },

  input: {
    ...Fonts.style.input,

    height: INPUT_HEIGHT,
    width: INPUT_WIDTH,
    paddingLeft: INPUT_PADDING,
  },
});
