import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button} from 'react-native-elements';

import Styles from './styles/Styles-ButtonReturn';
import {Colors} from '../../styles';

const ButtonReturn = (props) => {
  return (
    <Button
      containerStyle={Styles.container}
      buttonStyle={[
        Styles.button,
        {backgroundColor: props.color ? props.color : Colors.backgroundColor},
      ]}
      iconContainerStyle={Styles.icon}
      titleStyle={Styles.text}
      title={props.text}
      icon={
        <Icon
          name="arrow-left"
          size={24}
          color={props.iconColor ? props.iconColor : Colors.text}
          styles={Styles.icon}
        />
      }
      onPress={props.onPress}
    />
  );
};

export default ButtonReturn;
