import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import Styles from './styles/Styles-Link';

const Link = (props) => {
  return props.isLoading ? (
    <View style={Styles.container}>
      <Text style={[Styles.loadingText]}>
        {props.loadingText ? props.loadingText : 'Cargando...'}
      </Text>
    </View>
  ) : (
    <View style={[Styles.container, props.styleContainer]}>
      <TouchableOpacity
        style={[Styles.button, props.styleButton]}
        onPress={props.onPress}>
        <Text style={[Styles.text, props.styleText]}>{props.text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Link;
