import React, { useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Styles from "./styles/Styles-ButtonFacebook";

const ButtonFacebook = (props) => {
  const [count, setCount] = useState(0);
  const onPress = () => setCount((prevCount) => prevCount + 1);

  return (
    <View style={Styles.container}>
      <TouchableOpacity style={Styles.button} onPress={props.onPress}>
        <View
          style={[
            Styles.viewIcon,
            props.facebook
              ? Styles.viewIconFacebook
              : props.google
              ? Styles.viewIconGoogle
              : Styles.viewIcon,
          ]}
        >
          <Icon
            name={
              props.facebook
                ? "facebook"
                : props.google
                ? "google"
                : "instagram"
            }
            color={"#FFF"}
            size={22}
          />
        </View>
        <View
          style={[
            Styles.viewText,
            props.facebook
              ? Styles.viewTextFacebook
              : props.google
              ? Styles.viewTextGoogle
              : Styles.viewText,
          ]}
        >
          <Text style={Styles.text}>{props.text}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ButtonFacebook;
