import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Styles from './styles/Styles-ButtonHeaderIcon';
import {Colors} from '../../styles';

const ButtonHeaderIcon = (props) => {
  return props.isLoading ? (
    <View style={Styles.container}>
      <Text style={[Styles.loadingText]}>
        {props.loadingText ? props.loadingText : ''}
      </Text>
    </View>
  ) : (
    <TouchableOpacity
      style={[Styles.button, props.styleButton]}
      onPress={props.onPress}>
      <Icon
        name={props.icon}
        size={28}
        color={props.disabled ? Colors.background : '#6D6D6D'}
      />
    </TouchableOpacity>
  );
};

export default ButtonHeaderIcon;
