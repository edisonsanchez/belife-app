import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const BUTTON_HEIGHT = Metrics.Height * 30;
const BUTTON_WIDTH = Metrics.Width * 280;
const BUTTON_MARGIN = Metrics.Height * 20;
const TEXT_WIDTH = Metrics.Width * 210;
const ICON_WIDTH = Metrics.Width * 20;
const ICON_MARGIN = Metrics.Width * 10;

const BUTTON_FACEBOOK_HEIGHT = Metrics.Height * 22;
const BUTTON_GOOGLE_HEIGHT = Metrics.Height * 24;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',

    height: BUTTON_HEIGHT,
    width: BUTTON_WIDTH,
  },

  iconContainer: {
    height: BUTTON_HEIGHT,
    width: BUTTON_HEIGHT,

    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  iconView: {
    borderRadius: BUTTON_GOOGLE_HEIGHT / 2,
    height: BUTTON_GOOGLE_HEIGHT,
    width: BUTTON_GOOGLE_HEIGHT,

    justifyContent: 'center',
    alignItems: 'center',
  },

  iconViewFacebook: {
    flexDirection: 'column',
    borderRadius: BUTTON_FACEBOOK_HEIGHT / 2,
    height: BUTTON_FACEBOOK_HEIGHT,
    width: BUTTON_FACEBOOK_HEIGHT,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: Colors.facebook,
  },

  icon: {},

  text: {
    ...Fonts.style.button,
    color: Colors.facebook,
    width: TEXT_WIDTH,
    marginLeft: ICON_MARGIN,
  },

  disabledButton: {
    // backgroundColor: Colors.buttonDisabled,
  },

  loadingText: {
    ...Fonts.style.button,
    color: Colors.textDark,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },

  textFacebook: {
    color: Colors.facebook,
    fontWeight: 'bold',
  },

  textGoogle: {
    color: Colors.google,
    fontWeight: 'bold',
  },

  default: {},
});
