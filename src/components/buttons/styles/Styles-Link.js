import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const BUTTON_MARGIN = Metrics.Height * 5;

//
export default StyleSheet.create({
  container: {
    //width: Metrics.ScreenWidth,
  },

  button: {
    paddingVertical: Metrics.Height * 5,
  },

  text: {
    ...Fonts.style.link,
  },

  press: {
    //fontWeight: 'bold',
    color: Colors.link,
    fontSize: Fonts.size.regular,
  },

  disabledText: {
    ...Fonts.style.regular,
  },

  loadingText: {
    ...Fonts.style.regular,
    color: Colors.link,
  },

  default: {},
});
