import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.Height * 55,
    width: Metrics.Height * 55,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.Height * 49,
    width: Metrics.Width * 49,
  },

  icon: {},

  disabledButton: {
    backgroundColor: Colors.background,
  },

  default: {},
});
