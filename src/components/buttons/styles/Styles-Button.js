import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const BUTTON_HEIGHT = Metrics.Height * 40;
const BUTTON_WIDTH = Metrics.Width * 320;
const BUTTON_MARGIN = Metrics.Height * 10;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',

    height: BUTTON_HEIGHT,
    width: BUTTON_WIDTH,

    borderColor: Colors.border,
    borderWidth: 1,
    borderRadius: Metrics.Width * 5,

    backgroundColor: Colors.button,
  },

  text: {
    ...Fonts.style.button,
  },

  disabledButton: {
    backgroundColor: Colors.buttonDisabled,
  },

  loadingText: {
    ...Fonts.style.button,
    color: Colors.textDark,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },

  default: {},
});
