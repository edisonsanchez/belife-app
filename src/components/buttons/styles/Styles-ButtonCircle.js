import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const IMAGE_SIZE = Metrics.Width * 40;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.Height * 72,
    width: Metrics.Width * 80,
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.Height * 72,
    width: Metrics.Width * 80,
  },

  image: {
    height: IMAGE_SIZE,
    width: IMAGE_SIZE,

    borderRadius: IMAGE_SIZE / 2,
    resizeMode: 'stretch',
  },

  text: {
    color: '#6D6D6D',
    fontSize: 12,
  },

  textSelected: {
    fontWeight: 'bold',
    color: '#000',
    fontSize: 13,
  },
});
