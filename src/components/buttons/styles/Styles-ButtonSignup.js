import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.ScreenWidth,
  },

  viewDisclaimer: {
    justifyContent: 'center',
    alignItems: 'center',

    width: Metrics.Width * 340,
  },

  disclaimer: {
    width: Metrics.Width * 300,
    alignItems: 'center',
    textAlign: 'center',
    color: '#5d5c5f',
    paddingVertical: Metrics.Height * 5,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: Metrics.Height * 49,
    width: Metrics.Width * 340,

    borderColor: Colors.border,
    borderWidth: 2,
    borderRadius: Metrics.Width * 5,
  },

  viewIcon: {
    // backgroundColor: '#2f55a4',
    height: Metrics.Height * 49,
    width: Metrics.Width * 40,
    justifyContent: 'center',
    alignItems: 'center',
  },

  viewText: {
    height: Metrics.Height * 49,
    width: Metrics.Width * 300,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  text: {
    marginLeft: Metrics.Width * 12,
    fontWeight: 'bold',
    color: '#5d5c5f',
    fontSize: 18,
  },
});
