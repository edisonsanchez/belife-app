import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'flex-start',

    // height: Metrics.Height * 50,
    // width: Metrics.ScreenWidth,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: Metrics.Height * 40,
    //width: Metrics.Width * 200,

    // borderColor: Colors.border,
    // borderWidth: 1,
    // // borderRadius: Metrics.Width * 5,
    // borderTopRightRadius: Metrics.Width * 5,
    // borderBottomRightRadius: Metrics.Width * 5,
    paddingRight: Metrics.Width * 20,
  },

  icon: {
    marginHorizontal: 50,
  },

  text: {
    marginLeft: Metrics.Width * 18,
    fontWeight: 'bold',
    color: Colors.text,
  },
});
