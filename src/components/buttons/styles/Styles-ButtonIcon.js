import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.Height * 55,
    width: Metrics.ScreenWidth,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: Metrics.Height * 49,
    width: Metrics.Width * 340,

    borderColor: Colors.border,
    borderWidth: 1,
    borderRadius: Metrics.Width * 5,

    backgroundColor: '#AAAAAA',
  },

  icon: {
    marginHorizontal: 50,
  },

  text: {
    marginLeft: Metrics.Width * 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },

  disabledButton: {
    backgroundColor: '#FFFFFF',
  },

  disabledText: {
    color: '#AAAAAA',
  },

  loadingText: {
    ...Fonts.style.h5,
    color: '#AAAAAA',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },

  default: {},
});
