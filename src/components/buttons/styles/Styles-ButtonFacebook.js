import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.Height * 55,
    width: Metrics.ScreenWidth,
  },

  viewIcon: {
    backgroundColor: '#2f55a4',
    height: Metrics.Height * 49,
    width: Metrics.Width * 40,
    justifyContent: 'center',
    alignItems: 'center',

    borderTopLeftRadius: Metrics.Width * 5,
    borderBottomLeftRadius: Metrics.Width * 5,
  },

  viewText: {
    backgroundColor: '#3b5998',
    height: Metrics.Height * 49,
    width: Metrics.Width * 300,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    borderTopRightRadius: Metrics.Width * 5,
    borderBottomRightRadius: Metrics.Width * 5,
  },

  button: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: Metrics.Height * 49,

    borderColor: Colors.border,
    borderWidth: 1,
  },

  icon: {
    marginHorizontal: 50,
  },

  text: {
    marginLeft: Metrics.Width * 12,
    fontWeight: 'bold',
    color: '#FFF',
    fontSize: 18,
  },

  viewIconFacebook: {
    backgroundColor: '#2f55a4',
  },

  viewIconGoogle: {
    backgroundColor: '#ef3a25',
  },

  viewTextFacebook: {
    backgroundColor: '#3b5998',
  },

  viewTextGoogle: {
    backgroundColor: '#db4a39',
  },
});
