import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {Fonts, Colors, Metrics} from '../../styles';

import Styles from './styles/Styles-ButtonSocial';

const ButtonSocial = (props) => {
  return props.isLoading ? (
    <View style={Styles.container}>
      <Text style={[Styles.loadingText]}>
        {props.loadingText ? props.loadingText : 'Cargando...'}
      </Text>
    </View>
  ) : (
    <View style={[Styles.container, props.styleContainer, Styles.disabled]}>
      {props.disabled && (
        <View style={[Styles.button, props.styleButton]}>
          <View
            style={[
              Styles.iconView,
              props.styleIconView,
              props.facebook ? Styles.iconViewFacebook : Styles.default,
            ]}>
            <Icon
              name={
                props.facebook
                  ? 'facebook-f'
                  : props.google
                  ? 'google-plus-square'
                  : props.icon
              }
              size={props.iconSize ? props.iconSize : props.facebook ? 16 : 24}
              color={
                props.facebook
                  ? props.disabled
                    ? Colors.disabled
                    : '#FFF'
                  : props.google
                  ? props.disabled
                    ? Colors.disabled
                    : Colors.google
                  : props.disabled
                  ? Colors.disabled
                  : Colors.icon
              }
              styles={[Styles.icon, props.styleIcon]}
            />
          </View>
          <Text style={[Styles.text, props.styleText]}>{props.text}</Text>
        </View>
      )}
      {!props.disabled && (
        <TouchableOpacity
          props
          style={[Styles.button, props.styleButton]}
          onPress={props.onPress}>
          <View style={Styles.iconContainer}>
            <View
              style={[
                Styles.iconView,
                props.styleIconView,
                props.facebook ? Styles.iconViewFacebook : Styles.default,
              ]}>
              <Icon
                name={
                  props.facebook
                    ? 'facebook-f'
                    : props.google
                    ? 'google-plus-official'
                    : props.icon
                }
                size={
                  props.iconSize ? props.iconSize : props.facebook ? 16 : 26
                }
                color={
                  props.facebook
                    ? props.disabled
                      ? Colors.disabled
                      : '#FFF'
                    : props.google
                    ? props.disabled
                      ? Colors.disabled
                      : Colors.google
                    : props.disabled
                    ? Colors.disabled
                    : Colors.icon
                }
                styles={[Styles.icon, props.styleIcon]}
              />
            </View>
          </View>
          <Text
            style={[
              Styles.text,
              props.styleText,
              props.facebook
                ? Styles.textFacebook
                : props.google
                ? Styles.textGoogle
                : Styles.default,
            ]}>
            {props.text}
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default ButtonSocial;
