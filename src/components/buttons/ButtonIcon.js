import React, {useState} from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button} from 'react-native-elements';

import Styles from './styles/Styles-ButtonIcon';

const ButtonIcon = (props) => {
  return props.isLoading ? (
    <View style={Styles.container}>
      <Text style={[Styles.loadingText]}>
        {props.loadingText ? props.loadingText : 'Cargando...'}
      </Text>
    </View>
  ) : (
    <Button
      disabled={props.disabled}
      containerStyle={[Styles.container, props.containerStyle]}
      buttonStyle={[
        Styles.button,
        props.style,
        props.disabled ? Styles.disabledButton : Styles.default,
      ]}
      iconContainerStyle={[Styles.icon, props.iconStyle]}
      titleStyle={[
        Styles.text,
        props.disabled ? Styles.disabledText : Styles.default,
      ]}
      title={props.text}
      icon={
        <Icon
          name={props.icon}
          size={24}
          color={
            props.iconColor
              ? props.iconColor
              : props.disabled
              ? '#AAAAAA'
              : '#FFFFFF'
          }
          styles={Styles.icon}
        />
      }
      onPress={props.onPress}
    />
  );
};

export default ButtonIcon;
