import React, {useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Styles from './styles/Styles-ButtonSignup';

const ButtonSignup = (props) => {
  const [count, setCount] = useState(0);
  const onPress = () => setCount((prevCount) => prevCount + 1);

  return (
    <View style={[Styles.container, props.styleContainer]}>
      <View style={[Styles.viewDisclaimer]}>
        <Text style={[Styles.disclaimer]}>
          {props.disclaimer ? props.disclaimer : ''}
        </Text>
      </View>
      <TouchableOpacity style={Styles.button} onPress={props.onPress}>
        <View style={[Styles.viewIcon]} />
        <View style={[Styles.viewText]}>
          <Text style={Styles.text}>{props.text}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ButtonSignup;
