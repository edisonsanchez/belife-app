import React, {useState} from 'react';
import {Text, TouchableOpacity, View, Image} from 'react-native';

import Styles from './styles/Styles-ButtonCircle';

const ButtonCircle = (props) => {
  return (
    <View style={Styles.container}>
      <TouchableOpacity style={Styles.button} onPress={props.onPress}>
        <Image source={props.image} style={Styles.image} />
        <Text style={Styles.text}>{props.text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ButtonCircle;
