import React, {useState} from 'react';
import {Platform} from 'react-native';
import * as rssParser from 'react-native-rss-parser';
import {parse} from 'himalaya';
import {convertHtmlItem} from '../helpers/convertHtmlItem';
import moment from 'moment';

// const NEWS_RSS = 'https://belifebyca.blogspot.com/feeds/posts/default?alt=rss';

const NEWS_RSS =
  Platform.OS === 'ios'
    ? 'https://belifebyca.blogspot.com/feeds/posts/default?alt=rss'
    : 'http://belifebyca.blogspot.com/feeds/posts/default?alt=rss';

const STUDIES_RSS =
  Platform.OS === 'ios'
    ? 'https://estudiosbiblicos-belife.blogspot.com/feeds/posts/default?alt=rss'
    : 'http://estudiosbiblicos-belife.blogspot.com/feeds/posts/default?alt=rss';

moment.locale('es');

const getText = (htmlBody) => {
  let text = '';

  htmlBody.map(function (item, index, array) {
    const obj = convertHtmlItem(item);

    if (obj.type && obj.type === 'text') {
      if (text.length > 0) {
        text = text + ' ' + obj.data;
      } else {
        text = obj.data;
      }
    }
  });

  return text;
};

//
const fetchRSS = async (rssURL) => {
  // let err = {message: 'Exepcion'};
  try {
    var response = await fetch(rssURL);
    var data = await response.text();
    var responseData = await rssParser.parse(data);
  } catch (err) {
    return [
      {
        image: null,
        title: 'ERROR',
        description: [],
        raw: [],
        bodyText: err,
        published: null,
      },
    ];
  }

  var posts = responseData.items.map(function (item, index, array) {
    try {
      const image = parse(item.description)
        .filter((element) => element.tagName === 'img')[0]
        .attributes.filter((attribute) => attribute.key === 'src')[0]
        .value.replace('&amp;', '&');

      const description = parse(item.description);
      const published = moment(item.published).format('LLL');

      return {
        image: image,
        title: item.title,
        description: description,
        raw: description,
        bodyText: getText(description),
        published: published,
      };
    } catch (err) {
      return {
        image: null,
        title: 'ERROR',
        description: [],
        raw: [],
        bodyText: JSON.stringify(err),
        published: null,
      };
    }
  });

  return posts;
};

export const NewsRSS = async () => {
  return await fetchRSS(NEWS_RSS);
};

export const StudiesRSS = async () => {
  return await fetchRSS(STUDIES_RSS);
};
