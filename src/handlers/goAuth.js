import React, {useState} from 'react';
import {StackActions, NavigationActions} from 'react-navigation';

export const goAuth = async (props) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: 'AuthLoading',
        params: {},
      }),
    ],
  });

  const goLogin = NavigationActions.navigate({
    routeName: 'AuthLoading',
    params: {},
  });

  props.navigation.dispatch(resetAction);
  props.navigation.dispatch(goLogin);
};
