import AsyncStorage from '@react-native-community/async-storage';
import {Auth} from 'aws-amplify';

export const awsSignIn = async (username, password) => {
  let ret = {
    code: 'error',
    data: null,
    message: 'Error al registrarse.',
  };

  try {
    const user = await Auth.signIn(username.toLowerCase(), password);

    if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
      const {requiredAttributes} = user.challengeParam;
      //const {username, email, phone_number} = getInfoFromUserInput();
      const loggedUser = await Auth.completeNewPassword(
        user, // the Cognito User Object
        password, // the new password
      );
    }

    await AsyncStorage.setItem(
      'sessionToken',
      user.signInUserSession.accessToken.jwtToken,
    );

    return {
      code: 'success',
      data: user.signInUserSession.accessToken.jwtToken,
      message: 'Ingreso Exitoso',
    };
  } catch (err) {
    if (err.code === 'UserNotConfirmedException') {
      ret.code = err.code;
      ret.message = 'Usuario no ha confirmado su correo electronico.';

      // The error happens if the user didn't finish the confirmation step when signing up
      // In this case you need to resend the code and confirm the user
      // About how to resend the code and confirm the user, please check the signUp part
    } else if (err.code === 'PasswordResetRequiredException') {
      ret.code = err.code;
      ret.message = 'Debe Cambiar su contraseña actual.';
      // The error happens when the password is reset in the Cognito console
      // In this case you need to call forgotPassword to reset the password
      // Please check the Forgot Password part.
    } else if (err.code === 'NotAuthorizedException') {
      ret.code = err.code;
      ret.message = 'Contraseña incorrecta.';

      // The error happens when the incorrect password is provided
    } else if (err.code === 'UserNotFoundException') {
      ret.code = err.code;
      ret.message = 'Usuario o Correo Electronico incorrecto.';

      // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else {
      ret.message = 'Correo electronico no registrado.';
    }

    return ret;
  }
};
