import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';

const AppStyles = {
  screen: {
    container: {
      flex: 1,
      backgroundColor: Colors.background,
    },

    background: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
    },

    borderRadius: {
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#000000',
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 0,
      },
    },

    title: {
      color: Colors.app,
      height: Metrics.screenDesignHeight * 59,

      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      borderWidth: 0,
      borderColor: '#fff',
      justifyContent: 'center',
    },

    subtitle: {
      color: Colors.snow,
      //padding: Metrics.marginVerticalSmall,
      marginBottom: Metrics.marginVerticalSmall,
      marginHorizontal: Metrics.marginHorizontal,
    },

    text: {
      ...Fonts.style.normal,
      color: Colors.text,
      textAlign: 'justify',
    },

    icon: {
      width: Metrics.ScreenWidth * 0.1,
      height: Metrics.ScreenWidth * 0.1,
    },
  }
};

export default AppStyles;
