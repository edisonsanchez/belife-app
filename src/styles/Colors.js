const colors = {
  white: '#FFFFFF',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: 'white',
  ember: 'rgba(164, 0, 48, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(30, 30, 29, 0.95)',
  eggplant: '#251a34',
  grayLight: '#A9A9A9',
  grayLighter: '#D3D3D3',
  facebook: '#3B5998',
  // google: 'green',
  google: '#DD4B39',
  clear: 'rgba(0,0,0,0)',

  border: '#C4C4C4',
  banner: '#5E9FB4',

  text: '#808686',
  textLight: '#deeaea',
  textDark: '#757a7a',
  title: '#000000',
  subtitle: '#000000',

  footerText: '#404040',

  background: '#F4F4F4',

  iconBackground: '#5E9FB4',
  iconLight: '#6D6D6D',
  iconDark: '#5E9FB4',

  progressCircleOn: '#5E9FB4',
  progressCircleOff: '#ECECEC',

  transparent: 'rgba(0,0,0,0)',

  button: '#282828',
  buttonText: '#FFFFFF',
  buttonDisabled: '#898989',

  input: '#898989',
  placeholder: '#C4C4C4',
  inputDisabled: '#F0F0F0',
  inputBackground: '#FFFFFF',

  link: 'blue',

  app: '#5E9FB4',
  shadow: 'rgba(0, 0, 0, 0.25)',
  disabled: '#95C6D6',
  error: '#B11212',
};

export default colors;
