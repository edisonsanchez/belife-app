const images = {
  logo: require('../../assets/logos/logo.png'),
  language_en: require('../../assets/icons/language_en.png'),
  language_es: require('../../assets/icons/language_es.png'),
  language_pr: require('../../assets/icons/language_pr.png'),
  language_fr: require('../../assets/icons/language_fr.png'),
  category_feed: require('../../assets/icons/category-feed.png'),
  category_instagram: require('../../assets/icons/category-instagram.png'),
  category_youtube: require('../../assets/icons/category-youtube.png'),
  category_music: require('../../assets/icons/category-music.png'),
  category_facebook: require('../../assets/icons/category-facebook.png'),
  background: require('../../assets/backgrounds/background.png'),
};

export default images;
