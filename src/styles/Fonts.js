import {Dimensions, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');

const type = {
  base: 'Avenir-Book',
  bold: 'Avenir-Black',
};

const size = {
  title: 60,
  h1: 60,
  h2: 50,
  h3: 40,
  h4: 30,
  h5: 25,
  h6: 20,
  h7: 13,
  input: 16,
  button: 30,
  big: 22,
  medium: 19,
  regular: 16,
  small: 13,
  tiny: 10,
  subtitle: 20,
  textarea: 12,
  iconText: 12,
  text: 12,
};

const style = {
  title: {
    fontFamily: type.base,
    fontSize: size.title,
    fontWeight: 'bold',
  },

  subtitle: {
    fontFamily: type.base,
    fontSize: size.subtitle,
    color: '#000',
  },

  input: {
    fontFamily: type.base,
    fontSize: size.input,
    color: '#000',
  },

  link: {
    fontFamily: type.base,
    fontSize: size.small,
    color: '#007cee',
  },

  button: {
    fontFamily: type.base,
    fontSize: size.regular,
    color: '#FFF',
  },

  label: {
    fontFamily: type.base,
    fontSize: size.label,
    color: '#000',
  },

  text: {
    fontFamily: type.base,
    fontSize: size.text,
    color: '#000',
  },

  icon: {
    fontFamily: type.base,
    fontSize: size.iconText,
    color: '#000',
  },

  textarea: {
    fontFamily: type.base,
    fontSize: size.textarea,
    color: '#000',
  },

  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
    fontWeight: 'bold',
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },

  normal: {
    fontFamily: type.base,
    fontSize: size.text,
  },

  bold: {
    fontFamily: type.bold,
    fontSize: size.text,
  },

  blogH1: {
    fontSize: size.h1 / 2,
    fontWeight: 'bold',
    marginBottom: 6,
  },

  blogH2: {
    fontSize: size.h2 / 2,
    fontWeight: 'bold',
    marginBottom: 4,
  },

  blogH3: {
    fontSize: size.h3 / 2,
    fontWeight: 'bold',
    marginBottom: 2,
  },

  blogH4: {
    fontSize: size.h4 / 2,
    fontWeight: 'bold',
  },
};

export default {
  type,
  size,
  style,
};
