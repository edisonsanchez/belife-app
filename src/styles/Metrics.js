import {Dimensions, Platform, NativeModules} from 'react-native';
const {StatusBarManager} = NativeModules;

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('screen').width;
const SCREEN_HEIGHT = Dimensions.get('screen').height;

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0; //StatusBar.currentHeight;

const TABBAR_NORMAL_HEIGHT = 49;
//const TABBAR_COMPACT_HEIGHT = 29;

const BODY_HEIGHT = WINDOW_HEIGHT - STATUS_BAR_HEIGHT - TABBAR_NORMAL_HEIGHT;
const COMPONENT_RADIUS = 5;

const UNIT_HEIGHT = WINDOW_HEIGHT / 640;
const UNIT_WIDTH = WINDOW_WIDTH / 360;

// Used via Metrics.baseMargin
const metrics = {
  // With Android OS Bottom Bar
  Height: UNIT_HEIGHT,
  Width: UNIT_WIDTH,

  WindowHeight: WINDOW_HEIGHT,
  WindowWidth: WINDOW_WIDTH,

  ScreenHeight: SCREEN_HEIGHT,
  ScreenWidth: SCREEN_WIDTH,

  StatusBarHeight: STATUS_BAR_HEIGHT,
  TabBottomBarHeight: TABBAR_NORMAL_HEIGHT,

  BodyHeight: BODY_HEIGHT,
  BodyWidth: WINDOW_WIDTH,

  //Margin
  MarginStatusBar: STATUS_BAR_HEIGHT,

  //.Section
  SectionWidth: SCREEN_WIDTH / 2,
  SectionHeight: BODY_HEIGHT / 2,

  // Lines.
  HorizontalLineHeight: 1,

  //. Button's Defaults
  ButtonLargeWidth: BODY_HEIGHT - 20,
  ButtonNormalWidth: BODY_HEIGHT / 2,
  ButtonSmallWidth: BODY_HEIGHT / 3,

  //. Shadow and Borders
  ShadowElevation: Platform.OS === 'ios' ? 2 : 10,
  ButtonRadius: COMPONENT_RADIUS,
  InputRadius: COMPONENT_RADIUS,
  DefaultRadius: COMPONENT_RADIUS,

  //. Cards Defaults

  // Bar Defaults

  TitleBarHeight: UNIT_HEIGHT * 35,
  HeaderBarHeight: UNIT_HEIGHT * 72,
  HeaderBarMarginTop:
    Platform.OS === 'ios'
      ? STATUS_BAR_HEIGHT + UNIT_HEIGHT * 10
      : UNIT_HEIGHT * 2,

  //. Icons
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50,
  },

  //. Images
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 70,
  },
};

export default metrics;
