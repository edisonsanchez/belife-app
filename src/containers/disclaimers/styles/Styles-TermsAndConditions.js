import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    marginTop: Metrics.StatusBarHeight,
    //flexDirection: 'row',
    alignItems: 'center',
    width: Metrics.ScreenWidth,
  },

  line: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: Metrics.ScreenWidth,
  },

  text: {
    fontSize: Fonts.size.tiny,
    color: Colors.text,
  },

  link: {
    fontSize: Fonts.size.tiny,
  },
});
