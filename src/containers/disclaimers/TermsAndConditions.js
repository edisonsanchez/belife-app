import React, {useState} from 'react';
import {Text, View, Linking} from 'react-native';

import {Images, Metrics} from '../../styles';

import Styles from './styles/Styles-TermsAndConditions';

import {Link} from '../../components/buttons';
import {object} from 'prop-types';

const openTerms = () => {
  Linking.openURL('https://belifebyca.com/terms');
};

const openPrivacy = () => {
  Linking.openURL('https://belifebyca.com/privacy');
};

const TermsAndConditions = (props) => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.text}>
        {'Al presionar el boton "Registrar", acepto los '}
      </Text>
      <View style={Styles.line}>
        <Link
          styleText={Styles.link}
          text="Terminos y Condiciones"
          onPress={() => {
            this.openTerms();
          }}
        />

        <Text style={Styles.text}>{', y las '}</Text>

        <Link
          styleText={Styles.link}
          text="Politicas de Privacidad."
          onPress={() => {
            this.openPrivacy();
          }}
        />
      </View>
    </View>
  );
};

export default TermsAndConditions;
