import React from 'react';
import {View} from 'react-native';
import Styles from './styles/Styles-LoadingScreen';
//import Styles from "./styles/Styles-LoadingScreen";
import Loading from './Loading';

const LoadingScreen = (props) => {
  return (
    <View style={[Styles.container, props.style]}>
      <Loading />
    </View>
  );
};

export default LoadingScreen;
