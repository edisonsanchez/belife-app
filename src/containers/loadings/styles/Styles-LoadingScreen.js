import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.BodyHeight,
    width: Metrics.BodyWidth,
  },
});
