import { StyleSheet, Platform } from "react-native";
import { Fonts, Colors, Metrics } from "../../../styles/index";

//
export default StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },

  text: {
    ...Fonts.style.h4,
  },
});
