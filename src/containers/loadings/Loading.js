import React, {useState} from 'react';
import {Text, View, Linking} from 'react-native';
import Styles from './styles/Styles-Loading';
//import Styles from "./styles/Styles-Loading";
import SpinnerCircle from '../../components/spinners';

const Loading = (props) => {
  return (
    <View style={[Styles.container, props.styleContainer]}>
      <Text style={Styles.text}>Cargando...</Text>
    </View>
  );
};

export default Loading;
