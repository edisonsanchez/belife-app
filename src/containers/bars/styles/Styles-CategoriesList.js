import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    // marginTop: Metrics.HeaderBarMarginTop,
    justifyContent: 'center',
    alignItems: 'center',

    height: Metrics.HeaderBarHeight,
    width: Metrics.ScreenWidth,
  },
});
