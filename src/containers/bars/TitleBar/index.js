import React, {useState} from 'react';
import {Text, View, Linking} from 'react-native';
import {ButtonHeaderIcon} from '../../../components/buttons';

import Styles from './styles';

const TitleBar = ({onLeftIcon, onRightIcon}) => {
  return (
    <View style={Styles.container}>
      <ButtonHeaderIcon icon="music-box-outline" onPress={onLeftIcon} />
      <Text style={Styles.text}>BeLife App</Text>
      <ButtonHeaderIcon icon="whatsapp" onPress={onRightIcon} />
    </View>
  );
};

export default TitleBar;
