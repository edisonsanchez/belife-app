import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

//
export default StyleSheet.create({
  container: {
    marginTop: Metrics.HeaderBarMarginTop,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // backgroundColor: 'red',

    height: Metrics.TitleBarHeight,
    // width: 200,
    width: 'auto',
    borderBottomColor: '#CDCDCD',
    borderBottomWidth: 1,
  },

  text: {
    fontSize: Fonts.size.h5,
    fontWeight: '700',
    color: '#6D6D6D',
  },

  icon: {
    backgroundColor: 'blue',
    width: '100%',
  },
});
