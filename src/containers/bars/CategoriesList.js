import React, {useState} from 'react';
import {ScrollView, View, Linking} from 'react-native';
import {ButtonCircle} from '../../components/buttons';
import {Images, Metrics} from '../../styles';
import {loadNews} from '../../handlers/blogger';
import * as rssParser from 'react-native-rss-parser';
import {parse} from 'himalaya';

import Styles from './styles/Styles-CategoriesList';
import {object} from 'prop-types';

const openInstagram = () => {
  Linking.openURL('https://instagram.com/belifebyca');
};

const openYoutube = () => {
  Linking.openURL('https://www.youtube.com/channel/UCQYyB58msiWZVdX4mwf1-bg');
};

const openSpotify = () => {
  Linking.openURL(
    'https://open.spotify.com/playlist/4XxvnKB4HYc3JuyCoja0nK?si=6m-ZywZQSgmqtYLPHIOJHA',
  );
};

const openFacebook = () => {
  Linking.openURL('https://www.facebook.com/belifebyca/');
};

const CategoriesList = (props) => {
  return (
    <View style={Styles.container}>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <ButtonCircle
          onPress={props.onNews}
          image={Images.category_feed}
          text="Noticias"
        />
        <ButtonCircle
          onPress={() => openInstagram()}
          image={Images.category_instagram}
          text="Instagram"
        />
        <ButtonCircle
          onPress={() => openYoutube()}
          image={Images.category_youtube}
          text="YouTube"
        />
        <ButtonCircle
          onPress={() => openSpotify()}
          image={Images.category_music}
          text="Musica"
        />
        <ButtonCircle
          onPress={() => openFacebook()}
          image={Images.category_facebook}
          text="Facebook"
        />
      </ScrollView>
    </View>
  );
};

export default CategoriesList;
