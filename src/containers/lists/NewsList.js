import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {ButtonCircle} from '../../components/buttons';
import {Images, Metrics} from '../../styles';
import {BloggerRss} from '../../handlers/blogger';
import * as rssParser from 'react-native-rss-parser';
import {parse} from 'himalaya';

import Styles from './styles/Styles-ArticlesList';
import {object} from 'prop-types';

const Item = ({item, onPress}) => {
  item.image = item.image.replace('&amp;', '&');

  return (
    <View style={Styles.containerItem}>
      <TouchableOpacity style={Styles.button} onPress={() => onPress(item)}>
        <Image style={Styles.itemImage} source={{uri: item.image}} />
        <Text style={Styles.itemText}>{item.title}</Text>
      </TouchableOpacity>
    </View>
  );
};

const NewsList = (props) => {
  return (
    <SafeAreaView style={Styles.container}>
      <FlatList
        data={props.items}
        renderItem={({item}) => (
          <Item item={item} onPress={() => props.onPressItem(item)} />
        )}
        keyExtractor={(item) => item.title}
      />
    </SafeAreaView>
  );
};

export default NewsList;
