import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const HEIGHT =
  Platform.OS === 'ios'
    ? Metrics.ScreenHeight -
      Metrics.StatusBarHeight -
      Metrics.TabBottomBarHeight
    : Metrics.ScreenHeight -
      Metrics.StatusBarHeight -
      Metrics.TabBottomBarHeight -
      Metrics.HeaderBarHeight -
      Metrics.HeaderBarMarginTop -
      30;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: HEIGHT,
    width: Metrics.ScreenWidth,
    backgroundColor: '#FEFEFE',
    // backgroundColor: 'red',
  },

  containerItem: {
    width: Metrics.ScreenWidth,
    //    marginTop: Metrics.Height * 5,
  },

  itemImage: {
    height: Metrics.Height * 270,
    width: Metrics.ScreenWidth,
  },

  itemText: {
    height: Metrics.Height * 30,
    width: Metrics.ScreenWidth,
    color: '#000000',
    paddingHorizontal: 10,
    paddingTop: 5,
    fontSize: 16,
    textAlignVertical: 'center',
  },
});
