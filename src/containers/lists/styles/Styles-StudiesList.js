import {StyleSheet, Platform} from 'react-native';
import {Fonts, Colors, Metrics} from '../../../styles/index';

const CARD_HEIGHT = Metrics.Height * 200;
const CARD_WIDTH = Metrics.Width * 330;
const CARD_MARGIN = Metrics.Height * 6;
const TITLE_MARGIN = Metrics.Height * 10;
const BANNER_HEIGHT = CARD_HEIGHT * 0.3;

//
export default StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'center',

    height: Metrics.BodyHeight,
    width: Metrics.ScreenWidth,
  },

  containerItem: {
    width: CARD_WIDTH,
    marginVertical: CARD_MARGIN,

    borderRadius: 10,
    shadowColor: '#000000',
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      height: 3,
      width: 1,
    },
  },

  itemImage: {
    height: CARD_HEIGHT,
    resizeMode: 'stretch',
    borderRadius: 10,
  },

  itemView: {
    height: BANNER_HEIGHT,
    width: CARD_WIDTH,
    borderBottomLeftRadius: Metrics.ButtonRadius,
    borderBottomRightRadius: Metrics.ButtonRadius,

    backgroundColor: 'rgba(0,0,0, 0.5)',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },

  itemTitle: {
    ...Fonts.style.h4,
    width: CARD_WIDTH,

    color: '#FFFFFF',
    paddingLeft: TITLE_MARGIN,

    fontWeight: 'bold',
    textAlignVertical: 'center',
  },
});
