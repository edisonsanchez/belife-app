import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {ButtonCircle} from '../../components/buttons';
import {Images, Metrics} from '../../styles';
import {BloggerRss} from '../../handlers/blogger';
import * as rssParser from 'react-native-rss-parser';
import {parse} from 'himalaya';

import Styles from './styles/Styles-ArticlesList';
import {object} from 'prop-types';

import {ArticleCard} from '../../components/articles';

const Item = ({item, onPress}) => {
  if (item.title === 'ERROR') {
    return <View />;
  }

  if (item.image) {
    item.image = item.image.replace('&amp;', '&');
  }

  return (
    <View style={Styles.containerItem}>
      <ArticleCard
        image={item.image}
        title={item.title}
        description={item.bodyText}
        date={item.published}
        onPress={() => onPress(item)}
      />
    </View>
  );
};

const ArticlesList = (props) => {
  return (
    <SafeAreaView style={Styles.container}>
      <FlatList
        data={props.items}
        renderItem={({item}) => (
          <Item item={item} onPress={() => props.onPressItem(item)} />
        )}
        keyExtractor={(item) => item.published}
      />
    </SafeAreaView>
  );
};

export default ArticlesList;
