import NewsList from './NewsList';
import StudiesList from './StudiesList';
import ArticlesList from './ArticlesList';

export {NewsList, StudiesList, ArticlesList};
