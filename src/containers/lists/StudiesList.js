import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';

import Styles from './styles/Styles-StudiesList';

const Item = ({item, onPress}) => {
  item.image = item.image.replace('&amp;', '&');

  return (
    <View style={Styles.containerItem}>
      <TouchableOpacity style={Styles.button} onPress={() => onPress(item)}>
        <Image style={Styles.itemImage} source={{uri: item.image}} />
        <View style={Styles.itemView}>
          <Text style={Styles.itemTitle}>{item.title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const StudiesList = (props) => {
  return (
    <SafeAreaView style={Styles.container}>
      <FlatList
        data={props.items}
        renderItem={({item}) => (
          <Item item={item} onPress={() => props.onPressItem(item)} />
        )}
        keyExtractor={(item) => item.published}
      />
    </SafeAreaView>
  );
};

export default StudiesList;
