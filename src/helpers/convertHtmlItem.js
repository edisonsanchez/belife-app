import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Styles from './styles/Styles-HtmlToJsx';

export const convertHtmlItem = (item) => {
  if (item.type === 'element') {
    if (item.tagName === 'img') {
      const uriImage = item.attributes
        .filter((attribute) => attribute.key === 'src')[0]
        .value.replace(/\&amp;/g, '&');

      return {type: 'image', data: uriImage};
    } else if (item.tagName === 'br') {
      return {type: 'br', data: null};
    } else if (item.tagName === 'h2') {
      if (
        item.children[0].tagName &&
        item.children[0].children[0].type &&
        item.children[0].tagName === 'b' &&
        item.children[0].children[0].type === 'text'
      ) {
        return {
          type: 'text',
          data: item.children[0].children[0].content,
          style: 'h2',
        };
      } else if (item.children[0].type && item.children[0].type === 'text') {
        return {
          type: 'text',
          data: item.children[0].content,
          style: 'h2',
        };
      } else {
        return {
          type: '',
          data: null,
        };
      }
    } else if (item.tagName === 'h3' && item.children[0].type === 'text') {
      return {
        type: 'text',
        data: item.children[0].content,
        style: 'h3',
      };
    } else if (item.tagName === 'h4' && item.children[0].type === 'text') {
      return {
        type: 'text',
        data: item.children[0].content,
        style: 'h4',
      };
    } else if (item.tagName && item.tagName === 'div') {
      if (item.children && item.children.length > 0) {
        if (item.children[0].type === 'text') {
          return {
            type: 'text',
            data:
              item.children[0] && item.children[0].type === 'text'
                ? item.children[0].content
                : '',
            style: 'normal',
          };
        } else if (
          item.children[0].type === 'element' &&
          item.children[0].children &&
          item.children[0].children.length > 0 &&
          item.children[0].children[0].type === 'text'
        ) {
          return {
            type: 'text',
            data: item.children[0].children[0].content,
            style:
              item.children[0].tagName === 'b'
                ? 'bold'
                : item.children[0].tagName === 'i'
                ? 'italic'
                : item.children[0].tagName === 'u'
                ? 'underlined'
                : 'normal',
          };
        }
      } else if (item.children && item.children.length > 0) {
      } else {
        return {
          type: '',
          data: null,
        };
      }
    } else if (
      item.type &&
      item.type === 'element' &&
      item.children[0].type &&
      item.tagName &&
      item.children[0].type === 'text'
    ) {
      return {
        type: 'text',
        data: item.children[0].content,
        style:
          item.tagName === 'b'
            ? 'bold'
            : item.tagName === 'i'
            ? 'italic'
            : item.tagName === 'u'
            ? 'underlined'
            : 'normal',
      };
    } else if (item.children[0].type && item.children[0].type === 'text') {
      return {
        type: 'text',
        data: item.children[0].content,
        style: 'normal',
      };
    } else {
      return {
        type: '',
        data: null,
      };
    }
  } else if (item.type === 'text') {
    return {
      type: 'text',
      data: item.content,
      style: 'normal',
    };
  } else {
    return {type: null, data: null};
  }
};
