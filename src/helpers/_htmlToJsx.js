import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Styles from './styles/Styles-HtmlToJsx';

export const htmlToJSX = (item) => {
  if (item.type === 'element') {
    if (item.tagName === 'img') {
      const uriImage = item.attributes
        .filter((attribute) => attribute.key === 'src')[0]
        .value.replace(/\&amp;/g, '&');

      return (
        <Image
          style={Styles.itemImage}
          source={{
            uri: uriImage,
          }}
        />
      );
    } else if (item.tagName === 'br') {
      return <Text style={Styles.itemBr}> </Text>;
    } else if (
      item.tagName === 'h2' &&
      item.children[0].tagName === 'b' &&
      item.children[0].children[0].type === 'text'
    ) {
      return (
        <Text style={Styles.itemH2}>
          {item.children[0].children[0].content}
        </Text>
      );
    } else if (item.tagName === 'h3' && item.children[0].type === 'text') {
      return <Text style={Styles.itemH3}>{item.children[0].content}</Text>;
    } else if (item.tagName === 'h4' && item.children[0].type === 'text') {
      return <Text style={Styles.itemH4}>{item.children[0].content}</Text>;
    } else if (item.tagName === 'div' && item.children[0].type === 'text') {
      return <Text style={Styles.itemText}>{item.children[0].content}</Text>;
    } else if (item.tagName === 'div' && item.children[0].type === 'element') {
      let divStyles = Styles.itemText;

      if (item.children[0].tagName === 'b') {
        divStyles = [Styles.itemText, {fontWeight: 'bold'}];
      }

      if (item.children[0].tagName === 'i') {
        divStyles = [Styles.itemText, {fontStyle: 'italic'}];
      }

      if (item.children[0].tagName === 'u') {
        divStyles = [Styles.itemText, {textDecorationLine: 'underline'}];
      }

      return (
        <Text style={divStyles}>{item.children[0].children[0].content}</Text>
      );
    } else {
      return <Text style={Styles.itemBr}> </Text>;
    }
  } else if (item.type === 'text') {
    return <Text style={Styles.itemText}>{item.content}</Text>;
  } else {
    return <Text style={Styles.itemBr}> </Text>;
  }
};
