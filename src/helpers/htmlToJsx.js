import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Styles from './styles/Styles-HtmlToJsx';
import {convertHtmlItem} from './convertHtmlItem';

export const htmlToJSX = (item) => {
  const itemToRender = convertHtmlItem(item);

  switch (itemToRender.type) {
    case 'image':
      if (itemToRender.data === null) {
        break;
      }

      const uriImage = itemToRender.data.replace(/\&amp;/g, '&');

      return (
        <Image
          style={Styles.itemImage}
          source={{
            uri: uriImage,
          }}
        />
      );

    case 'br':
      return <Text style={Styles.itemBr}> </Text>;

    case 'text':
      if (itemToRender.style === 'h2') {
        return <Text style={Styles.itemH2}>{itemToRender.data}</Text>;
      } else if (itemToRender.style === 'h3') {
        return <Text style={Styles.itemH3}>{itemToRender.data}</Text>;
      } else if (itemToRender.style === 'h4') {
        return <Text style={Styles.itemH4}>{itemToRender.data}</Text>;
      } else if (itemToRender.style === 'h5') {
        return <Text style={Styles.itemH5}>{itemToRender.data}</Text>;
      } else if (itemToRender.style === 'b') {
        return (
          <Text style={[Styles.itemText, {fontWeight: 'bold'}]}>
            {itemToRender.data}
          </Text>
        );
      } else if (itemToRender.style === 'i') {
        return (
          <Text style={[Styles.itemText, {fontWeight: 'italic'}]}>
            {itemToRender.data}
          </Text>
        );
      } else if (itemToRender.style === 'u') {
        return (
          <Text style={[Styles.itemText, {textDecorationLine: 'underline'}]}>
            {itemToRender.data}
          </Text>
        );
      } else {
        return <Text style={[Styles.itemText]}>{itemToRender.data}</Text>;
      }

    default:
      return <Text style={Styles.itemBr}> </Text>;
  }
};
