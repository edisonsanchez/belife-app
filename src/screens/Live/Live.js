import React, { Component, Fragment } from "react";
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
  ScrollView,
} from "react-native";

import Styles from "./styles/LiveStyle";

import { Auth } from "aws-amplify";

import { YellowBox } from "react-native";
YellowBox.ignoreWarnings(["Warning: -[RCTRootView cancelTouches]"]);
import { HideNavigationBar } from "react-native-navigation-bar-color";
import SplashScreen from "react-native-splash-screen";
// Styles

//Multilanguage
import i18 from "../../localize/I18n";
import AsyncStorage from "@react-native-community/async-storage";
// import AudioStreamPlayer from "../../components/players/AudioStreamPlayer";

export default class Studies extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      errorMessage: null,
    };
  }

  componentDidMount = () => {
    // HideNavigationBar();
  };

  //
  render() {
    return (
      <View style={Styles.container}>
        {this.state.isLoading && <Text>IS LOADING...</Text>}
        {!this.state.isLoading && <Text>AAA</Text>}
      </View>
    );
  }
}
