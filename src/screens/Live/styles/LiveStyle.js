import { StyleSheet } from "react-native";
import { Fonts, Colors, Metrics } from "../../../styles";

//
export default StyleSheet.create({
  container: {
    width: Metrics.ScreenWidth,
    height:
      Metrics.ScreenHeight -
      (Metrics.StatusBarHeight + Metrics.TabBottomBarHeight),
    paddingTop: Metrics.StatusBarHeight,
  },
});
