import React, {Component} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/LoginStyle';

import {StackActions, NavigationActions} from 'react-navigation';
import Amplify, {Auth, Hub} from 'aws-amplify';

// import { YellowBox } from "react-native";
// YellowBox.ignoreWarnings(["Warning: -[RCTRootView cancelTouches]"]);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {Logo} from '../../components/logo';
import {Input} from '../../components/inputs';
import {
  Button,
  ButtonFacebook,
  ButtonSignup,
  Link,
  ButtonSocial,
} from '../../components/buttons';
import {DividerText} from '../../components/dividers';

import {LoadingScreen} from '../../containers/loadings';

import {awsSignIn} from '../../handlers/awsSignIn';

//
export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadingScreen: true,
      isLogining: false,

      username: '',
      password: '',
      button: false,
      onFocus: false,
      buttonSubmitDisabled: true,

      errorMessage: null,

      timer: null,
      sourceScreen: this.props.navigation.getParam('sourceScreen', null),
    };
  }

  componentDidMount = async () => {
    SplashScreen.hide();

    const errorMessageParent = this.props.navigation.getParam(
      'errorMessage',
      null,
    );

    if (errorMessageParent) {
      this.handleErrorMessage(errorMessageParent);
    }

    this.HUBstart();
    // HideNavigationBar();
    this.setState({isLoadingScreen: false});

    // setTimeout(() => {
    //   SplashScreen.hide();
    // }, 10000);
  };

  HUBstart = () => {
    Hub.listen('auth', async (data) => {
      if (this.state.sourceScreen === 'ConfirmSignup') {
        this.handleErrorMessage(this.props.navigation.getParam('errorMessage'));

        this.setState({isLogining: false});

        setTimeout(() => {
          this.setState({sourceScreen: null});
        }, 4000);
      }

      switch (data.payload.event) {
        case 'signIn':
          // await this.setState({isLogining: true});
          await this.props.navigation.navigate('AuthLoading');
          break;
        case 'signIn_failure':
          this.setState({isLogining: false});
          this.handleErrorMessage(
            'Para Ingresar con Facebook o Google debe dar permisos a la aplicacion.',
          );
          break;

        case 'cognitoHostedUI':
          this.setState({isLogining: true});
          break;

        case 'signOut':
          this.setState({isLogining: false});
          break;

        default:
          if (
            data.payload.event === 'parsingCallbackUrl' &&
            data.payload.data.url
          ) {
            this.setState({isLogining: true});
          }
          break;
      }
    });
  };

  HUBstop = () => {
    Hub.remove('auth', async (data) => {});
  };

  //
  formValidate = () => {
    return this.state.username.length && this.state.password.length;
  };

  //
  federatedTimeout = () => {
    const timer = setTimeout(() => {
      this.setState({isLogining: false});
    }, 10000);

    this.setState({timer});
  };

  //
  handleSignIn = async (type) => {
    this.setState({isLogining: true});

    if (type === 'FACEBOOK') {
      this.federatedTimeout();
      return await Auth.federatedSignIn({provider: 'Facebook'});
    } else if (type === 'GOOGLE') {
      this.federatedTimeout();
      return await Auth.federatedSignIn({provider: 'Google'});
    } else {
      const {data, message, code} = await awsSignIn(
        this.state.username,
        this.state.password,
      );

      if (data) {
        await this.props.navigation.navigate('AuthLoading');
        return;
      } else {
        this.setState({password: ''});
        if (code === 'UserNotConfirmedException') {
          this.setState({isLogining: false});
          await this.props.navigation.navigate('ConfirmSignup', {
            errorMessage:
              'Verificar el Correo electronico, se ha enviado un codigo de validacion.',
            username: this.state.username,
            password: this.state.password,
          });
          return;
        }

        this.handleErrorMessage(message);
      }

      this.setState({isLogining: false});
    }
  };

  //
  handleErrorMessage = (message) => {
    this.setState({errorMessage: message});

    setTimeout(() => {
      this.setState({errorMessage: null});
    }, 3000);
  };

  //
  handleChangeText = (type, text, lowercase) => {
    this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: !this.formValidate()});
    });
  };

  //
  handleSignup = () => {
    this.props.navigation.navigate('SignUp');
  };

  //
  handleForgotPassword = () => {
    this.props.navigation.navigate('ForgotPassword');
  };

  //
  //
  render() {
    //const isFocusOn = this.state.onFocus;

    return (
      <View style={Styles.container}>
        {this.state.isLoadingScreen ? (
          <LoadingScreen />
        ) : (
          <View>
            <View style={Styles.containerLogo}>
              <Logo />
            </View>
            <Input
              placeholder="Correo Electronico"
              value={this.state.username}
              onChangeText={(text) =>
                this.handleChangeText('username', text, true)
              }
            />
            <Input
              password
              placeholder="Contraseña"
              value={this.state.password}
              onChangeText={(text) => this.handleChangeText('password', text)}
            />
            <View style={Styles.forgotPassword}>
              <Link
                text="Recordar contraseña?"
                onPress={() => {
                  this.handleForgotPassword();
                }}
                disabled={this.state.buttonSubmitDisabled}
                isLoading={this.state.isLogining}
              />
            </View>
            <Button
              styleContainer={Styles.buttonLogin}
              text="Acceder"
              onPress={() => {
                this.handleSignIn('COGNITO');
              }}
              disabled={this.state.buttonSubmitDisabled}
              isLoading={this.state.isLogining}
              loadingText="Accediendo"
            />

            {this.state.errorMessage && (
              <Text style={Styles.errorMessage}>{this.state.errorMessage}</Text>
            )}
            {!this.state.isLogining && this.state.errorMessage === null && (
              <View>
                <ButtonSocial
                  facebook
                  styleContainer={Styles.buttonSocial}
                  text="Ingresar con Facebook"
                  disabled={this.state.isLogining}
                  onPress={() => {
                    this.handleSignIn('FACEBOOK');
                  }}
                />
                <ButtonSocial
                  google
                  styleContainer={Styles.buttonSocial}
                  text="Ingresar con Google"
                  disabled={this.state.isLogining}
                  onPress={() => {
                    this.handleSignIn('GOOGLE');
                  }}
                />
                <DividerText
                  text="o"
                  styleContainer={Styles.dividerContainer}
                />
                <View style={Styles.signupContainer}>
                  <Text style={Styles.dontHaveAccount}>
                    No tienes una cuenta?
                  </Text>
                  <Link
                    styleContainer={Styles.signupLink}
                    text="Registrate aqui."
                    onPress={() => {
                      this.handleSignup();
                    }}
                    disabled={this.state.buttonSubmitDisabled}
                    isLoading={this.state.isLogining}
                  />
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
