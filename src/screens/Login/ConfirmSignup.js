import React, {Component} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/LoginStyle';

import {StackActions, NavigationActions} from 'react-navigation';
import Amplify, {Auth, Hub} from 'aws-amplify';

// import { YellowBox } from "react-native";
// YellowBox.ignoreWarnings(["Warning: -[RCTRootView cancelTouches]"]);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {Logo} from '../../components/logo';
import {Input} from '../../components/inputs';
import {Button, ButtonSignup, Link} from '../../components/buttons';
import {LoadingScreen} from '../../containers/loadings';
import {DividerText} from '../../components/dividers';

import {goAuth} from '../../handlers/goAuth';

//
export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadingScreen: true,
      isSubmitting: false,

      username: this.props.navigation.getParam('username', null),
      password: this.props.navigation.getParam('password', null),
      code: '',
      button: false,
      onFocus: false,
      buttonSubmitDisabled: true,

      errorMessage: null,

      isResending: false,
    };
  }

  componentDidMount = async () => {
    SplashScreen.hide();

    const errorMessageParent = this.props.navigation.getParam(
      'errorMessage',
      null,
    );

    if (errorMessageParent) {
      this.handleErrorMessage(errorMessageParent);
    }

    // HideNavigationBar();
    this.setState({isLoadingScreen: false});
  };

  //
  formValidate = () => {
    return this.state.code.length === 6;
  };

  //
  handleAutoLogin = async (username, password) => {
    try {
      const retSignin = await Auth.signIn(username, password);

      this.props.navigation.navigate('Home');
      return;
    } catch (err) {
      await this.props.navigation.navigate('Login', {
        errorMessage: 'Usuario verificado, Contraseña Incorrecta',
        sourceScreen: 'ConfirmSignup',
      });
    }
    return;
  };

  //
  handleSubmit = async () => {
    // alert(JSON.stringify(this.state));
    const {username, password, code} = this.state;
    this.setState({isSubmitting: true});

    try {
      const success = await Auth.confirmSignUp(username, code);

      //Reset and Go Auth.
      this.handleAutoLogin(username, password);
    } catch (err) {
      if (err.code === 'CodeMismatchException') {
        this.handleErrorMessage(
          'Codigo de Verificacion Incorrecto.  Verifica el correo electronico',
        );
      } else {
        this.handleErrorMessage('Error en Registro (SGN95).');
      }
    }

    this.setState({isSubmitting: false, code: ''});
  };

  //
  handleErrorMessage = (message) => {
    this.setState({errorMessage: message});

    setTimeout(() => {
      this.setState({errorMessage: null});
    }, 3000);
  };

  //
  handleChangeText = (type, text, lowercase) => {
    this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: !this.formValidate()});
    });
  };

  //
  handleResend = async () => {
    this.setState({isSubmitting: true, isResending: true});

    try {
      await Auth.resendSignUp(this.state.username);

      this.handleErrorMessage('Codigo Enviado al correo electronico.');
    } catch (err) {
      return;
    }

    this.setState({isSubmitting: false, isResending: false});
  };

  //
  //
  render() {
    //const isFocusOn = this.state.onFocus;

    return (
      <View style={Styles.container}>
        {this.state.isLoadingScreen ? (
          <LoadingScreen />
        ) : (
          <View>
            <View style={Styles.containerLogo}>
              <Logo />
            </View>
            <Text style={Styles.info}>
              Se ha enviado un correo electronico con un Codigo de
              Verificacion...
            </Text>

            <Input
              icon="user"
              placeholder="Codigo de Verificacion"
              value={this.state.code}
              onChangeText={(text) => this.handleChangeText('code', text, true)}
            />
            <Button
              text="Verificar"
              onPress={() => {
                this.handleSubmit();
              }}
              disabled={this.state.buttonSubmitDisabled}
              isLoading={this.state.isSubmitting}
              loadingText="Verificando..."
            />
            {this.state.errorMessage && (
              <Text style={Styles.errorMessage}>{this.state.errorMessage}</Text>
            )}
            {!this.state.isSubmitting && (
              <View>
                <View style={[Styles.signupContainer, {marginTop: 20}]}>
                  <Text style={Styles.dontHaveAccount}>
                    No recibiste el codigo?
                  </Text>
                  <Link
                    styleContainer={Styles.signupLink}
                    text="Enviar nuevamente"
                    onPress={() => {
                      this.handleResend();
                    }}
                    disabled={this.state.buttonSubmitDisabled}
                    isLoading={this.state.isLogining}
                  />
                </View>
                <DividerText
                  text="o"
                  styleContainer={Styles.dividerContainer}
                />

                <View style={Styles.signupContainer}>
                  <Text style={Styles.dontHaveAccount}>
                    Ya tienes una cuenta?
                  </Text>
                  <Link
                    styleContainer={Styles.signupLink}
                    text="Ingresa Aqui."
                    onPress={() => {
                      goAuth(this.props);
                    }}
                    disabled={this.state.buttonSubmitDisabled}
                    isLoading={this.state.isLogining}
                  />
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
