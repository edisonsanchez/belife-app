import React, {Component} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/LoginStyle';

import {StackActions, NavigationActions} from 'react-navigation';
import Amplify, {Auth, Hub} from 'aws-amplify';

// import { YellowBox } from "react-native";
// YellowBox.ignoreWarnings(["Warning: -[RCTRootView cancelTouches]"]);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {Logo} from '../../components/logo';
import {Input} from '../../components/inputs';
import {Button, ButtonSignup, Link} from '../../components/buttons';
import {LoadingScreen} from '../../containers/loadings';
import {DividerText} from '../../components/dividers';

import {goAuth} from '../../handlers/goAuth';

//
export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadingScreen: true,
      isSubmitting: false,

      email: this.props.navigation.getParam('email', null),
      code: '',
      button: false,
      onFocus: false,
      buttonSubmitDisabled: true,

      errorMessage: null,

      isResending: false,
    };
  }

  componentDidMount = async () => {
    SplashScreen.hide();

    const errorMessageParent = this.props.navigation.getParam(
      'errorMessage',
      null,
    );

    if (errorMessageParent) {
      this.handleErrorMessage(errorMessageParent);
    }

    // HideNavigationBar();
    this.setState({isLoadingScreen: false});
  };

  //
  formValidate = () => {
    return this.state.email.length === 6;
  };

  //
  handleSubmit = async () => {
    // alert(JSON.stringify(this.state));
    const {email, code} = this.state;
    this.setState({isSubmitting: true});

    try {
      const success = await Auth.forgotPassword(email);

      this.props.navigation.navigate('NewPassword');
      return;
    } catch (err) {
      if (err.code === 'CodeMismatchException') {
        this.handleErrorMessage(
          'Codigo de Verificacion Incorrecto.  Verifica el correo electronico',
        );
      } else {
        this.handleErrorMessage('Error (SGN95).');
      }
    }

    this.setState({isSubmitting: false, code: ''});
  };

  //
  handleErrorMessage = (message) => {
    this.setState({errorMessage: message});

    setTimeout(() => {
      this.setState({errorMessage: null});
    }, 3000);
  };

  //
  handleChangeText = (type, text, lowercase) => {
    this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: !this.formValidate()});
    });
  };

  //
  //
  render() {
    //const isFocusOn = this.state.onFocus;

    return (
      <View style={Styles.container}>
        {this.state.isLoadingScreen ? (
          <LoadingScreen />
        ) : (
          <View>
            <View style={Styles.containerLogo}>
              <Logo />
            </View>
            <Text style={Styles.info}>
              Se enviara un codigo al correo electronico, para cambiar la clave.
            </Text>

            <Input
              placeholder="Correo Electronico"
              value={this.state.email}
              onChangeText={(text) =>
                this.handleChangeText('email', text, true)
              }
            />
            <Button
              text="Enviar codigo"
              onPress={() => {
                this.handleSubmit();
              }}
              disabled={this.state.buttonSubmitDisabled}
              isLoading={this.state.isSubmitting}
              loadingText="Enviando..."
            />
            {this.state.errorMessage && (
              <Text style={Styles.errorMessage}>{this.state.errorMessage}</Text>
            )}
            {!this.state.isSubmitting && (
              <View>
                <DividerText
                  text="o"
                  styleContainer={Styles.dividerContainer}
                />

                <View style={Styles.signupContainer}>
                  <Text style={Styles.dontHaveAccount}>
                    Ya recordo su contraseña?
                  </Text>
                  <Link
                    styleContainer={Styles.signupLink}
                    text="Ingresa Aqui."
                    onPress={() => {
                      goAuth(this.props);
                    }}
                    disabled={this.state.buttonSubmitDisabled}
                    isLoading={this.state.isLogining}
                  />
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
