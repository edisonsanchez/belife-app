import React, {Component} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/LoginStyle';

import {StackActions, NavigationActions} from 'react-navigation';
import Amplify, {Auth, Hub} from 'aws-amplify';

// import { YellowBox } from "react-native";
// YellowBox.ignoreWarnings(["Warning: -[RCTRootView cancelTouches]"]);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {Logo} from '../../components/logo';
import {Input} from '../../components/inputs';
import {DividerText} from '../../components/dividers';
import {
  ButtonIcon,
  ButtonFacebook,
  ButtonSignup,
  Button,
  ButtonSocial,
  Link,
} from '../../components/buttons';
import {LoadingScreen} from '../../containers/loadings';
import {TermsAndConditions} from '../../containers/disclaimers';

import {goAuth} from '../../handlers/goAuth';

//
export default class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoadingScreen: true,
      isLogining: false,

      username: '',
      password: '',
      confirmPassword: '',
      button: false,
      onFocus: false,
      buttonSubmitDisabled: true,

      errorMessage: null,

      timer: null,
    };
  }

  componentDidMount = async () => {
    SplashScreen.hide();

    // HideNavigationBar();
    this.setState({isLoadingScreen: false});
  };

  //
  formValidate = () => {
    return (
      this.state.username.length &&
      this.state.password.length &&
      this.state.password.length >= 6
    );
  };

  handleSignup = async () => {
    // alert(JSON.stringify(this.state));
    const {password, confirmPassword} = this.state;

    const username = this.state.username.toLowerCase();
    const email = username;

    try {
      const success = await Auth.signUp({
        username,
        password,
        attributes: {email},
      });

      this.props.navigation.navigate('ConfirmSignup', {
        username: username,
        password: password,
      });
    } catch (err) {
      if (err.code === 'UsernameExistsException') {
        this.handleErrorMessage('Usuario Existe, Intente acceder nuevamente.');
      } else if (err.code === 'InvalidParameterException') {
        if (err.message === 'Invalid email address format.') {
          this.handleErrorMessage('Formato de Correo electronico incorrecto.');
        } else if (err.message.includes('password')) {
          this.handleErrorMessage(
            'Contraseña debe ser minimo de 6 letras o numeros.',
          );
        } else {
          this.handleErrorMessage('Datos incorrectos, Verifique los datos.');
        }
      } else {
        this.handleErrorMessage('Error en Registro (SGN110).');
      }
    }
  };

  //
  handleErrorMessage = (message) => {
    this.setState({errorMessage: message});

    setTimeout(() => {
      this.setState({errorMessage: null});
    }, 3000);
  };

  //
  handleChangeText = (type, text, lowercase) => {
    this.setState({[type]: text}, () => {
      this.setState({buttonSubmitDisabled: !this.formValidate()});
    });
  };

  //
  handleLogin = () => {
    this.props.navigation.navigate('Login');
  };

  //
  //
  render() {
    //const isFocusOn = this.state.onFocus;

    return (
      <View style={Styles.container}>
        {this.state.isLoadingScreen ? (
          <LoadingScreen />
        ) : (
          <View>
            <View style={Styles.containerLogo}>
              <Logo />
            </View>
            <Input
              icon="user"
              placeholder="Correo Electronico"
              value={this.state.username}
              onChangeText={(text) =>
                this.handleChangeText('username', text, true)
              }
            />
            <Input
              password
              placeholder="Contraseña"
              value={this.state.password}
              onChangeText={(text) => this.handleChangeText('password', text)}
              secureTextEntry={true}
            />
            <Button
              text="Registrarse"
              onPress={() => {
                this.handleSignup();
              }}
              disabled={this.state.buttonSubmitDisabled}
              isLoading={this.state.isLogining}
              loadingText="Registrando..."
            />
            <TermsAndConditions />
            {this.state.errorMessage && (
              <Text style={Styles.errorMessage}>{this.state.errorMessage}</Text>
            )}
            <DividerText text="o" styleContainer={Styles.dividerContainer} />

            {!this.state.isLogining && (
              <View style={Styles.signupContainer}>
                <Text style={Styles.dontHaveAccount}>
                  Ya tienes una cuenta?
                </Text>
                <Link
                  styleContainer={Styles.signupLink}
                  text="Ingresa Aqui."
                  onPress={() => {
                    goAuth(this.props);
                  }}
                  disabled={this.state.buttonSubmitDisabled}
                  isLoading={this.state.isLogining}
                />
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}
