import {StyleSheet} from 'react-native';
import {Fonts, Colors, Metrics, AppStyles} from '../../../styles';

const LOGO_CONTAINER_HEIGHT = Metrics.Height * 204;

//
export default StyleSheet.create({
  ...AppStyles.screen,

  containerLogo: {
    width: Metrics.ScreenWidth,
    height: LOGO_CONTAINER_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Metrics.StatusBarHeight,
  },

  errorMessage: {
    width: Metrics.ScreenWidth,
    padding: 5,

    color: 'red',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },

  info: {
    width: Metrics.ScreenWidth,
    paddingVertical: 5,
    paddingHorizontal: 5,
    color: Colors.app,
    fontSize: 16,
    textAlign: 'center',
  },

  signupButton: {
    marginTop: Metrics.Height * 20,
  },

  forgotPassword: {
    width: Metrics.ScreenWidth,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: Metrics.Width * 20,
    marginBottom: Metrics.Height * 10,
  },

  buttonLogin: {
    marginBottom: Metrics.Height * 20,
  },
  buttonSocial: {
    marginBottom: Metrics.Height * 10,
  },

  dividerContainer: {
    marginVertical: Metrics.Height * 30,
  },

  signupContainer: {
    flexDirection: 'row',
    width: Metrics.ScreenWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },

  dontHaveAccount: {
    ...Fonts.style.regular,
    color: Colors.textDark,
  },

  signupLink: {
    marginLeft: Metrics.Width * 5,
  },
});
