import React, {Component, Fragment} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
  ScrollView,
} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/StudiesStyle';

import {Auth} from 'aws-amplify';

import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['Warning: -[RCTRootView cancelTouches]']);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';
import {StudiesRSS} from '../../handlers/blogger';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {CategoriesList} from '../../containers/bars';
import {StudiesList} from '../../containers/lists';
import {ArticleDetail} from '../../components/articles';
import {LoadingScreen, Loading} from '../../containers/loadings';

export default class Studies extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      isLoading: true,
      isLoadingItems: true,

      isScreenArticle: false,
      item: null,

      errorMessage: null,
    };

    //this.handleLoadItems = this.handleLoadItems().bind();
  }

  componentDidMount = () => {
    // HideNavigationBar();

    this.handleLoadItems();
    SplashScreen.hide();
    this.setState({isLoading: false});
  };

  handleLoadItems = async () => {
    try {
      const items = await StudiesRSS();

      if (items && items.length > 0) {
        if (items[0].title === 'ERROR') {
          this.exceptionMessage(JSON.stringify(items[0].bodyText));
          this.setState({isLoadingItems: false});
          return;
        }
      }

      this.setState({items, isLoadingItems: false});
    } catch (err) {
      this.exceptionMessage(JSON.stringify(err));
    }
  };

  handleItems = (items) => {
    this.setState({items, isLoading: false});
  };

  handleBack = () => {
    this.setState({item: null, isScreenArticle: false});
  };

  showItem = (item) => {
    this.setState({item, isScreenArticle: true});
  };

  render() {
    return (
      <View>
        {this.state.isLoading ? (
          <View style={Styles.container}>
            <LoadingScreen />
          </View>
        ) : (
          <View style={Styles.container}>
            {!this.state.isLoadingItems &&
              this.state.items.length > 0 &&
              !this.state.isScreenArticle && (
                <StudiesList
                  items={this.state.items}
                  onPressItem={(item) => this.showItem(item)}
                />
              )}
            {this.state.isScreenArticle && this.state.item && (
              <ArticleDetail
                article={this.state.item}
                btnRetText="Ver Otros Estudios"
                onBack={() => this.handleBack()}
              />
            )}
            {this.state.isLoadingItems && (
              <Loading styleContainer={Styles.body} />
            )}
          </View>
        )}
      </View>
    );
  }
}
