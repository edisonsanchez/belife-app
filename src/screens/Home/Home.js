import React, {Component, Fragment} from 'react';
import {View, Alert, Linking} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/HomeStyle';
import {Container, Header, Content, Input, Item} from 'native-base';

import {Auth} from 'aws-amplify';

import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['Warning: -[RCTRootView cancelTouches]']);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

import TitleBar from '../../containers/bars/TitleBar';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {CategoriesList} from '../../containers/bars';
import {ArticlesList} from '../../containers/lists';
import {ArticleDetail} from '../../components/articles';
import {NewsRSS} from '../../handlers/blogger';
import {LoadingScreen, Loading} from '../../containers/loadings';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      isLoading: true,
      isLoadingScreen: true,

      isScreenArticle: false,
      item: null,

      errorMessage: null,
    };
  }

  componentDidMount = () => {
    this.handleLoadItems();
    SplashScreen.hide();
    this.setState({isLoading: false});
  };

  //
  exceptionMessage = (message, title = 'Alert') => {
    Alert.alert(
      title,
      message,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'OK'},
      ],
      {cancelable: false},
    );
  };

  handleLoadItems = async () => {
    try {
      const items = await NewsRSS();

      if (items && items.length > 0) {
        const resultItems = items.filter((x) => {
          return x.title !== 'ERROR';
        });
        this.setState({items: resultItems, isLoadingItems: false});
      }
    } catch (err) {
      this.exceptionMessage(JSON.stringify(err));
    }
  };

  handleItems = (items) => {
    this.setState({items, isLoading: false});
  };

  handleBack = () => {
    this.setState({item: null, isScreenArticle: false});
  };

  handleRadio = () => {
    Linking.openURL('http://belifebyca.com/');
  };

  handleChat = () => {
    Linking.openURL(
      'https://api.whatsapp.com/send?phone=18298665388&text=Hola,%20mi%20consulta%20es...',
    );
  };

  showItem = (item) => {
    this.setState({item, isScreenArticle: true});
  };

  render() {
    return (
      <View>
        {this.state.isLoading ? (
          <View style={Styles.container}>
            <LoadingScreen />
          </View>
        ) : (
          <View style={Styles.container}>
            <TitleBar
              onLeftIcon={this.handleRadio}
              onRightIcon={this.handleChat}
            />
            {!this.state.isScreenArticle && (
              <CategoriesList onNews={() => this.handleLoadItems()} />
            )}

            {!this.state.isLoadingItems &&
              this.state.items.length > 0 &&
              !this.state.isScreenArticle && (
                <ArticlesList
                  items={this.state.items}
                  onPressItem={(item) => this.showItem(item)}
                />
              )}
            {this.state.isScreenArticle && this.state.item && (
              <ArticleDetail
                article={this.state.item}
                btnRetText="Ver mas Noticias"
                onBack={() => this.handleBack()}
              />
            )}
            {this.state.isLoadingItems && (
              <LoadingScreen style={Styles.loadingItems} />
            )}
          </View>
        )}
      </View>
    );
  }
}
