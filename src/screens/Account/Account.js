import React, {Component} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Keyboard,
  ImageBackground,
  Platform,
  StatusBar,
  Text,
} from 'react-native';

import {Images, Metrics} from '../../styles';
import Styles from './styles/AccountStyle';

import {StackActions, NavigationActions} from 'react-navigation';
import {Auth} from 'aws-amplify';

import {YellowBox} from 'react-native';
YellowBox.ignoreWarnings(['Warning: -[RCTRootView cancelTouches]']);
import {HideNavigationBar} from 'react-native-navigation-bar-color';
import SplashScreen from 'react-native-splash-screen';

// Styles

//Multilanguage
import i18 from '../../localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

//import Icon from 'react-native-vector-icons/FontAwesome';
import {Logo} from '../../components/logo';
import {ButtonIcon, ButtonFacebook} from '../../components/buttons';

export default class Account extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      button: false,
      onFocus: false,
      buttonSubmitDisabled: true,
      isSigning: false,

      errorMessage: null,
    };
  }

  componentDidMount = () => {
    SplashScreen.hide();

    // this.HUBstart();

    // HideNavigationBar();
  };

  handleLogout = () => {
    try {
      AsyncStorage.removeItem('sessionToken');
      Auth.signOut();

      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'AuthStack',
            params: {},
          }),
        ],
      });

      const goLogin = NavigationActions.navigate({
        routeName: 'Login',
        params: {},
      });

      this.props.navigation.dispatch(resetAction);
      this.props.navigation.dispatch(goLogin);
    } catch (error) {
      return;
    }
  };

  render() {
    //const isFocusOn = this.state.onFocus;

    return (
      <View style={Styles.container}>
        <View style={Styles.containerLogo}>
          <Logo />
          <Text style={Styles.version}>Powered by PerSan (Ver 1.0.0 Beta)</Text>
        </View>
        <ButtonIcon
          icon="logout"
          text="Cerrar Sesion"
          onPress={() => {
            this.handleLogout();
          }}
        />
      </View>
    );
  }
}
