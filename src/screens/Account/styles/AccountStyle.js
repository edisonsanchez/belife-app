import { StyleSheet } from "react-native";
import { Fonts, Colors, Metrics } from "../../../styles";

//
export default StyleSheet.create({
  containerLogo: {
    width: Metrics.ScreenWidth,
    height: Metrics.Height * 230,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: Metrics.Height * 28,
  },

  version: {
    color: Colors.grayLight,
  },
});
