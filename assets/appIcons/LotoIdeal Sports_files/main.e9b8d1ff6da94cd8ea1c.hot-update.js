webpackHotUpdate("main",{

/***/ "./src/pages/store/BodySport/index.js":
/*!********************************************!*\
  !*** ./src/pages/store/BodySport/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! contexts/AppContext */ "./src/contexts/AppContext.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/pages/store/BodySport/styles.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/store/BodySport/index.js";

/* eslint-disable react-hooks/exhaustive-deps */
 // import PropTypes from 'prop-types'
//!Material UI
//!Contexts 

 //!Components
//!Global Styles
// import { } from 'styles';
//!Local Styles






 // import Link from '@material-ui/core/Link';
//

const BodySportComponent = ({
  sport
}) => {
  // const { user } = useAuthContext();
  const {
    ligues,
    matches,
    getLiguesBySportId,
    getMatchsByLigueId,
    liguesResult,
    matchsResult
  } = Object(contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__["useAppContext"])();
  const [ligue, setLigue] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(ligues ? ligues[0] : null);
  const [match, setMatch] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(matches ? matches[0] : null);
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  const [isSelectingLigue, setSelectingLigue] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true); //Will Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useLayoutEffect(() => {
    const onLoad = async () => {
      try {} catch (err) {
        console.log(err);
      }
    };

    onLoad(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    console.log('Component Did Mount');
  }, []); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    console.log('sportchange');
    console.log(sport);

    if (sport) {
      console.log('loadLigues');
      const x = getLiguesBySportId(sport.id);
      console.log(x);
      setLigue(null);
    }
  }, [sport, ligues]); //

  const handleSelectLigue = item => {
    console.log(item);
    setLigue(item);
  };

  const renderLigueMatches = () => {// console.log(ligueMatches);
  };

  const selectMatch = bet => {
    console.log(bet);
    setMatch(bet);
  }; //


  const renderLigues = () => {
    return liguesResult.map(item => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_6__["default"], {
        key: item.id,
        onClick: () => handleSelectLigue(item),
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86,
          columnNumber: 9
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
        key: item.id,
        styles: item && item.local && item.local.url ? { ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueItemView,
          backgroundImage: `url(${item.local.url})`,
          backgroundRepeat: 'round'
        } : _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueItemView,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87,
          columnNumber: 11
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
        styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueTitleView,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 13
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
        styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueTitle,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 15
        }
      }, item.local.title))));
    });
  }; //


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: sport && sport.local && sport.local.url ? { ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].body,
      backgroundImage: `url(${sport.local.url})`,
      backgroundRepeat: 'round'
    } : _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].body,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: isSelectingLigue ? { ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].container,
      ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].containerSelectLigue
    } : _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].container,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 7
    }
  }, sport && sport.local && sport.local.id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].sportTitle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108,
      columnNumber: 11
    }
  }, translate(sport.id)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].disclaimer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109,
      columnNumber: 11
    }
  }, translate('STORE.LIGUES.INSTRUCTIONS'))), liguesResult && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].liguesContainer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 11
    }
  }, renderLigues())));
};

const BodySport = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(BodySportComponent);
/* harmony default export */ __webpack_exports__["default"] = (BodySport);

/***/ })

})
//# sourceMappingURL=main.e9b8d1ff6da94cd8ea1c.hot-update.js.map