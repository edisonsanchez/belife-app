(this["webpackJsonpbolet-sports"] = this["webpackJsonpbolet-sports"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/App.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".App {\n  text-align: center;\n}\n\n.App-logo {\n  height: 40vmin;\n  pointer-events: none;\n}\n\n@media (prefers-reduced-motion: no-preference) {\n  .App-logo {\n    animation: App-logo-spin infinite 20s linear;\n  }\n}\n\n.App-header {\n  background-color: #282c34;\n  min-height: 100vh;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  font-size: calc(10px + 2vmin);\n  color: white;\n}\n\n.App-link {\n  color: #61dafb;\n}\n\n@keyframes App-logo-spin {\n  from {\n    transform: rotate(0deg);\n  }\n  to {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/index.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "body {\n  margin: 0;\n  padding: 0;\n  font-family: \"Roboto\", sans-serif;\n  font-size: 16px;\n  color: #333;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n\ncode {\n  font-family: source-code-pro, Menlo, Monaco, Consolas, \"Courier New\",\n    monospace;\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-family: \"Roboto\", sans-serif;\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./src/App.css":
/*!*********************!*\
  !*** ./src/App.css ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pages_Routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/Routes */ "./src/pages/Routes.js");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App.css */ "./src/App.css");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_App_css__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/App.js";




const App = () => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Suspense, {
    fallback: null,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_pages_Routes__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 5
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/assets/logo/BoletSportsPNG.png":
/*!********************************************!*\
  !*** ./src/assets/logo/BoletSportsPNG.png ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/BoletSportsPNG.21379632.png";

/***/ }),

/***/ "./src/aws-exports.js":
/*!****************************!*\
  !*** ./src/aws-exports.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// WARNING: DO NOT EDIT. This file is automatically generated by AWS Amplify. It will be overwritten.
const awsmobile = {
  "aws_project_region": "us-east-1",
  "aws_cognito_identity_pool_id": "us-east-1:0d783b37-cd66-4a39-9fb5-8d0fb039bb40",
  "aws_cognito_region": "us-east-1",
  "aws_user_pools_id": "us-east-1_xEltxudeU",
  "aws_user_pools_web_client_id": "5sdtoh4ja546qq8tdro0u3acov",
  "oauth": {
    "domain": "boletsports-local.auth.us-east-1.amazoncognito.com",
    "scope": ["phone", "email", "openid", "profile", "aws.cognito.signin.user.admin"],
    "redirectSignIn": "http://localhost:3000/,https://localhost:3000/",
    "redirectSignOut": "http://localhost:3000/,https://localhost:3000/",
    "responseType": "code"
  },
  "federationTarget": "COGNITO_USER_POOLS",
  "aws_appsync_graphqlEndpoint": "https://7krqewsufvcezfx2d6wuxgctmi.appsync-api.us-east-1.amazonaws.com/graphql",
  "aws_appsync_region": "us-east-1",
  "aws_appsync_authenticationType": "AMAZON_COGNITO_USER_POOLS",
  "aws_appsync_apiKey": "da2-5vgfaj7asnauxcca4yq4mnbsyu",
  "aws_content_delivery_bucket": "bolet-sports-20200519124627-hostingbucket-local",
  "aws_content_delivery_bucket_region": "us-east-1",
  "aws_content_delivery_url": "http://bolet-sports-20200519124627-hostingbucket-local.s3-website-us-east-1.amazonaws.com"
};
/* harmony default export */ __webpack_exports__["default"] = (awsmobile);

/***/ }),

/***/ "./src/components/bars/Header/index.js":
/*!*********************************************!*\
  !*** ./src/components/bars/Header/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/bars/Header/styles.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_bars_NavBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/bars/NavBar */ "./src/components/bars/NavBar/index.js");
/* harmony import */ var components_images_Logo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! components/images/Logo */ "./src/components/images/Logo/index.js");
/* harmony import */ var components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/bars/LanguageBar */ "./src/components/bars/LanguageBar/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/bars/Header/index.js";








 //!Header

const Header = ({
  authenticated,
  onNavBar,
  menuSelected,
  balance,
  onLogin
}) => {
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_3__["useLanguageContext"])();
  const TextValues = {
    SeeDetails: translate('see-details'),
    PlayNow: translate('play-now')
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["WebHeader"], {
    id: "Header",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["BoxLogo"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_images_Logo__WEBPACK_IMPORTED_MODULE_5__["default"], {
    id: "Logo",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["Menu"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_NavBar__WEBPACK_IMPORTED_MODULE_4__["NavBar"], {
    onChange: onNavBar,
    selectedItem: menuSelected,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 9
    }
  })), authenticated && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["ButtonBalance"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_7__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].balance,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 11
    }
  }, '$' + balance), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_7__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].seeDetails,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 11
    }
  }, TextValues.SeeDetails)), !authenticated && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["BoxLogin"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["ButtonLogin"], {
    onClick: () => {
      onNavBar('login');
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 11
    }
  }, TextValues.PlayNow ? TextValues.PlayNow : ''), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_6__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 11
    }
  })));
};

Header.propTypes = {
  authenticated: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./src/components/bars/Header/styles.js":
/*!**********************************************!*\
  !*** ./src/components/bars/Header/styles.js ***!
  \**********************************************/
/*! exports provided: Stylesheet, WebHeader, BoxLogo, Menu, BoxLogin, ButtonBalance, ButtonLogin, Span */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebHeader", function() { return WebHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoxLogo", function() { return BoxLogo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Menu", function() { return Menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoxLogin", function() { return BoxLogin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonBalance", function() { return ButtonBalance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonLogin", function() { return ButtonLogin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Span", function() { return Span; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/bars/Header/styles.js";

 // Material UI


 // Global Styles

 //Stylesheet

const Stylesheet = {
  balance: {
    display: 'flex',
    color: 'white',
    height: '60%',
    fontSize: 32 // width: '100%',

  },
  seeDetails: {
    display: 'flex',
    color: 'white',
    fontSize: 12,
    height: '40%' // lineHeight: 0,

  }
}; //Container

const WebHeader = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.SpaceBetween,
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].Header,
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].primary
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]); //!BoxLogo

const BoxLogo = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Center,
    height: styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].Header.height,
    minWidth: '20%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]); //!BoxMenu

const Menu = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Start,
    height: styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].Header.height,
    minWidth: '60%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]); //!BoxLogin

const BoxLogin = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.SpaceEvenly,
    height: styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].Header.height,
    minWidth: '20%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]); //!ButtonBalance

const ButtonBalance = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Center,
    height: styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].Header.height,
    minWidth: '20%',
    alignItems: 'flex-end'
  },
  text: {
    padding: 0,
    paddingRight: 10
  },
  label: {
    height: 'inherit',
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Center,
    flexDirection: 'column'
  }
})(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__["default"]); //!ButtonBalance

const ButtonLogin = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Center,
    height: '60%',
    minWidth: '20%',
    width: '70%',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].secondary,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].textLight,
    fontWeight: 700,
    fontSize: 14
  },
  text: {
    padding: 0
  },
  label: {
    height: 'inherit',
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Center,
    flexDirection: 'column'
  }
})(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__["default"]);
const Span = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: {},
  label: {
    fontSize: 30,
    lineHeight: 'normal',
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].text
  },
  subtitle: {
    fontSize: 10,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].MUIColors.grey[200]
  }
})(({
  classes,
  children,
  subtitle
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
  className: subtitle ? classes.subtitle : classes.label,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 121,
    columnNumber: 5
  }
}, children));

/***/ }),

/***/ "./src/components/bars/LanguageBar/index.js":
/*!**************************************************!*\
  !*** ./src/components/bars/LanguageBar/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core_Menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Menu */ "./node_modules/@material-ui/core/esm/Menu/index.js");
/* harmony import */ var _material_ui_core_MenuItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/MenuItem */ "./node_modules/@material-ui/core/esm/MenuItem/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var _data_languages__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../data/languages */ "./src/data/languages.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/bars/LanguageBar/index.js";
 // import { LeftContainer } from './styles'








const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__["makeStyles"])(theme => ({
  root: {
    // backgroundColor: 'yellow',
    display: 'flex',
    justifyContent: 'center',
    padding: 0,
    flexDirection: 'column',
    minWidth: 35,
    paddingLeft: 5,
    paddingRight: 5
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
    height: 30,
    maxHeight: 35,
    minWidth: 35,
    '&:hover': {
      backgroundColor: '#D0D0D0',
      borderColor: '#A3ADD0',
      borderRadius: 5 // width: 38,
      // height: 38,

    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf'
    },
    '&:focus': {// boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    }
  },
  buttonLabel: {
    maxWidth: 35
  },
  listContainer: {
    marginTop: 20 // top: 15,

  },
  listItem: {
    justifyContent: 'flex-start',
    aligntItems: 'center',
    // backgroundColor: 'green',
    lineHeight: 0,
    minHeight: 0,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20
  },
  flag: {
    borderRadius: 5,
    width: 25,
    height: 25,
    borderStyle: 'none'
  },
  iconList: {
    width: 25,
    height: 25,
    marginRight: 5
  },
  text: {
    justifyContent: 'flex-start',
    aligntItems: 'center',
    fontSize: 14,
    fontWeight: 400,
    marginLeft: 10,
    color: '#464e5f'
  },
  rippleRoot: {
    maxWidth: 0
  }
}));
const DataLanguages = {
  "spanish": _data_languages__WEBPACK_IMPORTED_MODULE_7__["languages"].filter(x => {
    return x.id === 'es';
  })[0],
  "english": _data_languages__WEBPACK_IMPORTED_MODULE_7__["languages"].filter(x => {
    return x.id === 'en';
  })[0],
  "french": _data_languages__WEBPACK_IMPORTED_MODULE_7__["languages"].filter(x => {
    return x.id === 'fr';
  })[0],
  "portuguese": _data_languages__WEBPACK_IMPORTED_MODULE_7__["languages"].filter(x => {
    return x.id === 'pr';
  })[0]
}; //

const LanguageBar = () => {
  const classes = useStyles();
  const {
    language,
    translate,
    onChange
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_6__["useLanguageContext"])();
  const [anchorEl, setAnchorEl] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  const [langObject, setLangObject] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(_data_languages__WEBPACK_IMPORTED_MODULE_7__["languages"].filter(x => {
    return x.id === language;
  })[0]);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  }; //


  const changeLanguage = id => {
    onChange(id);
    setLangObject(_data_languages__WEBPACK_IMPORTED_MODULE_7__["languages"].filter(function (x) {
      return x.id === id;
    })[0]);
    setAnchorEl(null);
  }; ///Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_5__["default"], {
    className: classes.root,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 125,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_1__["default"], {
    className: classes.button,
    classes: {
      label: classes.buttonLabel
    },
    "aria-controls": "simple-menu",
    "aria-haspopup": "true",
    onClick: handleClick // TouchRippleProps={{ classes: { root: classes.rippleRoot }}}
    ,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 126,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: classes.flag,
    src: langObject.flag,
    alt: 'language-selected-' + langObject.tag,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 133,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Menu__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: "simple-menu",
    anchorEl: anchorEl,
    keepMounted: true,
    open: Boolean(anchorEl),
    onClose: handleClose,
    className: classes.listContainer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 7
    }
  }, language !== DataLanguages.spanish.id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_MenuItem__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: classes.listItem,
    onClick: () => changeLanguage(DataLanguages.spanish.id),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 146,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: classes.flag,
    src: DataLanguages.spanish.flag,
    alt: 'language-selected-' + DataLanguages.spanish.tag,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: classes.text,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 150,
      columnNumber: 11
    }
  }, translate(DataLanguages.spanish.label))), language !== DataLanguages.english.id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_MenuItem__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: classes.listItem,
    onClick: () => changeLanguage(DataLanguages.english.id),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 154,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: classes.flag,
    src: DataLanguages.english.flag,
    alt: 'language-selected-' + DataLanguages.english.tag,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: classes.text,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 158,
      columnNumber: 11
    }
  }, translate(DataLanguages.english.label))), language !== 'fr' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_MenuItem__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: classes.listItem,
    onClick: () => changeLanguage(DataLanguages.french.id),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 162,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: classes.flag,
    src: DataLanguages.french.flag,
    alt: 'language-selected-' + DataLanguages.french.tag,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 163,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: classes.text,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 166,
      columnNumber: 9
    }
  }, translate(DataLanguages.french.label))), language !== 'pr' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_MenuItem__WEBPACK_IMPORTED_MODULE_3__["default"], {
    className: classes.listItem,
    onClick: () => changeLanguage(DataLanguages.portuguese.id),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 170,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: classes.flag,
    src: DataLanguages.portuguese.flag,
    alt: 'language-selected-' + DataLanguages.portuguese.tag,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 171,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: classes.text,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 174,
      columnNumber: 9
    }
  }, translate(DataLanguages.portuguese.label)))));
};

/* harmony default export */ __webpack_exports__["default"] = (LanguageBar);

/***/ }),

/***/ "./src/components/bars/LeftButtonNav/index.js":
/*!****************************************************!*\
  !*** ./src/components/bars/LeftButtonNav/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_BottomNavigation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/BottomNavigation */ "./node_modules/@material-ui/core/esm/BottomNavigation/index.js");
/* harmony import */ var _material_ui_core_BottomNavigationAction__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/BottomNavigationAction */ "./node_modules/@material-ui/core/esm/BottomNavigationAction/index.js");
/* harmony import */ var _material_ui_core_Icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Icon */ "./node_modules/@material-ui/core/esm/Icon/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/bars/LeftButtonNav/index.js";





const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])({
  root: {
    maxWidth: 80,
    height: 'auto',
    flexDirection: 'column'
  }
}); //

const LeftButtonNav = props => {
  const classes = useStyles();
  const [value, setValue] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(props.itemSelected ? props.itemSelected : props.secondary && props.secondary.length > 0 ? props.secondary[0].id : ''); // const [items, setItems] = React.useState(props.secondary);
  //Change Secondary Items

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    if (props.secondary && props.secondary.length > 0) {
      setValue(props.secondary[0].id);
      handleSelect('secondary', props.secondary[0].id);
    } // eslint-disable-next-line react-hooks/exhaustive-deps

  }, [props.secondary]); //Change HandleSelect

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    if (props.itemSelected) {
      setValue(props.itemSelected.id);
    }
  }, [props.itemSelected]); //

  const handleSelect = (level, newValue) => {
    const item = props.secondary.filter(function (el) {
      return el.id === newValue;
    });
    props.onSelect(level, newValue, item && item.length > 0 ? item[0] : null);
  }; //


  const handleChange = async (event, newValue) => {
    setValue(newValue);
    handleSelect('secondary', newValue);
  }; //


  const renderButtons = () => {
    return props.secondary.map(item => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_BottomNavigationAction__WEBPACK_IMPORTED_MODULE_3__["default"], {
        key: item.id,
        label: item.local.title,
        value: item.id,
        icon: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          __self: undefined,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 54,
            columnNumber: 31
          }
        }, item.local.icon),
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53,
          columnNumber: 14
        }
      });
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_BottomNavigation__WEBPACK_IMPORTED_MODULE_2__["default"], {
    showLabels: true,
    value: value,
    onChange: handleChange,
    className: classes.root,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 5
    }
  }, renderButtons());
};

/* harmony default export */ __webpack_exports__["default"] = (LeftButtonNav);

/***/ }),

/***/ "./src/components/bars/NavBar/index.js":
/*!*********************************************!*\
  !*** ./src/components/bars/NavBar/index.js ***!
  \*********************************************/
/*! exports provided: NavBar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBar", function() { return NavBar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_AppBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/AppBar */ "./node_modules/@material-ui/core/esm/AppBar/index.js");
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Tabs */ "./node_modules/@material-ui/core/esm/Tabs/index.js");
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Tab */ "./node_modules/@material-ui/core/esm/Tab/index.js");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/Typography */ "./node_modules/@material-ui/core/esm/Typography/index.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./styles */ "./src/components/bars/NavBar/styles.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/bars/NavBar/index.js";














function TabContainer(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_7__["default"], {
    component: "div",
    style: {
      padding: 8 * 3
    },
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 5
    }
  }, props.children);
}

TabContainer.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node.isRequired
};

function LinkTab(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_6__["default"], Object.assign({
    inkBarStyle: {
      background: '#FFF',
      fontWeight: 800
    },
    onClick: event => {
      event.preventDefault();
    }
  }, props, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 5
    }
  }));
}

const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__["makeStyles"])(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    boxShadow: 'rgba(0, 0, 0, 0)'
  },
  tabs: {
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_11__["Colors"].app,
    color: 'white',
    fontSize: 16,
    fontWeight: 800
  },
  appBar: {
    boxShadow: 'none'
  }
}));
const NavBar = ({
  onChange,
  selectedItem = 'home'
}) => {
  const {
    isAuthenticated,
    isAuthenticating,
    authLogout
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_8__["useAuthContext"])();
  const {
    translate,
    isLoading
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_9__["useLanguageContext"])();
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["useHistory"])();
  const classes = useStyles();
  const [value, setValue] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(selectedItem);

  const handleChange = (event, newValue) => {
    setValue(newValue);

    switch (newValue) {
      case 'logout':
        handleLogout();
        return;

      case 'login':
        history.push("/login");
        return;

      case 'signup':
        history.push("/signup");
        return;

      default:
        if (onChange) {
          onChange(newValue);
        }

        break;
    }
  };

  const handleChangeItem = (event, newValue) => {
    event.preventDefault();
    setValue(newValue);
    onChange(newValue);
  };

  const handleLogout = async () => {
    authLogout();
    history.push("/login");
  }; ///Render


  return !isAuthenticating && !isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: classes.root,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_AppBar__WEBPACK_IMPORTED_MODULE_4__["default"], {
    position: "static",
    className: classes.appBar,
    style: {
      background: 'transparent',
      boxShadow: 'none'
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111,
      columnNumber: 7
    }
  }, isAuthenticated && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_5__["default"], {
    variant: "fullWidth",
    value: value,
    onChange: handleChange,
    className: classes.tabs,
    TabIndicatorProps: {
      style: {
        backgroundColor: '#FFF'
      }
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkTab, {
    value: "home",
    label: translate('home'),
    to: "/",
    component: react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"],
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkTab, {
    value: "store",
    label: translate('store'),
    to: "/store",
    component: react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"],
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LinkTab, {
    value: "logout",
    label: translate('logout'),
    to: "/",
    component: react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"],
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 117,
      columnNumber: 11
    }
  })), !isAuthenticated && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_10__["MenuTabs"], {
    id: 'MenuTabs-Authenticated',
    variant: "fullWidth",
    value: value,
    onChange: handleChangeItem,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 121,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_10__["MenuItem"], {
    id: 'MenuItem.Home',
    value: "home",
    label: translate('home'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 126,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_10__["MenuItem"], {
    id: 'MenuItem.Results',
    value: "results",
    label: translate('results'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 129,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_10__["MenuItem"], {
    id: 'MenuItem.Help',
    value: "help",
    label: translate('help'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 132,
      columnNumber: 11
    }
  }))));
};

/***/ }),

/***/ "./src/components/bars/NavBar/styles.js":
/*!**********************************************!*\
  !*** ./src/components/bars/NavBar/styles.js ***!
  \**********************************************/
/*! exports provided: MenuTabs, MenuItem, Header, Nav, Link, LinkButton, Text */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuTabs", function() { return MenuTabs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuItem", function() { return MenuItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return Header; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Nav", function() { return Nav; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Link", function() { return Link; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkButton", function() { return LinkButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Text", function() { return Text; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_animation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../styles/animation */ "./src/styles/animation.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Tabs */ "./node_modules/@material-ui/core/esm/Tabs/index.js");
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Tab */ "./node_modules/@material-ui/core/esm/Tab/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styles */ "./src/styles/index.js");







 //!ButtonBalance

const MenuTabs = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__["withStyles"])({
  root: {
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_7__["Colors"].app
  },
  indicator: {
    backgroundColor: '#FFF'
  }
})(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_5__["default"]); //!ButtonBalance

const MenuItem = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_4__["withStyles"])({
  root: {
    fontSize: 16
  }
})(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_6__["default"]);
const Header = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isBrowser"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
      width: 50%;
      display: flex;
      background: #A0A0A0;
      height: 50px;
      align-items: center;
      justify-content: space-around;
      left: 0;
      top: 0;
      position: fixed;
      right: 0;
      width: 100%;
      z-index: 1000;
  `};
`;
const Nav = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].nav`
  align-items: center;
  display: flex;
  height: 40px;
  justify-content: space-around;
  left: 0;
  margin: 0 auto;
  ${''
/* max-width: 500px; */
}
  position: fixed;
  right: 0;
  width: 100%;
  z-index: 1000;
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
    background: #fcfcfc;
    border-top: 1px solid #e0e0e0;
    bottom: 0;
    height: 50px;
  `};
`;
const Link = Object(styled_components__WEBPACK_IMPORTED_MODULE_0__["default"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"])`
  align-items: center;
  color: #FFF;
  display: inline-flex;
  height: 100%;
  justify-content: center;
  text-decoration: none;
  width: 100%;

  ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
      color: #888;
  `};

  &[aria-current] {
    color: #000;

    &:after {
      ${Object(_styles_animation__WEBPACK_IMPORTED_MODULE_2__["fadeIn"])({
  time: '0.5s'
})};
      content: '-----';
      position: absolute;
      bottom: 0;
      font-size: 34px;
      line-height: 20px;
      ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
        content: '·';
      `};
    }
  }
`;
const LinkButton = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
    align-items: center;
    background: #A0A0A0;
    display: inline-flex;
    height: 100%;
    justify-content: center;
    text-decoration: none;
    width: 100%;
    border-width: 0px;
    cursor: pointer;
    padding: 0px;

    ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
      background: #FFF;
      color: #888;
  `};
  }
`;
const Text = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].p`
  color: #FFF;
  font-family: "Roboto", sans-serif;
  font-size: 16px;
  margin: 0px;
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
      color: #888;
  `};
`;

/***/ }),

/***/ "./src/components/bars/SupraHeader/index.js":
/*!**************************************************!*\
  !*** ./src/components/bars/SupraHeader/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/bars/LanguageBar */ "./src/components/bars/LanguageBar/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_menus_ProductsMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/menus/ProductsMenu */ "./src/components/menus/ProductsMenu/index.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var components_drawers_UserDrawer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/drawers/UserDrawer */ "./src/components/drawers/UserDrawer/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./styles */ "./src/components/bars/SupraHeader/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/bars/SupraHeader/index.js";








 //!Header

const SupraHeader = ({
  id = "SupraHeader",
  onNavBar,
  menuSelected,
  balance,
  onLogin
}) => {
  const {
    authLogout
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__["useAuthContext"])();
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_6__["useHistory"])();

  const handleLogout = async () => {
    authLogout();
    history.push("/login");
  }; // const TextValues = {
  // }


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    id: id,
    styles: _styles__WEBPACK_IMPORTED_MODULE_8__["Stylesheet"].container,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    id: id + '.Column-LeftContainer',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_menus_ProductsMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    onClick: id => {
      console.log(id);
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    id: id + '.Column-RightContainer',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    id: id + '.Column-SettingsContainer',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    id: id + 'Column.UserContainer',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_drawers_UserDrawer__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onLogout: handleLogout,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 13
    }
  }))));
};

SupraHeader.propTypes = {
  authenticated: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
/* harmony default export */ __webpack_exports__["default"] = (SupraHeader);

/***/ }),

/***/ "./src/components/bars/SupraHeader/styles.js":
/*!***************************************************!*\
  !*** ./src/components/bars/SupraHeader/styles.js ***!
  \***************************************************/
/*! exports provided: Stylesheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styles */ "./src/styles/index.js");

const Stylesheet = {
  container: {
    justifyContent: 'space-between',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_0__["Colors"].backgroundLight
  }
};

/***/ }),

/***/ "./src/components/buttons/ButtonLoading/index.js":
/*!*******************************************************!*\
  !*** ./src/components/buttons/ButtonLoading/index.js ***!
  \*******************************************************/
/*! exports provided: ButtonLoading */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonLoading", function() { return ButtonLoading; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/components/buttons/ButtonLoading/styles.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/buttons/ButtonLoading/index.js";



const ButtonLoading = ({
  children,
  disabled,
  onClick,
  loading
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["ButtonStyle"], {
    disabled: disabled || loading,
    onClick: onClick,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 10
    }
  }, loading ? "Loading..." : children);
};
ButtonLoading.propTypes = {
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  loading: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  children: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node.isRequired
};

/***/ }),

/***/ "./src/components/buttons/ButtonLoading/styles.js":
/*!********************************************************!*\
  !*** ./src/components/buttons/ButtonLoading/styles.js ***!
  \********************************************************/
/*! exports provided: ButtonStyle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonStyle", function() { return ButtonStyle; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
 // const rotate = keyframes`
//   100% {
//     transform: rotate(360deg);
//   }
// }`

const ButtonStyle = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
    width: 165px;
    height:35px;  
    border-radius: 4px;
    background: ${({
  disabled
}) => disabled ? '#A0A0A0' : '#3b5998'};

    color:white;
    border:0px transparent;  
    text-align: center;
    margin:0 auto;
    margin-bottom: 3%;
    ${''
/* display: inline-block; */
}
    display: block;

    transition-delay: ${({
  loading
}) => loading ? '0ms' : '200ms'};

    &:hover{
        background: #3b5998;
        opacity: 0.6;
    }


`;

/***/ }),

/***/ "./src/components/buttons/ButtonProduct/index.js":
/*!*******************************************************!*\
  !*** ./src/components/buttons/ButtonProduct/index.js ***!
  \*******************************************************/
/*! exports provided: ButtonProduct */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonProduct", function() { return ButtonProduct; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Link */ "./node_modules/@material-ui/core/esm/Link/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/buttons/ButtonProduct/index.js";



 //Styles



const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  button: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Center,
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].SupraHeader.Menu.Button,
    // ...Effects.SupraHeader.Menu.Button.Hover,
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.SupraHeader.Menu.Button.normal
  },
  selected: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Center,
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].SupraHeader.Menu.Button,
    // ...Effects.SupraHeader.Menu.Button.Hover,
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.SupraHeader.Menu.Button.active
  },
  text: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Fonts"].SupraHeader,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.SupraHeader.Menu.Text.normal
  }
})); // Main Component

const ButtonProduct = ({
  item,
  selected = false,
  onClick
}) => {
  const ref = react__WEBPACK_IMPORTED_MODULE_0___default.a.createRef();
  const classes = useStyles();
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])(); //

  const handleClick = id => {
    onClick(id);
  }; ///Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: 'ButtonProduct-' + item.id,
    component: "button",
    className: !selected ? classes.button : classes.selected,
    onClick: () => {
      handleClick(item.id);
    },
    ref: ref,
    key: item.id,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: classes.text,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 9
    }
  }, translate(item.id)));
};

ButtonProduct.propTypes = {
  item: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func
};


/***/ }),

/***/ "./src/components/buttons/ButtonSocial/index.js":
/*!******************************************************!*\
  !*** ./src/components/buttons/ButtonSocial/index.js ***!
  \******************************************************/
/*! exports provided: ButtonSocial */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonSocial", function() { return ButtonSocial; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/components/buttons/ButtonSocial/styles.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/buttons/ButtonSocial/index.js";



const ButtonSocial = ({
  children,
  disabled,
  onClick,
  google
}) => {
  return google ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["ButtonGoogle"], {
    type: "submit",
    disabled: disabled,
    onClick: onClick,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 18
    }
  }, children) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["ButtonFacebook"], {
    type: "submit",
    disabled: disabled,
    onClick: onClick,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 3
    }
  }, children);
};
ButtonSocial.propTypes = {
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  children: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.node.isRequired
};

/***/ }),

/***/ "./src/components/buttons/ButtonSocial/styles.js":
/*!*******************************************************!*\
  !*** ./src/components/buttons/ButtonSocial/styles.js ***!
  \*******************************************************/
/*! exports provided: ButtonFacebook, ButtonGoogle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonFacebook", function() { return ButtonFacebook; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonGoogle", function() { return ButtonGoogle; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");

const ButtonFacebook = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
    width: 165px;
    height:35px;  
    border-radius: 4px;
    background: #3b5998;
    color:white;
    border:0px transparent;  
    text-align: center;
    margin:5px;
    display: inline-block;

    &:hover{
        background: #3b5998;
        opacity: 0.6;
    }
`;
const ButtonGoogle = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
    margin:5px;
    width: 165px;
    height:35px;
    border-radius: 4px;
    background: #db3236;
    color:white;
    border:0px transparent;
    text-align: center;

    &:hover{
        background: #3b5998;
        opacity: 0.6;
    }
`;

/***/ }),

/***/ "./src/components/buttons/Link/index.js":
/*!**********************************************!*\
  !*** ./src/components/buttons/Link/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/buttons/Link/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/buttons/Link/index.js";


 //

const Link = ({
  children,
  id = "Link",
  component = "button",
  className,
  styles,
  onClick
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["LinkStyled"], {
    component: component,
    onClick: onClick,
    className: className,
    styles: styles,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 5
    }
  }, children);
}; //


Link.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Link);

/***/ }),

/***/ "./src/components/buttons/Link/styles.js":
/*!***********************************************!*\
  !*** ./src/components/buttons/Link/styles.js ***!
  \***********************************************/
/*! exports provided: LinkStyled */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkStyled", function() { return LinkStyled; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var clsx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! clsx */ "./node_modules/clsx/dist/clsx.m.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Link */ "./node_modules/@material-ui/core/esm/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/buttons/Link/styles.js";





const LinkStyled = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_3__["Fonts"].span,
    ...styles__WEBPACK_IMPORTED_MODULE_3__["Styles"].Row.Start,
    color: styles__WEBPACK_IMPORTED_MODULE_3__["Colors"].text,
    cursor: 'pointer'
  },
  styles: props => ({ ...props.styles
  }),
  empty: {}
})(({
  classes,
  children,
  className,
  onClick
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4__["default"], {
  onClick: onClick,
  className: Object(clsx__WEBPACK_IMPORTED_MODULE_1__["default"])(classes.root, classes.styles, className ? className : classes.empty),
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 22,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/drawers/UserDrawer/Drawer/index.js":
/*!***********************************************************!*\
  !*** ./src/components/drawers/UserDrawer/Drawer/index.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_images_Avatar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/images/Avatar */ "./src/components/images/Avatar/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./styles */ "./src/components/drawers/UserDrawer/Drawer/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/drawers/UserDrawer/Drawer/index.js";

 // Material UI


 //!Contexts


 //!Components

 // Global Styles
// import { Images } from 'styles';
// Local Styles



const BoxDrawer = ({
  onLogout,
  onToggle
}) => {
  const classes = Object(_styles__WEBPACK_IMPORTED_MODULE_7__["useStyles"])();
  const {
    userData
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__["useAuthContext"])();
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["View"], {
    id: "BoxDrawer-Render:View",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: "BoxDrawer-Render:Box.profileBox",
    className: classes.profileBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: "BoxDrawer-Render:Box-Box.profilePhoto",
    className: classes.profilePhoto,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["Photo"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_images_Avatar__WEBPACK_IMPORTED_MODULE_6__["default"], {
    letter: userData.name[0],
    big: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 13
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: "BoxDrawer-Render:Box-Box-Profile-Box.profileInfoBox",
    className: classes.profileInfoBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["TextUserTitle"], {
    id: "BoxDrawer-Render:TextUserTitle-name",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 11
    }
  }, userData.name.toUpperCase()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["TextUserSubtitle"], {
    id: "BoxDrawer-Render:TextUserTitle-email",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 11
    }
  }, userData.email), userData.phone && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["TextUserSubtitle"], {
    id: "BoxDrawer-Render:TextUserTitle-phone",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 13
    }
  }, "phone"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["TextUserSubtitle"], {
    id: "BoxDrawer-Render:TextUserTitle-verified",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 11
    }
  }, userData.email_verified ? 'verified' : 'Not verified'), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: "BoxDrawer-Render:Box-Box-Profile-Box.profileInfoBox",
    className: classes.logoutButtonBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["ButtonLogout"], {
    id: "BoxDrawer-Render:ButtonLogout",
    onClick: onLogout,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_7__["TextLogout"], {
    id: "BoxDrawer-Render:TextLogout",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 15
    }
  }, translate('logout')))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    id: "BoxDrawer-Render:Box-Box-Body.bodyBox",
    className: classes.bodyBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 7
    }
  }));
};

_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__["default"].propTypes = {
  onToggle: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onLogout: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (BoxDrawer);

/***/ }),

/***/ "./src/components/drawers/UserDrawer/Drawer/styles.js":
/*!************************************************************!*\
  !*** ./src/components/drawers/UserDrawer/Drawer/styles.js ***!
  \************************************************************/
/*! exports provided: useStyles, View, Photo, TextUserTitle, TextUserSubtitle, ButtonLogout, TextLogout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useStyles", function() { return useStyles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View", function() { return View; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Photo", function() { return Photo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextUserTitle", function() { return TextUserTitle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextUserSubtitle", function() { return TextUserSubtitle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonLogout", function() { return ButtonLogout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextLogout", function() { return TextLogout; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core */ "./node_modules/@material-ui/core/esm/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
 // Material UI



 // Components
// Global Styles


const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["makeStyles"])({
  profileBox: {
    width: '100%',
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.SpaceEvenly,
    // backgroundColor: Colors.Pallette.Yellow.darkkhaki,
    marginLeft: 10,
    marginRight: 10
  },
  profileInfoBox: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Start,
    alignItems: 'flex-start',
    width: '60%' // backgroundColor: Colors.Pallette.Yellow.lemonchiffon,

  },
  logoutButtonBox: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Center,
    // backgroundColor: Colors.Pallette.Yellow.palegoldenrod,
    width: '100%'
  },
  bodyBox: {},
  bodyItem: {},
  bodyItemText: {}
});
const View = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Start,
    // backgroundColor: Colors.Pallette.Yellow.yellow,
    // height: 500,
    padding: 10
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]);
const Photo = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Start,
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Pallette.Yellow.yellow,
    height: 100,
    width: 100,
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 10
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]);
const TextUserTitle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Fonts"].AccountTitle,
    padding: 5,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.AccountDrawer.title
  }
})(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormLabel"]);
const TextUserSubtitle = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Fonts"].AccountSubtitle,
    padding: 5,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.AccountDrawer.subtitle
  }
})(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormLabel"]);
const ButtonLogout = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Center,
    ...styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].Profile.Logout.Button,
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.AccountDrawer.button,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].MUIColors.white,
    width: '60%',
    marginTop: 10
  },
  label: {
    color: '#FFF'
  }
})(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__["default"]);
const TextLogout = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Fonts"].AccountTitle,
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].MUIColors.white
  }
})(_material_ui_core__WEBPACK_IMPORTED_MODULE_3__["FormLabel"]);

/***/ }),

/***/ "./src/components/drawers/UserDrawer/index.js":
/*!****************************************************!*\
  !*** ./src/components/drawers/UserDrawer/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Drawer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Drawer */ "./src/components/drawers/UserDrawer/Drawer/index.js");
/* harmony import */ var components_images_Avatar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/images/Avatar */ "./src/components/images/Avatar/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./styles */ "./src/components/drawers/UserDrawer/styles.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/drawers/UserDrawer/index.js";
 // import PropTypes from 'prop-types'
// Material UI
// import Box from '@material-ui/core/Box';
// Components


 // Global Styles

 // Local Styles


 // Render

const UserDrawer = ({
  onLogout
}) => {
  const [drawerOpen, setDrawerOpen] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const {
    userData
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_5__["useAuthContext"])(); //

  const toggleDrawer = open => event => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setDrawerOpen(open);
  }; //


  const sideList = () => {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Drawer__WEBPACK_IMPORTED_MODULE_1__["default"], {
      onToggle: toggleDrawer,
      onLogout: onLogout,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 7
      }
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__["View"], {
    id: "UserDrawer-Render:View",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__["ButtonDrawer"], {
    id: "UserDrawer-Render:ButtonDrawer",
    onClick: toggleDrawer(true),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__["BoxUsername"], {
    id: "UserDrawer-Render:BoxInitial",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__["TextUsername"], {
    id: "UserDrawer-Render:TextUsername",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 11
    }
  }, userData.name)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__["BoxInitial"], {
    id: "UserDrawer-Render:BoxInitial",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_images_Avatar__WEBPACK_IMPORTED_MODULE_2__["default"], {
    letter: userData.name[0],
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 11
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_4__["Drawer"], {
    id: "container-drawer-user",
    anchor: "right",
    open: drawerOpen,
    onClose: toggleDrawer(false),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 7
    }
  }, sideList('right')));
}; //Props


UserDrawer.propTypes = {//
};
/* harmony default export */ __webpack_exports__["default"] = (UserDrawer);

/***/ }),

/***/ "./src/components/drawers/UserDrawer/styles.js":
/*!*****************************************************!*\
  !*** ./src/components/drawers/UserDrawer/styles.js ***!
  \*****************************************************/
/*! exports provided: useStyles, BoxUsername, BoxInitial, Drawer, View, ButtonDrawer, TextUsername, TextInitial */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useStyles", function() { return useStyles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoxUsername", function() { return BoxUsername; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoxInitial", function() { return BoxInitial; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Drawer", function() { return Drawer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View", function() { return View; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonDrawer", function() { return ButtonDrawer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextUsername", function() { return TextUsername; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextInitial", function() { return TextInitial; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Drawer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Drawer */ "./node_modules/@material-ui/core/esm/Drawer/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js");
/* harmony import */ var _texts_Span__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../texts/Span */ "./src/components/texts/Span/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../styles */ "./src/styles/index.js");
 // Material UI



 // Components

 // Global Styles


const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["makeStyles"])({});
const BoxUsername = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].Column.Center,
    ..._styles__WEBPACK_IMPORTED_MODULE_5__["Metrics"].SupraHeader.Account.Username.Container // backgroundColor: Colors.Presets.SupraHeader.Initial.background,

  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]);
const BoxInitial = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].Column.Center,
    ..._styles__WEBPACK_IMPORTED_MODULE_5__["Metrics"].SupraHeader.Account.Initial.Container,
    minWidth: 30,
    minHeight: 30,
    backgroundColor: _styles__WEBPACK_IMPORTED_MODULE_5__["Colors"].Presets.SupraHeader.Initial.background
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]);
const Drawer = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: {},
  paper: {
    minWidth: '30%',
    height: 'auto'
  }
})(_material_ui_core_Drawer__WEBPACK_IMPORTED_MODULE_1__["default"]);
const View = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].Row.Center,
    ..._styles__WEBPACK_IMPORTED_MODULE_5__["Metrics"].SupraHeader.Account.Container
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]);
const ButtonDrawer = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].Row.Center,
    ..._styles__WEBPACK_IMPORTED_MODULE_5__["Metrics"].SupraHeader.Account.Button
  }
})(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_3__["default"]);
const TextUsername = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_5__["Fonts"].AccountName,
    color: _styles__WEBPACK_IMPORTED_MODULE_5__["Colors"].Presets.SupraHeader.Menu.Text.normal
  }
})(_texts_Span__WEBPACK_IMPORTED_MODULE_4__["default"]);
const TextInitial = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_5__["Fonts"].AccountInitial,
    ..._styles__WEBPACK_IMPORTED_MODULE_5__["Styles"].Row.Center,
    color: _styles__WEBPACK_IMPORTED_MODULE_5__["Colors"].Presets.SupraHeader.Initial.color
  }
})(_texts_Span__WEBPACK_IMPORTED_MODULE_4__["default"]); //   export {StyledDrawer}

/***/ }),

/***/ "./src/components/forms/Form/index.js":
/*!********************************************!*\
  !*** ./src/components/forms/Form/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/forms/Form/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/forms/Form/index.js";


 //

const Form = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["FormStyled"], Object.assign({}, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 10
    }
  }), children);
}; //


Form.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Form);

/***/ }),

/***/ "./src/components/forms/Form/styles.js":
/*!*********************************************!*\
  !*** ./src/components/forms/Form/styles.js ***!
  \*********************************************/
/*! exports provided: FormStyled */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormStyled", function() { return FormStyled; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/forms/Form/styles.js";



const FormStyled = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Fonts"].span,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].text,
    display: 'flex',
    flexDirection: 'column'
  },
  styles: props => ({ ...props.styles
  })
})(({
  classes,
  children,
  onSubmit,
  disabled
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
  className: `${classes.root} ${classes.styles}`,
  onSubmit: onSubmit,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/forms/FormConfirm/index.js":
/*!***************************************************!*\
  !*** ./src/components/forms/FormConfirm/index.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../hooks/useInputValue */ "./src/hooks/useInputValue.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/forms/FormConfirm/styles.js");
/* harmony import */ var components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/buttons/ButtonLoading */ "./src/components/buttons/ButtonLoading/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_forms_Form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! components/forms/Form */ "./src/components/forms/Form/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! components/bars/LanguageBar */ "./src/components/bars/LanguageBar/index.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/InputAdornment */ "./node_modules/@material-ui/core/esm/InputAdornment/index.js");
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/forms/FormConfirm/index.js";













/* harmony default export */ __webpack_exports__["default"] = (({
  error,
  disabled,
  onSubmit,
  onCancel,
  title,
  loading
}) => {
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_4__["useLanguageContext"])();
  const code = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])(''); //

  const handleSubmit = event => {
    event.preventDefault();
    onSubmit(code.value);
  }; //


  const handleValidate = event => {
    return code.value.length > 0;
  }; //!


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_Form__WEBPACK_IMPORTED_MODULE_5__["default"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].form,
    disabled: disabled,
    onSubmit: handleSubmit,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewHeader,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewLanguage,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_8__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_12__["default"], {
    onClick: onCancel,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_6__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].textClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 15
    }
  }, translate('COMMON.CANCEL')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9___default.a, {
    style: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].iconClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 15
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_6__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].title,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 11
    }
  }, title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["View"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewFields,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('FORM.CONFIRM.CODE'),
    placeholder: translate('FORM.CONFIRM.CODE'),
    variant: "outlined"
  }, code, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_10__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70,
          columnNumber: 19
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["AccountCircle"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 21
        }
      }))
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 13
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowForgot,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_3__["ButtonLoading"], {
    loading: loading,
    disabled: !handleValidate(),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 11
    }
  }, translate('FORM.CONFIRM.BUTTON')))), error && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["Error"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 17
    }
  }, error));
});

/***/ }),

/***/ "./src/components/forms/FormConfirm/styles.js":
/*!****************************************************!*\
  !*** ./src/components/forms/FormConfirm/styles.js ***!
  \****************************************************/
/*! exports provided: Stylesheet, TextField, Input, Title, Error, Wrapper, LinkForgot, ContainerSocial */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextField", function() { return TextField; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Input", function() { return Input; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Title", function() { return Title; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Error", function() { return Error; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Wrapper", function() { return Wrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkForgot", function() { return LinkForgot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContainerSocial", function() { return ContainerSocial; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");





const Stylesheet = {
  form: {
    width: '70%',
    minHeight: '50vh',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'gray',
    borderRadius: 5,
    elevation: 10,
    boxShadow: '10px 10px 5px 0px rgba(0,0,0,0.75)',
    justifyContent: 'space-between'
  },
  viewHeader: {
    width: '100%',
    minHeight: '5vh',
    height: '10%',
    justifyContent: 'space-between'
  },
  viewLanguage: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  viewClose: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].black
  },
  iconClose: {
    fontSize: 40
  },
  title: {
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].black
  },
  viewFields: {
    width: '100%',
    minHeight: '20vh',
    height: '40%',
    justifyContent: 'space-around' // backgroundColor: 'yellow',

  },
  rowField: {
    width: '100%',
    minHeight: '10vh',
    height: '50%',
    flexDirection: 'row',
    justifyContent: 'center' // backgroundColor: 'green',

  },
  viewSocial: {
    width: '100%',
    minHeight: '7vh',
    height: '10%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  rowForgot: {
    justifyContent: 'flex-end'
  },
  textForgot: {
    color: 'blue',
    textDecoration: 'undeline',
    marginRight: '15%',
    marginBottom: '3%'
  }
}; //Box Container for TextInputs

const TextField = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["withStyles"])({
  root: {
    width: '70%',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].MUIColors.blueGrey,
    fontSize: 30 // backgroundColor: Colors.primary,

  }
})(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__["default"]);
const Input = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].input`
  border: 1px solid #ccc;
  border-radius: 3px;
  margin-bottom: 5%;
  padding: 8px 4px;
  display: block;
  width: 80%;
  margin-left: 10%;

  &[disabled] {
    opacity: .3;
  };
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isBrowser"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
        ${''
/* margin-left: 25%; */
}
      `};
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`

        ${''
/* margin-left: 10%; */
}
      `};
`;
const Title = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].h2`
  font-size: 16px;
  font-weight: 500;
  padding: 8px 0;
`;
const Error = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].span`
  color: red;
  font-size: 14px;
`;
const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
    @media only screen and (max-width : 399px) {
        width: 10%
    }
`;
const LinkForgot = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
  display: block; 
  right: 0;
  color: blue;
  font-size: 14px;
  margin-right: 8%;
  margin-left: auto;
  margin-bottom: 2%;
`;
const ContainerSocial = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  display: block;
`;

/***/ }),

/***/ "./src/components/forms/FormForgot/index.js":
/*!**************************************************!*\
  !*** ./src/components/forms/FormForgot/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../hooks/useInputValue */ "./src/hooks/useInputValue.js");
/* harmony import */ var components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/buttons/ButtonLoading */ "./src/components/buttons/ButtonLoading/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./styles */ "./src/components/forms/FormForgot/styles.js");
/* harmony import */ var components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/bars/LanguageBar */ "./src/components/bars/LanguageBar/index.js");
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
/* harmony import */ var _material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/InputAdornment */ "./node_modules/@material-ui/core/esm/InputAdornment/index.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons */ "./node_modules/@material-ui/icons/esm/index.js");
/* harmony import */ var components_forms_Form__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! components/forms/Form */ "./src/components/forms/Form/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/forms/FormForgot/index.js";















const STEP_1_CODE = 1;
const STEP_2_PASSWORD = 2;
const STEP_3_CONFIRM = 3;
/* harmony default export */ __webpack_exports__["default"] = (({
  error,
  disabled,
  onSubmit,
  onCancel,
  title,
  loading,
  step
}) => {
  const email = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const password = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const confirmPassword = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const code = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_10__["useLanguageContext"])();

  const handleSubmit = event => {
    event.preventDefault(); // setIsLoading(true)

    onSubmit(email.value, password.value, code.value);
    return function cleanup() {};
  }; //


  const handleValidate = event => {
    if (step === STEP_1_CODE) {
      return email.value.length > 0;
    } else if (step === STEP_2_PASSWORD) {
      // Step 2
      return code.value.length > 0 && password.value.length > 0 && password.value === confirmPassword.value;
    }

    return true;
  }; //!


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_Form__WEBPACK_IMPORTED_MODULE_12__["default"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].form,
    disabled: disabled,
    onSubmit: handleSubmit,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].viewHeader,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].viewLanguage,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_6__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 11
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].viewClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_7__["default"], {
    onClick: onCancel,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].textClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 13
    }
  }, translate('COMMON.CANCEL')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9___default.a, {
    style: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].iconClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 13
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].title,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 11
    }
  }, title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].viewFields,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 9
    }
  }, step === STEP_1_CODE && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('COMMON.EMAIL'),
    placeholder: translate('COMMON.EMAIL'),
    variant: "outlined",
    type: "email"
  }, email, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_8__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88,
          columnNumber: 21
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["MailOutline"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89,
          columnNumber: 23
        }
      }))
    },
    disabled: disabled,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 13
    }
  }))), step === STEP_2_PASSWORD && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('FORM.FORGOT.CODE'),
    placeholder: translate('FORM.FORGOT.CODE'),
    variant: "outlined",
    type: "phone"
  }, code, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_8__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110,
          columnNumber: 23
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["Dialpad"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111,
          columnNumber: 25
        }
      }))
    },
    disabled: disabled,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 15
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('COMMON.PASSWORD'),
    placeholder: translate('COMMON.PASSWORD'),
    variant: "outlined",
    type: "password"
  }, password, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_8__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129,
          columnNumber: 23
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["Lock"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130,
          columnNumber: 25
        }
      }))
    },
    disabled: disabled,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119,
      columnNumber: 15
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 137,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('FORM.FORGOT.CONFIRM_PASSWORD'),
    placeholder: translate('FORM.FORGOT.CONFIRM_PASSWORD'),
    variant: "outlined",
    type: "password"
  }, confirmPassword, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_8__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 148,
          columnNumber: 23
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons__WEBPACK_IMPORTED_MODULE_11__["Lock"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149,
          columnNumber: 25
        }
      }))
    },
    disabled: disabled,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 138,
      columnNumber: 15
    }
  })))), step === STEP_3_CONFIRM && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 159,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 13
    }
  }, translate('FORM.FORGOT.SUCCESS'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_2__["ButtonLoading"], {
    loading: loading,
    disabled: !handleValidate(),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 163,
      columnNumber: 9
    }
  }, step === STEP_1_CODE ? translate('FORM.FORGOT.BUTTON_EMAIL') : step === STEP_2_PASSWORD ? translate('FORM.FORGOT.BUTTON_CHANGE') : step === STEP_3_CONFIRM ? translate('FORM.FORGOT.BUTTON_RETURN') : ''), error && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].error,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 172,
      columnNumber: 19
    }
  }, error)));
});

/***/ }),

/***/ "./src/components/forms/FormForgot/styles.js":
/*!***************************************************!*\
  !*** ./src/components/forms/FormForgot/styles.js ***!
  \***************************************************/
/*! exports provided: Stylesheet, TextField */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextField", function() { return TextField; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
// import { isMobile, isBrowser } from 'react-device-detect';



const Stylesheet = {
  form: {
    width: '70%',
    minHeight: '50vh',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'gray',
    borderRadius: 5,
    elevation: 10,
    boxShadow: '10px 10px 5px 0px rgba(0,0,0,0.75)',
    justifyContent: 'space-between',
    display: 'flex'
  },
  viewHeader: {
    width: '100%',
    minHeight: '5vh',
    height: '10%',
    justifyContent: 'space-between'
  },
  viewLanguage: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  viewClose: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].black
  },
  iconClose: {
    fontSize: 40
  },
  title: {
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].black
  },
  viewFields: {
    width: '100%',
    minHeight: '20vh',
    height: '40%',
    justifyContent: 'space-around' // backgroundColor: 'yellow',

  },
  rowField: {
    width: '100%',
    minHeight: '10vh',
    height: '50%',
    flexDirection: 'row',
    justifyContent: 'center' // backgroundColor: 'green',

  },
  viewSocial: {
    width: '100%',
    minHeight: '7vh',
    height: '10%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  rowForgot: {
    justifyContent: 'flex-end'
  },
  textForgot: {
    color: 'blue',
    textDecoration: 'undeline',
    marginRight: '15%',
    marginBottom: '3%'
  },
  error: {
    color: 'red'
  }
}; //Box Container for TextInputs

const TextField = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: {
    width: '70%',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].MUIColors.blueGrey,
    fontSize: 30 // backgroundColor: Colors.primary,

  }
})(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/components/forms/FormLogin/index.js":
/*!*************************************************!*\
  !*** ./src/components/forms/FormLogin/index.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../hooks/useInputValue */ "./src/hooks/useInputValue.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/forms/FormLogin/styles.js");
/* harmony import */ var components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/buttons/ButtonLoading */ "./src/components/buttons/ButtonLoading/index.js");
/* harmony import */ var components_buttons_ButtonSocial__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/buttons/ButtonSocial */ "./src/components/buttons/ButtonSocial/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_forms_Form__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/forms/Form */ "./src/components/forms/Form/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! components/bars/LanguageBar */ "./src/components/bars/LanguageBar/index.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/InputAdornment */ "./node_modules/@material-ui/core/esm/InputAdornment/index.js");
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/icons/AccountCircle */ "./node_modules/@material-ui/icons/AccountCircle.js");
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons/LockOpen */ "./node_modules/@material-ui/icons/LockOpen.js");
/* harmony import */ var _material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/forms/FormLogin/index.js";















/* harmony default export */ __webpack_exports__["default"] = (({
  error,
  disabled,
  onSubmit,
  onSocial,
  onForgot,
  onCancel,
  title,
  loading,
  textCancel = 'return'
}) => {
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  const email = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const password = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');

  const handleSubmit = event => {
    event.preventDefault();
    onSubmit(email.value, password.value);
  };

  const handleSocial = type => {
    onSocial(type);
  }; //


  const handleValidate = event => {
    return email.value.length > 0 && password.value.length > 0;
  }; //!


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_Form__WEBPACK_IMPORTED_MODULE_6__["default"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].form,
    disabled: disabled,
    onSubmit: handleSubmit,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewHeader,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewLanguage,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_9__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_14__["default"], {
    onClick: onCancel,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_7__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].textClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 15
    }
  }, translate('FORM.LOGIN.CANCEL')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_10___default.a, {
    style: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].iconClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 15
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_7__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].title,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 11
    }
  }, title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["View"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewFields,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('FORM.LOGIN.USERNAME'),
    placeholder: translate('FORM.LOGIN.USERNAME'),
    variant: "outlined"
  }, email, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_11__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 19
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_12___default.a, {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 21
        }
      }))
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 13
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["TextField"], Object.assign({
    id: "outlined-password-input",
    label: translate('FORM.LOGIN.PASSWORD'),
    type: "password",
    placeholder: translate('FORM.LOGIN.PASSWORD'),
    autoComplete: "current-password",
    variant: "outlined"
  }, password, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_11__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94,
          columnNumber: 19
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_13___default.a, {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95,
          columnNumber: 21
        }
      }))
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 13
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowForgot,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_14__["default"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].textForgot,
    onClick: onForgot,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 11
    }
  }, translate('FORM.LOGIN.FORGOT'))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_3__["ButtonLoading"], {
    loading: loading,
    disabled: !handleValidate(),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107,
      columnNumber: 9
    }
  }, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_8__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewSocial,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_ButtonSocial__WEBPACK_IMPORTED_MODULE_4__["ButtonSocial"], {
    disabled: false,
    onClick: () => {
      handleSocial('Facebook');
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109,
      columnNumber: 11
    }
  }, translate('FORM.LOGIN.FACEBOOK')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_ButtonSocial__WEBPACK_IMPORTED_MODULE_4__["ButtonSocial"], {
    disabled: false,
    onClick: () => {
      handleSocial('Google');
    },
    google: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110,
      columnNumber: 11
    }
  }, translate('FORM.LOGIN.GOOGLE')))), error && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["Error"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114,
      columnNumber: 17
    }
  }, error));
});

/***/ }),

/***/ "./src/components/forms/FormLogin/styles.js":
/*!**************************************************!*\
  !*** ./src/components/forms/FormLogin/styles.js ***!
  \**************************************************/
/*! exports provided: Stylesheet, TextField, Input, Title, Error, Wrapper, LinkForgot, ContainerSocial */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextField", function() { return TextField; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Input", function() { return Input; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Title", function() { return Title; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Error", function() { return Error; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Wrapper", function() { return Wrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkForgot", function() { return LinkForgot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContainerSocial", function() { return ContainerSocial; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");





const Stylesheet = {
  form: {
    width: '70%',
    minHeight: '50vh',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'gray',
    borderRadius: 5,
    elevation: 10,
    boxShadow: '10px 10px 5px 0px rgba(0,0,0,0.75)'
  },
  viewHeader: {
    width: '100%',
    minHeight: '5vh',
    height: '10%',
    justifyContent: 'space-between'
  },
  viewLanguage: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  viewClose: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].black
  },
  iconClose: {
    fontSize: 40
  },
  title: {
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].black
  },
  viewFields: {
    width: '100%',
    minHeight: '20vh',
    height: '40%',
    justifyContent: 'space-around' // backgroundColor: 'yellow',

  },
  rowField: {
    width: '100%',
    minHeight: '10vh',
    height: '50%',
    flexDirection: 'row',
    justifyContent: 'center' // backgroundColor: 'green',

  },
  viewSocial: {
    width: '100%',
    minHeight: '7vh',
    height: '10%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  rowForgot: {
    justifyContent: 'flex-end'
  },
  textForgot: {
    color: 'blue',
    textDecoration: 'undeline',
    marginRight: '15%',
    marginBottom: '3%'
  }
}; //Box Container for TextInputs

const TextField = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["withStyles"])({
  root: {
    width: '70%',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].MUIColors.blueGrey,
    fontSize: 30 // backgroundColor: Colors.primary,

  }
})(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__["default"]);
const Input = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].input`
  border: 1px solid #ccc;
  border-radius: 3px;
  margin-bottom: 5%;
  padding: 8px 4px;
  display: block;
  width: 80%;
  margin-left: 10%;

  &[disabled] {
    opacity: .3;
  };
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isBrowser"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
        ${''
/* margin-left: 25%; */
}
      `};
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`

        ${''
/* margin-left: 10%; */
}
      `};
`;
const Title = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].h2`
  font-size: 16px;
  font-weight: 500;
  padding: 8px 0;
`;
const Error = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].span`
  color: red;
  font-size: 14px;
`;
const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
    @media only screen and (max-width : 399px) {
        width: 10%
    }
`;
const LinkForgot = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
  display: block; 
  right: 0;
  color: blue;
  font-size: 14px;
  margin-right: 8%;
  margin-left: auto;
  margin-bottom: 2%;
`;
const ContainerSocial = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  display: block;
`;

/***/ }),

/***/ "./src/components/forms/FormSignup/index.js":
/*!**************************************************!*\
  !*** ./src/components/forms/FormSignup/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../hooks/useInputValue */ "./src/hooks/useInputValue.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/forms/FormSignup/styles.js");
/* harmony import */ var components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/buttons/ButtonLoading */ "./src/components/buttons/ButtonLoading/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_forms_Form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! components/forms/Form */ "./src/components/forms/Form/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! components/bars/LanguageBar */ "./src/components/bars/LanguageBar/index.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/InputAdornment */ "./node_modules/@material-ui/core/esm/InputAdornment/index.js");
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons/AccountCircle */ "./node_modules/@material-ui/icons/AccountCircle.js");
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/icons/LockOpen */ "./node_modules/@material-ui/icons/LockOpen.js");
/* harmony import */ var _material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/forms/FormSignup/index.js";














/* harmony default export */ __webpack_exports__["default"] = (({
  error,
  disabled,
  onSubmit,
  onCancel,
  title,
  loading,
  textCancel
}) => {
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_4__["useLanguageContext"])();
  const email = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const password = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');
  const confirmPassword = Object(_hooks_useInputValue__WEBPACK_IMPORTED_MODULE_1__["useInputValue"])('');

  const handleSubmit = event => {
    event.preventDefault(); // setIsLoading(true)

    onSubmit(email.value, password.value);
    return function cleanup() {};
  }; //


  const handleValidate = event => {
    return email.value.length > 0 && password.value.length > 0 && password.value === confirmPassword.value;
  }; //!


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_Form__WEBPACK_IMPORTED_MODULE_5__["default"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].form,
    disabled: disabled,
    onSubmit: handleSubmit,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewHeader,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewLanguage,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LanguageBar__WEBPACK_IMPORTED_MODULE_8__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 13
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Column"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_13__["default"], {
    onClick: onCancel,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_6__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].textClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 15
    }
  }, translate('FORM.SIGNUP.CANCEL')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_9___default.a, {
    style: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].iconClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 15
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_6__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].title,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 11
    }
  }, title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["View"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].viewFields,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["TextField"], Object.assign({
    required: true,
    id: "outlined-required",
    label: translate('COMMON.USERNAME'),
    placeholder: translate('COMMON.USERNAME'),
    variant: "outlined"
  }, email, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_10__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 19
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_11___default.a, {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78,
          columnNumber: 21
        }
      }))
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 13
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["TextField"], Object.assign({
    id: "outlined-password-input",
    label: translate('COMMON.PASSWORD'),
    type: "password",
    placeholder: translate('COMMON.PASSWORD'),
    autoComplete: "none",
    variant: "outlined"
  }, password, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_10__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95,
          columnNumber: 19
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_12___default.a, {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96,
          columnNumber: 21
        }
      }))
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 13
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowField,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["TextField"], Object.assign({
    id: "outlined-password-input",
    label: translate('FORM.SIGNUP.CONFIRM_PASSWORD'),
    type: "password",
    placeholder: translate('FORM.SIGNUP.CONFIRM_PASSWORD'),
    autoComplete: "none",
    variant: "outlined"
  }, confirmPassword, {
    InputProps: {
      startAdornment: /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_10__["default"], {
        position: "start",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113,
          columnNumber: 19
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_icons_LockOpen__WEBPACK_IMPORTED_MODULE_12___default.a, {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114,
          columnNumber: 21
        }
      }))
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 13
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_7__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].rowForgot,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 121,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_ButtonLoading__WEBPACK_IMPORTED_MODULE_3__["ButtonLoading"], {
    loading: loading,
    disabled: !handleValidate(),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 122,
      columnNumber: 11
    }
  }, title))), error && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["Error"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 125,
      columnNumber: 17
    }
  }, error));
});

/***/ }),

/***/ "./src/components/forms/FormSignup/styles.js":
/*!***************************************************!*\
  !*** ./src/components/forms/FormSignup/styles.js ***!
  \***************************************************/
/*! exports provided: Stylesheet, TextField, Input, Title, Error, Wrapper, LinkForgot, ContainerSocial */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextField", function() { return TextField; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Input", function() { return Input; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Title", function() { return Title; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Error", function() { return Error; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Wrapper", function() { return Wrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkForgot", function() { return LinkForgot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContainerSocial", function() { return ContainerSocial; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/TextField */ "./node_modules/@material-ui/core/esm/TextField/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");





const Stylesheet = {
  form: {
    width: '70%',
    minHeight: '50vh',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'gray',
    borderRadius: 5,
    elevation: 10,
    boxShadow: '10px 10px 5px 0px rgba(0,0,0,0.75)',
    justifyContent: 'space-between'
  },
  viewHeader: {
    width: '100%',
    minHeight: '5vh',
    height: '10%',
    justifyContent: 'space-between'
  },
  viewLanguage: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  viewClose: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].black
  },
  iconClose: {
    fontSize: 40
  },
  title: {
    color: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].black
  },
  viewFields: {
    width: '100%',
    minHeight: '20vh',
    height: '40%',
    justifyContent: 'space-around' // backgroundColor: 'yellow',

  },
  rowField: {
    width: '100%',
    minHeight: '10vh',
    height: '50%',
    flexDirection: 'row',
    justifyContent: 'center' // backgroundColor: 'green',

  },
  viewSocial: {
    width: '100%',
    minHeight: '7vh',
    height: '10%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  rowForgot: {
    justifyContent: 'flex-end'
  },
  textForgot: {
    color: 'blue',
    textDecoration: 'undeline',
    marginRight: '15%',
    marginBottom: '3%'
  }
}; //Box Container for TextInputs

const TextField = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["withStyles"])({
  root: {
    width: '70%',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].MUIColors.blueGrey,
    fontSize: 30 // backgroundColor: Colors.primary,

  }
})(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__["default"]);
const Input = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].input`
  border: 1px solid #ccc;
  border-radius: 3px;
  margin-bottom: 5%;
  padding: 8px 4px;
  display: block;
  width: 80%;
  margin-left: 10%;

  &[disabled] {
    opacity: .3;
  };
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isBrowser"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
        ${''
/* margin-left: 25%; */
}
      `};
  ${react_device_detect__WEBPACK_IMPORTED_MODULE_1__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`

        ${''
/* margin-left: 10%; */
}
      `};
`;
const Title = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].h2`
  font-size: 16px;
  font-weight: 500;
  padding: 8px 0;
`;
const Error = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].span`
  color: red;
  font-size: 14px;
`;
const Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
    @media only screen and (max-width : 399px) {
        width: 10%
    }
`;
const LinkForgot = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].button`
  display: block; 
  right: 0;
  color: blue;
  font-size: 14px;
  margin-right: 8%;
  margin-left: auto;
  margin-bottom: 2%;
`;
const ContainerSocial = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  display: block;
`;

/***/ }),

/***/ "./src/components/forms/index.js":
/*!***************************************!*\
  !*** ./src/components/forms/index.js ***!
  \***************************************/
/*! exports provided: FormLogin, FormSignup, FormConfirm, FormForgot */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FormLogin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FormLogin */ "./src/components/forms/FormLogin/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormLogin", function() { return _FormLogin__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _FormSignup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FormSignup */ "./src/components/forms/FormSignup/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormSignup", function() { return _FormSignup__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _FormConfirm___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FormConfirm/ */ "./src/components/forms/FormConfirm/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormConfirm", function() { return _FormConfirm___WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _FormForgot__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./FormForgot */ "./src/components/forms/FormForgot/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormForgot", function() { return _FormForgot__WEBPACK_IMPORTED_MODULE_3__["default"]; });







/***/ }),

/***/ "./src/components/images/Avatar/index.js":
/*!***********************************************!*\
  !*** ./src/components/images/Avatar/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UserAvatar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/components/images/Avatar/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/images/Avatar/index.js";


function UserAvatar(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["View"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["Avatar"], {
    src: props.photo ? props.photo : '/broken',
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 7
    }
  }, props.letter && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["Span"], {
    big: props.big ? true : false,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 11
    }
  }, props.letter.toUpperCase())));
}

/***/ }),

/***/ "./src/components/images/Avatar/styles.js":
/*!************************************************!*\
  !*** ./src/components/images/Avatar/styles.js ***!
  \************************************************/
/*! exports provided: useStyles, View, Avatar, Span */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useStyles", function() { return useStyles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "View", function() { return View; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Avatar", function() { return Avatar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Span", function() { return Span; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var _material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Avatar */ "./node_modules/@material-ui/core/esm/Avatar/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/images/Avatar/styles.js";

 // Material UI


 // Components
// Global Styles

 //

const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])({
  avatar: {
    margin: 10
  },
  letterAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Pallette.salmon
  },
  purpleAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Pallette.gold
  }
});
const View = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Center,
    height: '100%',
    width: '100%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"]);
const Avatar = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Column.Center,
    height: '100%',
    width: '100%',
    borderRadius: 10,
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_4__["Colors"].Presets.SupraHeader.Initial.background
  }
})(_material_ui_core_Avatar__WEBPACK_IMPORTED_MODULE_3__["default"]);
const Span = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: {},
  label: {
    fontSize: '300%'
  },
  small: {
    fontSize: 16
  }
})(({
  classes,
  children,
  big
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
  className: big ? classes.label : classes.small,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 58,
    columnNumber: 5
  }
}, children));

/***/ }),

/***/ "./src/components/images/Logo/index.js":
/*!*********************************************!*\
  !*** ./src/components/images/Logo/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/components/images/Logo/styles.js");
/* harmony import */ var _assets_logo_BoletSportsPNG_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../assets/logo/BoletSportsPNG.png */ "./src/assets/logo/BoletSportsPNG.png");
/* harmony import */ var _assets_logo_BoletSportsPNG_png__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_logo_BoletSportsPNG_png__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/images/Logo/index.js";




const Logo = () => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_styles__WEBPACK_IMPORTED_MODULE_1__["Image"], {
    src: _assets_logo_BoletSportsPNG_png__WEBPACK_IMPORTED_MODULE_2___default.a,
    alt: "Bolet Sports Logo",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 5
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Logo);

/***/ }),

/***/ "./src/components/images/Logo/styles.js":
/*!**********************************************!*\
  !*** ./src/components/images/Logo/styles.js ***!
  \**********************************************/
/*! exports provided: Image */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Image", function() { return Image; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");

const Image = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].img`
  ${''
/* width: '100%';
height: '100%'; */
}
  margin: 10px;
  width: 70%;
  height: 70%;
`;

/***/ }),

/***/ "./src/components/layouts/LandingLayout/index.js":
/*!*******************************************************!*\
  !*** ./src/components/layouts/LandingLayout/index.js ***!
  \*******************************************************/
/*! exports provided: LandingLayout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingLayout", function() { return LandingLayout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_bars_Header__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/bars/Header */ "./src/components/bars/Header/index.js");
/* harmony import */ var _bars_NavBar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../bars/NavBar */ "./src/components/bars/NavBar/index.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/layouts/LandingLayout/index.js";


 //!COMPONENTS


 // import LeftButtonNav from '../../../components/bars/LeftButtonNav'
// import { Div } from './styles'


 //

const LandingLayout = ({
  children,
  authenticated,
  title,
  description,
  onNavBar,
  balance
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }
  }, title && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 19
    }
  }, title, " | Bolet Sports"), description && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: description,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["BrowserView"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["PageContainer"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_Header__WEBPACK_IMPORTED_MODULE_4__["default"], {
    authenticated: authenticated,
    onNavBar: onNavBar,
    balance: balance,
    onLogin: () => console.log('Login'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 11
    }
  }, children))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["MobileView"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_bars_NavBar__WEBPACK_IMPORTED_MODULE_5__["NavBar"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, children)));
};

/***/ }),

/***/ "./src/components/layouts/LoginLayout/index.js":
/*!*****************************************************!*\
  !*** ./src/components/layouts/LoginLayout/index.js ***!
  \*****************************************************/
/*! exports provided: LoginLayout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginLayout", function() { return LoginLayout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./styles */ "./src/components/layouts/LoginLayout/styles.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_6__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/layouts/LoginLayout/index.js";


 //!COMPONENTS




 //

const LoginLayout = ({
  children,
  authenticated,
  title,
  description,
  onNavBar,
  balance
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }
  }, title && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 19
    }
  }, title, " | Bolet Sports"), description && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: description,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["BrowserView"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["PageContainer"], {
    id: 'Login.PageContainer',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["LoginContainer"], {
    id: 'Login.LoginContainer',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["LeftBox"], {
    id: 'Login.LeftBox',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].title,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 15
    }
  }, title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_5__["Stylesheet"].description,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 15
    }
  }, description)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_5__["RightBox"], {
    id: 'Login.RightBox',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 13
    }
  }, children)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_device_detect__WEBPACK_IMPORTED_MODULE_6__["MobileView"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 9
    }
  }, children)));
};

/***/ }),

/***/ "./src/components/layouts/LoginLayout/styles.js":
/*!******************************************************!*\
  !*** ./src/components/layouts/LoginLayout/styles.js ***!
  \******************************************************/
/*! exports provided: LoginContainer, LeftBox, RightBox, Stylesheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginContainer", function() { return LoginContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeftBox", function() { return LeftBox; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RightBox", function() { return RightBox; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
 // Material UI

 // Global Styles

 //Container

const LoginContainer = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: {
    width: '100%',
    height: '100vh',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].background,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]); //Container

const LeftBox = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: {
    width: '50%',
    height: '100vh',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].app,
    ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Column.Center
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]); //Container

const RightBox = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: {
    width: '50%',
    minWidth: 800,
    height: '100vh',
    backgroundColor: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].background,
    ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Column.Center
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]);
const Stylesheet = {
  title: {
    fontSize: 50,
    fontWeight: 800,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].white,
    width: '70%',
    textAlign: 'center'
  },
  description: {
    fontSize: 30,
    fontWeight: 600,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].white,
    width: '70%',
    textAlign: 'center',
    marginBottom: '10vh'
  }
};

/***/ }),

/***/ "./src/components/layouts/StoreLayout/index.js":
/*!*****************************************************!*\
  !*** ./src/components/layouts/StoreLayout/index.js ***!
  \*****************************************************/
/*! exports provided: StoreLayout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreLayout", function() { return StoreLayout; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var components_bars_LeftButtonNav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/bars/LeftButtonNav */ "./src/components/bars/LeftButtonNav/index.js");
/* harmony import */ var components_bars_NavBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/bars/NavBar */ "./src/components/bars/NavBar/index.js");
/* harmony import */ var components_bars_Header__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! components/bars/Header */ "./src/components/bars/Header/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_bars_SupraHeader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! components/bars/SupraHeader */ "./src/components/bars/SupraHeader/index.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var pages_landing_HomeBody_styles__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! pages/landing/HomeBody/styles */ "./src/pages/landing/HomeBody/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/layouts/StoreLayout/index.js";










const StoreLayout = ({
  children,
  authenticated,
  title,
  subtitle,
  sports,
  itemSelected,
  onSelect,
  onNavBar,
  bodyPage
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_helmet__WEBPACK_IMPORTED_MODULE_1__["Helmet"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }
  }, title && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 19
    }
  }, title, " | Bolet Sports"), subtitle && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "description",
    content: subtitle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 22
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_device_detect__WEBPACK_IMPORTED_MODULE_8__["BrowserView"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_6__["PageContainer"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 9
    }
  }, authenticated && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_SupraHeader__WEBPACK_IMPORTED_MODULE_7__["default"], {
    id: 'SupraHeader',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 29
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_Header__WEBPACK_IMPORTED_MODULE_5__["default"], {
    authenticated: authenticated,
    onNavBar: onNavBar,
    balance: 100,
    onBalance: () => console.log('Login'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 11
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_6__["View"], {
    id: 'Body',
    styles: pages_landing_HomeBody_styles__WEBPACK_IMPORTED_MODULE_9__["Stylesheet"].body,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_LeftButtonNav__WEBPACK_IMPORTED_MODULE_3__["default"], {
    secondary: sports,
    onSelect: onSelect,
    itemSelected: itemSelected,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 13
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_6__["Column"], {
    styles: {
      width: 'inherit'
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 13
    }
  }, children)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_device_detect__WEBPACK_IMPORTED_MODULE_8__["MobileView"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_bars_NavBar__WEBPACK_IMPORTED_MODULE_4__["NavBar"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 11
    }
  })));
};

/***/ }),

/***/ "./src/components/menus/ProductsMenu/index.js":
/*!****************************************************!*\
  !*** ./src/components/menus/ProductsMenu/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../styles */ "./src/styles/index.js");
/* harmony import */ var _components_buttons_ButtonProduct__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/buttons/ButtonProduct */ "./src/components/buttons/ButtonProduct/index.js");
/* harmony import */ var _data_products_json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../data/products.json */ "./src/data/products.json");
var _data_products_json__WEBPACK_IMPORTED_MODULE_6___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../data/products.json */ "./src/data/products.json", 1);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/menus/ProductsMenu/index.js";







const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  root: { ..._styles__WEBPACK_IMPORTED_MODULE_4__["Styles"].Row.Start,
    ..._styles__WEBPACK_IMPORTED_MODULE_4__["Metrics"].SupraHeader.Menu.Container
  }
})); // Main Component

const ProductsMenu = ({
  itemSelected = _data_products_json__WEBPACK_IMPORTED_MODULE_6__[0],
  onClick
}) => {
  const classes = useStyles();
  const [item, setItem] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(itemSelected); //

  const handleClick = id => {
    const xItem = _data_products_json__WEBPACK_IMPORTED_MODULE_6__.filter(function (x) {
      return x.id === id;
    });
    const retItem = xItem && xItem.length > 0 ? xItem[0] : _data_products_json__WEBPACK_IMPORTED_MODULE_6__[0];
    setItem(retItem);
    onClick(retItem);
  }; //Render Items


  const renderButtons = () => {
    return _data_products_json__WEBPACK_IMPORTED_MODULE_6__.map(product => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_buttons_ButtonProduct__WEBPACK_IMPORTED_MODULE_5__["ButtonProduct"], {
        key: product.id,
        item: product,
        onClick: handleClick,
        selected: item.id === product.id,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39,
          columnNumber: 14
        }
      });
    });
  }; //Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_2__["default"], {
    className: classes.root,
    id: "ProductsMenu--Grid.root",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 5
    }
  }, renderButtons());
};

ProductsMenu.propTypes = {
  itemSelected: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (ProductsMenu);

/***/ }),

/***/ "./src/components/texts/H1/index.js":
/*!******************************************!*\
  !*** ./src/components/texts/H1/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Paragraph__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Paragraph */ "./src/components/texts/Paragraph/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styles */ "./src/components/texts/H1/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/H1/index.js";



 //

const H1 = ({
  children,
  ...props
}) => {
  const style = { ..._styles__WEBPACK_IMPORTED_MODULE_3__["Stylesheet"].h1,
    ...props.styles
  }; // console.log(style);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Paragraph__WEBPACK_IMPORTED_MODULE_2__["default"], {
    styles: style,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 10
    }
  }, children);
}; //


H1.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (H1);

/***/ }),

/***/ "./src/components/texts/H1/styles.js":
/*!*******************************************!*\
  !*** ./src/components/texts/H1/styles.js ***!
  \*******************************************/
/*! exports provided: Stylesheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styles */ "./src/styles/index.js");

const Stylesheet = {
  h1: {
    color: styles__WEBPACK_IMPORTED_MODULE_0__["Colors"].app,
    ...styles__WEBPACK_IMPORTED_MODULE_0__["Fonts"].h1,
    textAlign: 'center',
    margin: 0
  }
};

/***/ }),

/***/ "./src/components/texts/H2/index.js":
/*!******************************************!*\
  !*** ./src/components/texts/H2/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Span__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Span */ "./src/components/texts/Span/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styles */ "./src/components/texts/H2/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/H2/index.js";



 //

const H2 = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Span__WEBPACK_IMPORTED_MODULE_2__["default"], Object.assign({
    styles: _styles__WEBPACK_IMPORTED_MODULE_3__["Stylesheet"].h2
  }, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 10
    }
  }), children);
}; //


H2.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (H2);

/***/ }),

/***/ "./src/components/texts/H2/styles.js":
/*!*******************************************!*\
  !*** ./src/components/texts/H2/styles.js ***!
  \*******************************************/
/*! exports provided: Stylesheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styles */ "./src/styles/index.js");

const Stylesheet = {
  h2: {
    color: styles__WEBPACK_IMPORTED_MODULE_0__["Colors"].app,
    ...styles__WEBPACK_IMPORTED_MODULE_0__["Fonts"].h2
  }
};

/***/ }),

/***/ "./src/components/texts/Paragraph/index.js":
/*!*************************************************!*\
  !*** ./src/components/texts/Paragraph/index.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/texts/Paragraph/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/Paragraph/index.js";


 //

const Paragraph = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["ParagraphStyled"], Object.assign({}, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 10
    }
  }), children);
}; //


Paragraph.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Paragraph);

/***/ }),

/***/ "./src/components/texts/Paragraph/styles.js":
/*!**************************************************!*\
  !*** ./src/components/texts/Paragraph/styles.js ***!
  \**************************************************/
/*! exports provided: ParagraphStyled */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParagraphStyled", function() { return ParagraphStyled; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/Paragraph/styles.js";



const ParagraphStyled = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Fonts"].p,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].text
  },
  styles: props => ({ ...props.styles
  })
})(({
  classes,
  children
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
  className: `${classes.root} ${classes.styles}`,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/texts/Span/index.js":
/*!********************************************!*\
  !*** ./src/components/texts/Span/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/texts/Span/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/Span/index.js";


 //

const Span = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["SpanStyled"], Object.assign({}, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 10
    }
  }), children);
}; //


Span.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Span);

/***/ }),

/***/ "./src/components/texts/Span/styles.js":
/*!*********************************************!*\
  !*** ./src/components/texts/Span/styles.js ***!
  \*********************************************/
/*! exports provided: SpanStyled */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpanStyled", function() { return SpanStyled; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/Span/styles.js";



const SpanStyled = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Fonts"].span,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].text
  },
  styles: props => ({ ...props.styles
  })
})(({
  classes,
  children
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
  className: `${classes.root} ${classes.styles}`,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/texts/Text/index.js":
/*!********************************************!*\
  !*** ./src/components/texts/Text/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Span__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Span */ "./src/components/texts/Span/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styles */ "./src/components/texts/Text/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/texts/Text/index.js";



 //

const Text = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Span__WEBPACK_IMPORTED_MODULE_2__["default"], Object.assign({
    styles: _styles__WEBPACK_IMPORTED_MODULE_3__["Stylesheet"].text
  }, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 10
    }
  }), children);
}; //


Text.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Text);

/***/ }),

/***/ "./src/components/texts/Text/styles.js":
/*!*********************************************!*\
  !*** ./src/components/texts/Text/styles.js ***!
  \*********************************************/
/*! exports provided: Stylesheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styles */ "./src/styles/index.js");

const Stylesheet = {
  text: {
    color: styles__WEBPACK_IMPORTED_MODULE_0__["Colors"].app
  }
};

/***/ }),

/***/ "./src/components/texts/index.js":
/*!***************************************!*\
  !*** ./src/components/texts/index.js ***!
  \***************************************/
/*! exports provided: Paragraph, Span, Text, H1, H2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Paragraph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Paragraph */ "./src/components/texts/Paragraph/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Paragraph", function() { return _Paragraph__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _Span__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Span */ "./src/components/texts/Span/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Span", function() { return _Span__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _Text__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Text */ "./src/components/texts/Text/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Text", function() { return _Text__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _H1__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./H1 */ "./src/components/texts/H1/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "H1", function() { return _H1__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _H2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./H2 */ "./src/components/texts/H2/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "H2", function() { return _H2__WEBPACK_IMPORTED_MODULE_4__["default"]; });








/***/ }),

/***/ "./src/components/view/Column/index.js":
/*!*********************************************!*\
  !*** ./src/components/view/Column/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/view/Column/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/Column/index.js";


 //

const Column = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["ColumnStyled"], Object.assign({}, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 10
    }
  }), children);
}; //


Column.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Column);

/***/ }),

/***/ "./src/components/view/Column/styles.js":
/*!**********************************************!*\
  !*** ./src/components/view/Column/styles.js ***!
  \**********************************************/
/*! exports provided: ColumnStyled */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColumnStyled", function() { return ColumnStyled; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/Column/styles.js";




const ColumnStyled = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Fonts"].span,
    ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Row.Start,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].text
  },
  styles: props => ({ ...props.styles
  })
})(({
  classes,
  children
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
  className: `${classes.root} ${classes.styles}`,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/view/PageContainer/index.js":
/*!****************************************************!*\
  !*** ./src/components/view/PageContainer/index.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/view/PageContainer/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/PageContainer/index.js";




const PageContainer = ({
  children
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["Container"], {
    disableGutters: true,
    maxWidth: false,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 5
    }
  }, children);
};

PageContainer.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (PageContainer);

/***/ }),

/***/ "./src/components/view/PageContainer/styles.js":
/*!*****************************************************!*\
  !*** ./src/components/view/PageContainer/styles.js ***!
  \*****************************************************/
/*! exports provided: Container */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Container", function() { return Container; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Container__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Container */ "./node_modules/@material-ui/core/esm/Container/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../styles */ "./src/styles/index.js");
 // Material UI

 // Global Styles

 //Container

const Container = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: {
    width: '100%',
    backgroundColor: _styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].background
  }
})(_material_ui_core_Container__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/components/view/Row/index.js":
/*!******************************************!*\
  !*** ./src/components/view/Row/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/view/Row/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/Row/index.js";


 //

const Row = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["RowStyled"], Object.assign({}, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 10
    }
  }), children);
}; //


Row.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Row);

/***/ }),

/***/ "./src/components/view/Row/styles.js":
/*!*******************************************!*\
  !*** ./src/components/view/Row/styles.js ***!
  \*******************************************/
/*! exports provided: RowStyled */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RowStyled", function() { return RowStyled; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/Row/styles.js";




const RowStyled = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Fonts"].span,
    ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Row.Start,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].text
  },
  styles: props => ({ ...props.styles
  })
})(({
  classes,
  children
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
  className: `${classes.root} ${classes.styles}`,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/view/View/index.js":
/*!*******************************************!*\
  !*** ./src/components/view/View/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/components/view/View/styles.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/View/index.js";


 //

const View = ({
  children,
  ...props
}) => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_2__["Box"], Object.assign({}, props, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 10
    }
  }), children);
}; //


View.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./src/components/view/View/styles.js":
/*!********************************************!*\
  !*** ./src/components/view/View/styles.js ***!
  \********************************************/
/*! exports provided: Box */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Box", function() { return Box; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/components/view/View/styles.js";




const Box = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Fonts"].span,
    ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Column.Center,
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].text
  },
  styles: props => ({ ...props.styles
  })
})(({
  classes,
  children
}) => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_3__["default"], {
  className: `${classes.root} ${classes.styles}`,
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 7
  }
}, children));

/***/ }),

/***/ "./src/components/view/index.js":
/*!**************************************!*\
  !*** ./src/components/view/index.js ***!
  \**************************************/
/*! exports provided: PageContainer, View, Column, Row */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PageContainer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PageContainer */ "./src/components/view/PageContainer/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageContainer", function() { return _PageContainer__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _View__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./View */ "./src/components/view/View/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "View", function() { return _View__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _Column__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Column */ "./src/components/view/Column/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Column", function() { return _Column__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _Row__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Row */ "./src/components/view/Row/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Row", function() { return _Row__WEBPACK_IMPORTED_MODULE_3__["default"]; });







/***/ }),

/***/ "./src/containers/localize/i18n.js":
/*!*****************************************!*\
  !*** ./src/containers/localize/i18n.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var i18next__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! i18next */ "./node_modules/i18next/dist/esm/i18next.js");
/* harmony import */ var i18next_xhr_backend__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! i18next-xhr-backend */ "./node_modules/i18next-xhr-backend/dist/esm/i18nextXHRBackend.js");
/* harmony import */ var react_i18next__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");



i18next__WEBPACK_IMPORTED_MODULE_0__["default"].use(i18next_xhr_backend__WEBPACK_IMPORTED_MODULE_1__["default"]).use(react_i18next__WEBPACK_IMPORTED_MODULE_2__["initReactI18next"]).init({
  lng: 'en',
  backend: {
    /* translation file path */
    loadPath: '/assets/i18n/{{ns}}/{{lng}}.json'
  },
  fallbackLng: 'en',
  debug: false,

  /* can have multiple namespace, in case you want to divide a huge translation into smaller pieces and load them on demand */
  ns: ['translations'],
  defaultNS: 'translations',
  keySeparator: false,
  interpolation: {
    escapeValue: false,
    formatSeparator: ','
  },
  react: {
    wait: true
  }
});
/* harmony default export */ __webpack_exports__["default"] = (i18next__WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/contexts/AppContext.js":
/*!************************************!*\
  !*** ./src/contexts/AppContext.js ***!
  \************************************/
/*! exports provided: AppContext, AppContextProvider, useAppContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppContext", function() { return AppContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppContextProvider", function() { return AppContextProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useAppContext", function() { return useAppContext; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");
/* harmony import */ var _graphql_queries__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../graphql/queries */ "./src/graphql/queries.js");
/* harmony import */ var data_sports_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! data/sports.json */ "./src/data/sports.json");
var data_sports_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! data/sports.json */ "./src/data/sports.json", 1);
/* harmony import */ var data_ligues_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! data/ligues.json */ "./src/data/ligues.json");
var data_ligues_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! data/ligues.json */ "./src/data/ligues.json", 1);
/* harmony import */ var data_matchs_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! data/matchs.json */ "./src/data/matchs.json");
var data_matchs_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! data/matchs.json */ "./src/data/matchs.json", 1);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/contexts/AppContext.js";

/* eslint-disable react-hooks/exhaustive-deps */







const AppContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(null);
const AppContextProvider = ({
  children
}) => {
  const [isLoading, setLoading] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true);
  const [sports, setSports] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState([]);
  const [disconnected, setDisconnected] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true);
  const [ligues, setLigues] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState([]);
  const [matchs, setMatchs] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState([]);
  const [liguesResult, setLiguesResult] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState([]);
  const [matchsResult, setMatchsResult] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState([]); //

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    const handleConnection = () => {
      const condition = navigator.onLine ? 'online' : 'offline';

      if (condition === 'online') {
        const webPing = setInterval(() => {
          fetch('//google.com', {
            mode: 'no-cors'
          }).then(() => {
            console.log('Internet Connected');
            setDisconnected(false);
            return clearInterval(webPing);
          }).catch(() => {
            console.log('Internet Offline');
            setDisconnected(true);
          });
        }, 2000);
        return;
      }

      console.log('Internet Disconnected');
      return setDisconnected(true);
    }; //start Listeners


    window.addEventListener('online', handleConnection);
    window.addEventListener('offline', handleConnection);
    setLoading(false); //component will unmount

    return () => {
      console.log('remove event listener');
      window.removeEventListener('online', handleConnection);
      window.removeEventListener('offline', handleConnection);
    };
  }, []); //

  const loadSports = async () => {
    setLoading(true);

    try {
      const apiResponse = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["API"].graphql(Object(aws_amplify__WEBPACK_IMPORTED_MODULE_1__["graphqlOperation"])(_graphql_queries__WEBPACK_IMPORTED_MODULE_2__["listSports"]));
      const localSports = apiResponse.data.listSports.items.map(item => {
        const resultLocalData = data_sports_json__WEBPACK_IMPORTED_MODULE_3__.filter(function (el) {
          return el.id === item.id;
        });
        const localData = resultLocalData && resultLocalData.length > 0 ? resultLocalData[0] : data_sports_json__WEBPACK_IMPORTED_MODULE_3__[0];
        return { ...item,
          local: localData
        };
      }); // console.log('localSports')
      // console.log(localSports)

      setSports(localSports);
      await loadLigues();
      await loadMatchs();
      console.log('AppCOntext End of Load');
    } catch (err) {
      console.log(err);
    }

    setLoading(false);
  }; //


  const loadLigues = async () => {
    setLoading(true);

    try {
      let liguesResponse = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["API"].graphql(Object(aws_amplify__WEBPACK_IMPORTED_MODULE_1__["graphqlOperation"])(_graphql_queries__WEBPACK_IMPORTED_MODULE_2__["listLigues"]));
      let liguesItems = liguesResponse.data.listLigues.items;

      for (; liguesResponse.data.listLigues.items.length >= 10 && liguesResponse.data.listLigues.nextToken;) {
        liguesResponse = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["API"].graphql(Object(aws_amplify__WEBPACK_IMPORTED_MODULE_1__["graphqlOperation"])(_graphql_queries__WEBPACK_IMPORTED_MODULE_2__["listLigues"], {
          nextToken: liguesResponse.data.listLigues.nextToken
        }));
        liguesItems.push(...liguesResponse.data.listLigues.items);
      }

      const localLigues = liguesItems.map(item => {
        let resultLocalData = data_ligues_json__WEBPACK_IMPORTED_MODULE_4__.find(function (el) {
          return el.id === item.id;
        }); // console.log('item.id');
        // console.log(item.id);
        // console.log('resultLocalData');
        // console.log(resultLocalData);

        if (!resultLocalData) {
          resultLocalData = data_ligues_json__WEBPACK_IMPORTED_MODULE_4__.find(function (el) {
            return el.id === item.sportID;
          });
        }

        const localData = resultLocalData ? resultLocalData : data_ligues_json__WEBPACK_IMPORTED_MODULE_4__[0]; // console.log('localData');
        // console.log(localData);

        return { ...item,
          local: localData
        };
      }); // console.log('localLigues')
      // console.log(localLigues)

      setLigues(localLigues);
      console.log('read ligues');
      console.log(ligues);
    } catch (err) {
      console.log(err);
    }

    setLoading(false);
  }; //


  const loadMatchs = async () => {
    setLoading(true);

    try {
      let matchsResponse = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["API"].graphql(Object(aws_amplify__WEBPACK_IMPORTED_MODULE_1__["graphqlOperation"])(_graphql_queries__WEBPACK_IMPORTED_MODULE_2__["listMatchs"]));
      let matchsItems = matchsResponse.data.listMatchs.items;

      for (; matchsResponse.data.listMatchs.items.length >= 10 && matchsResponse.data.listMatchs.nextToken;) {
        matchsResponse = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["API"].graphql(Object(aws_amplify__WEBPACK_IMPORTED_MODULE_1__["graphqlOperation"])(_graphql_queries__WEBPACK_IMPORTED_MODULE_2__["listMatchs"], {
          nextToken: matchsResponse.data.listMatchs.nextToken
        }));
        matchsItems.push(...matchsResponse.data.listMatchs.items);
      }

      const localMatchs = matchsItems.map(item => {
        let resultLocalData = data_matchs_json__WEBPACK_IMPORTED_MODULE_5__.find(function (el) {
          return el.id === item.teams[0];
        });

        if (!resultLocalData) {
          resultLocalData = data_ligues_json__WEBPACK_IMPORTED_MODULE_4__.find(function (el) {
            return el.id === item.ligueID;
          });
        }

        const localData = resultLocalData ? resultLocalData : data_ligues_json__WEBPACK_IMPORTED_MODULE_4__[0];
        resultLocalData = data_matchs_json__WEBPACK_IMPORTED_MODULE_5__.find(function (el) {
          return el.id === item.teams[1];
        });

        if (!resultLocalData) {
          resultLocalData = data_ligues_json__WEBPACK_IMPORTED_MODULE_4__.find(function (el) {
            return el.id === item.ligueID;
          });
        }

        const awayData = resultLocalData ? resultLocalData : data_ligues_json__WEBPACK_IMPORTED_MODULE_4__[0];
        return { ...item,
          local: {
            home: localData,
            away: awayData
          }
        };
      }); // console.log('localMatchs')
      // console.log(localMatchs)

      setMatchs(localMatchs);
    } catch (err) {
      console.log(err);
    }

    setLoading(false);
  }; //


  const getLiguesBySportId = id => {
    const filtered = ligues.filter(x => x.sportID === id);
    setLiguesResult(filtered);
    return filtered;
  }; //


  const getMatchsByLigueId = id => {
    const filtered = matchs.filter(x => x.ligueID === id);
    setMatchsResult(filtered);
    return filtered;
  }; //


  const values = react__WEBPACK_IMPORTED_MODULE_0___default.a.useMemo(() => ({
    isLoading,
    sports,
    ligues,
    matchs,
    disconnected,
    liguesResult,
    matchsResult,
    loadSports,
    loadLigues,
    loadMatchs,
    getLiguesBySportId,
    getMatchsByLigueId
  }), [isLoading, sports, ligues, matchs, disconnected, liguesResult, matchsResult]); // Finally, return the interface that we want to expose to our other components

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AppContext.Provider, {
    value: values,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 221,
      columnNumber: 10
    }
  }, children); //<AppContext.Provider value={{ isAuthenticated, userHasAuthenticated, isAuthenticating }}>
};
function useAppContext() {
  const context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(AppContext);
  return context;
}

/***/ }),

/***/ "./src/contexts/AuthContext.js":
/*!*************************************!*\
  !*** ./src/contexts/AuthContext.js ***!
  \*************************************/
/*! exports provided: AuthContext, AuthContextProvider, useAuthContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthContext", function() { return AuthContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthContextProvider", function() { return AuthContextProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useAuthContext", function() { return useAuthContext; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");
/* harmony import */ var _handlers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../handlers */ "./src/handlers/index.js");
/* harmony import */ var _helpers_onErrorAuth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/onErrorAuth */ "./src/helpers/onErrorAuth.js");
/* harmony import */ var _aws_exports__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../aws-exports */ "./src/aws-exports.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/contexts/AuthContext.js";

/* eslint-disable react-hooks/exhaustive-deps */






const AuthContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(null);
const AuthContextProvider = ({
  children
}) => {
  // const history = useHistory();
  const [isAuthenticating, setIsAuthenticating] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true);
  const [isAuthenticated, userHasAuthenticated] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const [user, setUser] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  const [userData, setUserData] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    // Configure the keys needed for the Auth module. Essentially this is 
    // like calling `Amplify.configure` but only for `Auth`.
    aws_amplify__WEBPACK_IMPORTED_MODULE_1__["Amplify"].configure(_aws_exports__WEBPACK_IMPORTED_MODULE_4__["default"]); //HUB Listen

    aws_amplify__WEBPACK_IMPORTED_MODULE_1__["Hub"].listen('auth', async data => {
      const {
        payload
      } = data;

      if (payload.event === 'signIn') {
        userHasAuthenticated(true);

        try {
          const resultAuth = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["Auth"].currentAuthenticatedUser();
          authSetUserData(resultAuth); // setUser(resultAuth);

          userHasAuthenticated(true);
        } catch (err) {
          console.log(err);
          userHasAuthenticated(false);
        }
      } else if (payload.event === 'signOut') {
        userHasAuthenticated(false);
      }
    });

    const onLoad = async () => {
      try {
        const resultAuth = await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["Auth"].currentAuthenticatedUser();
        authSetUserData(resultAuth); // setUser(resultAuth);

        userHasAuthenticated(true);
      } catch (err) {
        console.log(err);
        userHasAuthenticated(false);
      }
    };

    onLoad();
    setIsAuthenticating(false);
  }, []);

  const authSetUserData = authUserData => {
    let name = authUserData ? authUserData.attributes ? authUserData.attributes.name ? authUserData.attributes.name : authUserData.attributes.email ? authUserData.attributes.email.split('@')[0] : 'Name' : 'Loading...' : 'Loading...';
    let attributes = { ...authUserData.attributes,
      name: name
    };
    setUser(authUserData);
    setUserData(attributes);
  }; //


  const authLogout = async () => {
    try {
      await aws_amplify__WEBPACK_IMPORTED_MODULE_1__["Auth"].signOut();
    } catch (err) {//
    }

    userHasAuthenticated(false);
  };

  const authFederated = async type => {
    try {
      await Object(_handlers__WEBPACK_IMPORTED_MODULE_2__["federatedSignin"])(type);
    } catch (err) {
      console.log(err);
    }
  };

  const authLogin = async (username, password) => {
    const result = await Object(_handlers__WEBPACK_IMPORTED_MODULE_2__["cognitoSignin"])(username, password);

    if (result.data) {
      userHasAuthenticated(true);
      authSetUserData(result.data); // setUser(result.data);
    }

    return result;
  }; // Submit button on Login Form.


  const authSignup = async (username, password) => {
    const result = await Object(_handlers__WEBPACK_IMPORTED_MODULE_2__["cognitoSignup"])(username, password);

    if (result.code !== 'success') {
      Object(_helpers_onErrorAuth__WEBPACK_IMPORTED_MODULE_3__["onError"])(result.message);
    }

    return result;
  }; // Submit button on Login Form.


  const authConfirm = async (username, password, code) => {
    const result = await Object(_handlers__WEBPACK_IMPORTED_MODULE_2__["cognitoConfirm"])(username, code);

    if (result.data) {
      if (password) {
        const resultLogin = await Object(_handlers__WEBPACK_IMPORTED_MODULE_2__["cognitoSignin"])(username, password);
        userHasAuthenticated(true);

        if (resultLogin.code !== 'success') {
          Object(_helpers_onErrorAuth__WEBPACK_IMPORTED_MODULE_3__["onErrorAuth"])(resultLogin.message);
        }

        authSetUserData(resultLogin.data); // setUser(resultLogin.data);

        return { ...resultLogin,
          authorized: resultLogin.code === 'success',
          confirmed: true
        };
      }
    }

    return { ...result,
      authorized: false,
      confirmed: result.code === 'success'
    };
  }; //


  const authForgot = async (username, password, code) => {
    const result = await Object(_handlers__WEBPACK_IMPORTED_MODULE_2__["cognitoForgot"])(username, password, code);

    if (result.data !== 'success') {
      Object(_helpers_onErrorAuth__WEBPACK_IMPORTED_MODULE_3__["onErrorAuth"])(result.message);
    }

    return result;
  }; //


  const values = react__WEBPACK_IMPORTED_MODULE_0___default.a.useMemo(() => ({
    user,
    userData,
    isAuthenticating,
    isAuthenticated,
    userHasAuthenticated,
    setIsAuthenticating,
    authLogout,
    authFederated,
    authLogin,
    authSignup,
    authConfirm,
    authForgot
  }), [user, userData, isAuthenticating, isAuthenticated]); // Finally, return the interface that we want to expose to our other components

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AuthContext.Provider, {
    value: values,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 181,
      columnNumber: 10
    }
  }, children); //<AuthContext.Provider value={{ isAuthenticated, userHasAuthenticated, isAuthenticating }}>
};
function useAuthContext() {
  const context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(AuthContext);
  return context;
}

/***/ }),

/***/ "./src/contexts/LanguageContext.js":
/*!*****************************************!*\
  !*** ./src/contexts/LanguageContext.js ***!
  \*****************************************/
/*! exports provided: LanguageContext, LanguageContextProvider, useLanguageContext */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageContext", function() { return LanguageContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageContextProvider", function() { return LanguageContextProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useLanguageContext", function() { return useLanguageContext; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_i18next__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");
/* harmony import */ var _containers_localize_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../containers/localize/i18n */ "./src/containers/localize/i18n.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/contexts/LanguageContext.js";




const LanguageContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(() => {
  const {
    i18n
  } = Object(react_i18next__WEBPACK_IMPORTED_MODULE_1__["useTranslation"])();
  i18n.changeLanguage(localStorage.getItem('local.language'));
});
const LanguageContextProvider = ({
  children
}) => {
  const [init, setInit] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const [isLoadingLanguage, setLoading] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true);
  const [language, setLanguage] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(() => {
    const x = localStorage.getItem('local.language');
    return x ? x : 'en';
  });
  const {
    t,
    i18n
  } = Object(react_i18next__WEBPACK_IMPORTED_MODULE_1__["useTranslation"])(); //

  const constructor = () => {
    if (!init) {
      if (language === null) {
        setLanguage('en');
        console.log('setLanguage');
        console.log(language);

        if (language) {
          localStorage.setItem('local.language', language);
        } else localStorage.setItem('local.language', 'en');
      }

      setInit(true);
    }
  };

  constructor(); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    const localStorageLanguage = async () => {
      await i18n.changeLanguage(language, () => {
        setLoading(false);
      });
    };

    localStorageLanguage(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); //On Update language

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    localStorage.setItem('local.language', language); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [language]);

  const onChange = async lang => {
    await i18n.changeLanguage(lang, () => {
      setLanguage(lang);
      return lang;
    });
  }; //.


  const translate = text => {
    if (i18n.language !== language) {
      i18n.changeLanguage(language, () => {
        return t(text);
      });
      return;
    }

    const retValue = t(text);
    return retValue ? retValue : '';
  }; //


  const values = react__WEBPACK_IMPORTED_MODULE_0___default.a.useMemo(() => ({
    language,
    isLoadingLanguage,
    onChange,
    translate
  }), // eslint-disable-next-line react-hooks/exhaustive-deps
  [language, isLoadingLanguage]); // Finally, return the interface that we want to expose to our other components

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(LanguageContext.Provider, {
    value: values,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 10
    }
  }, children);
};
function useLanguageContext() {
  const context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(LanguageContext);
  return context;
}

/***/ }),

/***/ "./src/data/languages.js":
/*!*******************************!*\
  !*** ./src/data/languages.js ***!
  \*******************************/
/*! exports provided: languages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "languages", function() { return languages; });
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers */ "./src/helpers/index.js");

const languages = [{
  "id": "es",
  "tag": "spanish",
  "label": "language.es",
  "short": "spa",
  "abrev": "sp",
  "icon": "ES",
  "flag": Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/flags/016-spain.svg'),
  "countryCode": "ES"
}, {
  "id": "en",
  "tag": "english",
  "label": "language.en",
  "short": "eng",
  "abrev": "en",
  "icon": "US",
  "flag": Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/flags/226-united-states.svg'),
  "countryCode": "US"
}, {
  "id": "fr",
  "tag": "french",
  "label": "language.fr",
  "short": "fra",
  "abrev": "fr",
  "icon": "FR",
  "flag": Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/flags/019-france.svg'),
  "countryCode": "FR"
}, {
  "id": "pr",
  "tag": "portugiese",
  "label": "language.pr",
  "short": "por",
  "abrev": "pr",
  "icon": "PR",
  "flag": Object(_helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/flags/011-brazil.svg'),
  "countryCode": "BR"
}];

/***/ }),

/***/ "./src/data/ligues.json":
/*!******************************!*\
  !*** ./src/data/ligues.json ***!
  \******************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"\",\"url\":\"https://img2.pngio.com/team-sport-silhouette-png-transparent-png-838x4123676186-sports-silhouettes-png-840_492.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"soccer\",\"url\":\"https://cdn.pixabay.com/photo/2013/07/13/10/49/ball-157860_960_720.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"mma\",\"url\":\"https://cdn.pixabay.com/photo/2016/03/31/17/46/fitness-1293891_960_720.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"americanfootball\",\"url\":\"https://cdn.pixabay.com/photo/2017/01/31/23/28/football-2028191_960_720.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"germany_bundesliga\",\"url\":\"https://www.soccertimes.com/wp-content/uploads/2018/05/680_58fdddf9d27e3.jpg\",\"image\":\"\",\"title\":\"Germany BundesLiga\",\"icon\":\"bundesliga\"},{\"id\":\"germany_bundesliga2\",\"url\":\"https://upload.wikimedia.org/wikipedia/en/thumb/7/7b/2._Bundesliga_logo.svg/480px-2._Bundesliga_logo.svg.png\",\"image\":\"\",\"title\":\"Germany Bundesliga 2\",\"icon\":\"bundesliga2\"},{\"id\":\"denmark_superliga\",\"url\":\"https://www.logofootball.net/wp-content/uploads/danish-superliga-logo.png\",\"image\":\"\",\"title\":\"Denmark SuperLigue\",\"icon\":\"denmark_superliga\"},{\"id\":\"korea_kleague1\",\"url\":\"https://upload.wikimedia.org/wikipedia/en/thumb/3/3d/K_League.png/180px-K_League.png\",\"image\":\"\",\"title\":\"Korean K Ligue\",\"icon\":\"korea_kleague1\"},{\"id\":\"mixed_martial_arts\",\"url\":\"https://radtintandgraphics.com/storage/2019/03/mixed-martial-arts-mma-logo.jpg\",\"image\":\"\",\"title\":\"Mixed Martial Arts\",\"icon\":\"mixed_martial_arts\"},{\"id\":\"ncaaf\",\"url\":\"https://www.futbol-americano.com/wp-content/uploads/2018/10/NCAAF-1024x576.png\",\"image\":\"\",\"title\":\"NCA Football\",\"icon\":\"ncaff\"},{\"id\":\"nfl\",\"url\":\"https://a4.espncdn.com/combiner/i?img=%2Fi%2Fespn%2Fmisc_logos%2F500%2Fnfl.png\",\"image\":\"\",\"title\":\"National Football League\",\"icon\":\"nfl\"}]");

/***/ }),

/***/ "./src/data/matchs.json":
/*!******************************!*\
  !*** ./src/data/matchs.json ***!
  \******************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"soccer\",\"url\":\"https://cdn.pixabay.com/photo/2013/07/13/10/49/ball-157860_960_720.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"mma\",\"url\":\"https://finalfightchampionship.com/Media/_noprofile.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"americanfootball\",\"url\":\"https://cdn.pixabay.com/photo/2017/01/31/23/28/football-2028191_960_720.png\",\"image\":\"\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"Chase Sherman\",\"url\":\"https://a.espncdn.com/combiner/i?img=/i/headshots/mma/players/full/3952937.png&w=350&h=254https://a.espncdn.com/combiner/i?img=/i/headshots/mma/players/full/3952937.png&w=350&h=254\",\"image\":\"\",\"title\":\"Chase Sherman\",\"icon\":\"mma_chase_sherman\"},{\"id\":\"Ike Villanueva\",\"url\":\"https://a.espncdn.com/combiner/i?img=/i/headshots/mma/players/full/2618069.png&w=350&h=254\",\"image\":\"\",\"title\":\"Ike Villanueva2\",\"icon\":\"mma_ike_villanueva\"},{\"id\":\"Brian Kelleher\",\"url\":\"https://a.espncdn.com/combiner/i?img=/i/headshots/mma/players/full/2517186.png\",\"image\":\"\",\"title\":\"Brian Kelleher\",\"icon\":\"mma_brian_kelleher\"},{\"id\":\"Hunter Azure\",\"url\":\"https://a.espncdn.com/combiner/i?img=/i/headshots/mma/players/full/4425706.png\",\"image\":\"\",\"title\":\"NCA Football\",\"icon\":\"mma_hunter_azure\"},{\"id\":\"Gabriel Benitez\",\"url\":\"https://a.espncdn.com/combiner/i?img=/i/headshots/mma/players/full/3032218.png&w=350&h=254\",\"image\":\"\",\"title\":\"Gabriel Benitez\",\"icon\":\"mma_grabriel_benitez\"}]");

/***/ }),

/***/ "./src/data/products.json":
/*!********************************!*\
  !*** ./src/data/products.json ***!
  \********************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"sports\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"icon\":\"sports\"},{\"id\":\"lottery\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"icon\":\"ballot\"}]");

/***/ }),

/***/ "./src/data/sports.json":
/*!******************************!*\
  !*** ./src/data/sports.json ***!
  \******************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"\",\"icon\":\"emoji_events\"},{\"id\":\"basketball\",\"url\":\"https://cdn.pixabay.com/photo/2016/07/27/01/36/basketball-1544370_960_720.jpg\",\"title\":\"Basketball\",\"icon\":\"sports_basketball\"},{\"id\":\"baseball\",\"url\":\"https://cdn.pixabay.com/photo/2016/08/25/03/28/baseball-1618655_960_720.jpg\",\"title\":\"Baseball\",\"icon\":\"sports_baseball\"},{\"id\":\"soccer\",\"url\":\"https://cdn.pixabay.com/photo/2016/07/01/07/17/soccer-1490541_960_720.jpg\",\"title\":\"Soccer\",\"icon\":\"sports_soccer\"},{\"id\":\"mma\",\"url\":\"https://cdn.pixabay.com/photo/2016/04/07/19/07/mixed-martial-arts-1314503_960_720.jpg\",\"title\":\"Mixed Martial Arts\",\"icon\":\"sports_kabaddi\"},{\"id\":\"boxing\",\"url\":\"https://cdn.pixabay.com/photo/2012/10/25/23/32/box-62867_960_720.jpg\",\"title\":\"Boxing\",\"icon\":\"sports_mma\"},{\"id\":\"americanfootball\",\"url\":\"https://cdn.pixabay.com/photo/2012/11/28/11/11/quarterback-67701_960_720.jpg\",\"title\":\"American Football\",\"icon\":\"sports_football\"},{\"id\":\"handball\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"HandBall\",\"icon\":\"sports_handball\"},{\"id\":\"swimming\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Swim\",\"icon\":\"pool\"},{\"id\":\"golf\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Gold\",\"icon\":\"golf_course\"},{\"id\":\"weights\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Weights\",\"icon\":\"fitness_center\"},{\"id\":\"biking\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Bike\",\"icon\":\"directions_bike\"},{\"id\":\"rowing\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Rowing\",\"icon\":\"rowing\"},{\"id\":\"tennis\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Tennis\",\"icon\":\"sports_tennis\"},{\"id\":\"rugby\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Rugby\",\"icon\":\"sports_rugby\"},{\"id\":\"volleyball\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Volleyball\",\"icon\":\"sports_volleyball\"},{\"id\":\"hockey\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Hockey\",\"icon\":\"sports_hockey\"},{\"id\":\"motorsports\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"MotorSports\",\"icon\":\"sports_motorsports\"},{\"id\":\"gymnastics\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Gymnastics\",\"icon\":\"accessibility_new\"},{\"id\":\"elections\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Elections\",\"icon\":\"ballot\"},{\"id\":\"horseracing\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Horse Racing\",\"icon\":\"flag\"},{\"id\":\"running\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Running\",\"icon\":\"directions_run\"},{\"id\":\"nasscar\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Nasscar\",\"icon\":\"local_car_wash\"},{\"id\":\"stockoptions\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Stock Options\",\"icon\":\"poll\"},{\"id\":\"darts\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Darts\",\"icon\":\"track_changes\"},{\"id\":\"dogs\",\"url\":\"https://images.pexels.com/photos/3571569/pexels-photo-3571569.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940\",\"title\":\"Dogs Racing\",\"icon\":\"pets\"}]");

/***/ }),

/***/ "./src/graphql/queries.js":
/*!********************************!*\
  !*** ./src/graphql/queries.js ***!
  \********************************/
/*! exports provided: getSport, listSports, getLigue, listLigues, getMatch, listMatchs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSport", function() { return getSport; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "listSports", function() { return listSports; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLigue", function() { return getLigue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "listLigues", function() { return listLigues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMatch", function() { return getMatch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "listMatchs", function() { return listMatchs; });
/* eslint-disable */
// this is an auto generated file. This will be overwritten
const getSport =
/* GraphQL */
`
  query GetSport($id: ID!) {
    getSport(id: $id) {
      id
      name
      sports {
        key
        active
        group
        details
        title
        has_outrights
      }
      ligues {
        items {
          id
          name
          sportID
        }
        nextToken
      }
    }
  }
`;
const listSports =
/* GraphQL */
`
  query ListSports(
    $filter: ModelSportFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSports(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        sports {
          key
          active
          group
          details
          title
          has_outrights
        }
        ligues {
          nextToken
        }
      }
      nextToken
    }
  }
`;
const getLigue =
/* GraphQL */
`
  query GetLigue($id: ID!) {
    getLigue(id: $id) {
      id
      name
      sportID
      ligues {
        key
        active
        group
        details
        title
        has_outrights
      }
      sport {
        id
        name
        sports {
          key
          active
          group
          details
          title
          has_outrights
        }
        ligues {
          nextToken
        }
      }
      matches {
        items {
          id
          teams
          home
          commence_time
          region
          h2h
          h2h_lay
          ligueID
          odds
        }
        nextToken
      }
    }
  }
`;
const listLigues =
/* GraphQL */
`
  query ListLigues(
    $filter: ModelLigueFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLigues(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        sportID
        ligues {
          key
          active
          group
          details
          title
          has_outrights
        }
        sport {
          id
          name
        }
        matches {
          nextToken
        }
      }
      nextToken
    }
  }
`;
const getMatch =
/* GraphQL */
`
  query GetMatch($id: ID!) {
    getMatch(id: $id) {
      id
      teams
      home
      commence_time
      region
      h2h
      h2h_lay
      totals {
        points
        odds
        position
      }
      ligueID
      ligue {
        id
        name
        sportID
        ligues {
          key
          active
          group
          details
          title
          has_outrights
        }
        sport {
          id
          name
        }
        matches {
          nextToken
        }
      }
      odds
    }
  }
`;
const listMatchs =
/* GraphQL */
`
  query ListMatchs(
    $filter: ModelMatchFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMatchs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        teams
        home
        commence_time
        region
        h2h
        h2h_lay
        totals {
          points
          odds
          position
        }
        ligueID
        ligue {
          id
          name
          sportID
        }
        odds
      }
      nextToken
    }
  }
`;

/***/ }),

/***/ "./src/handlers/cognitoConfirm.js":
/*!****************************************!*\
  !*** ./src/handlers/cognitoConfirm.js ***!
  \****************************************/
/*! exports provided: cognitoConfirm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cognitoConfirm", function() { return cognitoConfirm; });
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");

const cognitoConfirm = async (username, confirmationCode) => {
  let ret = {
    code: 'error',
    data: null,
    message: 'Error al registrarse.'
  };

  try {
    const user = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Auth"].confirmSignUp(username.toLowerCase(), confirmationCode);
    return {
      code: 'success',
      data: user,
      message: 'Ingreso Exitoso'
    };
  } catch (err) {
    if (err.code === 'UserNotConfirmedException') {
      ret.code = err.code;
      ret.message = 'Usuario no ha confirmado su correo electronico.'; // The error happens if the user didn't finish the confirmation step when signing up
      // In this case you need to resend the code and confirm the user
      // About how to resend the code and confirm the user, please check the signUp part
    } else if (err.code === 'PasswordResetRequiredException') {
      ret.code = err.code;
      ret.message = 'Debe Cambiar su contraseña actual.'; // The error happens when the password is reset in the Cognito console
      // In this case you need to call forgotPassword to reset the password
      // Please check the Forgot Password part.
    } else if (err.code === 'NotAuthorizedException') {
      ret.code = err.code;
      ret.message = 'Contraseña incorrecta.'; // The error happens when the incorrect password is provided
    } else if (err.code === 'UserNotFoundException') {
      ret.code = err.code;
      ret.message = 'Usuario o Correo Electronico incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else if (err.code === 'CodeMismatchException') {
      ret.code = err.code;
      ret.message = 'Codigo de Verificacion Incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else {
      ret.message = 'Correo electronico no registrado.';
    }

    return ret;
  }
};

/***/ }),

/***/ "./src/handlers/cognitoForgot.js":
/*!***************************************!*\
  !*** ./src/handlers/cognitoForgot.js ***!
  \***************************************/
/*! exports provided: cognitoForgot */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cognitoForgot", function() { return cognitoForgot; });
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");

const cognitoForgot = async (email, password, code) => {
  let ret = {
    code: 'error',
    data: null,
    message: 'Error al registrarse.'
  };

  try {
    let user;

    if (code && password) {
      user = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Auth"].forgotPasswordSubmit(email.toLowerCase(), code, password);
      return {
        code: 'success',
        data: user || code,
        message: 'Contraseña restablecida exitosamente.'
      };
    } else {
      user = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Auth"].forgotPassword(email.toLowerCase());
      return {
        code: 'success',
        data: user,
        message: 'Se ha enviado un codigo de confirmacion a su correo'
      };
    }
  } catch (err) {
    if (err.code === 'UserNotConfirmedException') {
      ret.code = err.code;
      ret.message = 'Usuario no ha confirmado su correo electronico.'; // The error happens if the user didn't finish the confirmation step when signing up
      // In this case you need to resend the code and confirm the user
      // About how to resend the code and confirm the user, please check the signUp part
    } else if (err.code === 'PasswordResetRequiredException') {
      ret.code = err.code;
      ret.message = 'Debe Cambiar su contraseña actual.'; // The error happens when the password is reset in the Cognito console
      // In this case you need to call forgotPassword to reset the password
      // Please check the Forgot Password part.
    } else if (err.code === 'NotAuthorizedException') {
      ret.code = err.code;
      ret.message = 'Contraseña incorrecta.'; // The error happens when the incorrect password is provided
    } else if (err.code === 'UserNotFoundException') {
      ret.code = err.code;
      ret.message = 'Usuario o Correo Electronico incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else if (err.code === 'CodeMismatchException') {
      ret.code = err.code;
      ret.message = 'Codigo de Verificacion Incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else if (err.code === 'LimitExceededException') {
      ret.code = err.code;
      ret.message = 'Ha Excedido los Intentos, Intente mas tarde.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else {
      ret.message = 'Correo electronico no registrado.';
    }

    return ret;
  }
};

/***/ }),

/***/ "./src/handlers/cognitoSignin.js":
/*!***************************************!*\
  !*** ./src/handlers/cognitoSignin.js ***!
  \***************************************/
/*! exports provided: cognitoSignin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cognitoSignin", function() { return cognitoSignin; });
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");

const cognitoSignin = async (username, password) => {
  let ret = {
    code: 'error',
    data: null,
    message: 'Error al registrarse.'
  };

  try {
    const user = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Auth"].signIn(username.toLowerCase(), password); // if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
    //   const {requiredAttributes} = user.challengeParam;
    //   //const {username, email, phone_number} = getInfoFromUserInput();
    //   const loggedUser = await Auth.completeNewPassword(
    //     user, // the Cognito User Object
    //     password, // the new password
    //   );
    // }
    // console.log(user)

    return {
      code: 'success',
      data: user,
      // data: user.signInUserSession.accessToken.jwtToken,
      message: 'Ingreso Exitoso'
    };
  } catch (err) {
    console.log(err);

    if (err.code === 'UserNotConfirmedException') {
      ret.code = err.code;
      ret.message = 'Usuario no ha confirmado su correo electronico.'; // The error happens if the user didn't finish the confirmation step when signing up
      // In this case you need to resend the code and confirm the user
      // About how to resend the code and confirm the user, please check the signUp part
    } else if (err.code === 'PasswordResetRequiredException') {
      ret.code = err.code;
      ret.message = 'Debe Cambiar su contraseña actual.'; // The error happens when the password is reset in the Cognito console
      // In this case you need to call forgotPassword to reset the password
      // Please check the Forgot Password part.
    } else if (err.code === 'NotAuthorizedException') {
      ret.code = err.code;
      ret.message = 'Contraseña incorrecta.'; // The error happens when the incorrect password is provided
    } else if (err.code === 'UserNotFoundException') {
      ret.code = err.code;
      ret.message = 'Usuario o Correo Electronico incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else {
      ret.message = 'Correo electronico no registrado.';
    }

    return ret;
  }
};

/***/ }),

/***/ "./src/handlers/cognitoSignup.js":
/*!***************************************!*\
  !*** ./src/handlers/cognitoSignup.js ***!
  \***************************************/
/*! exports provided: cognitoSignup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cognitoSignup", function() { return cognitoSignup; });
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");

const cognitoSignup = async (username, password) => {
  let ret = {
    code: 'error',
    data: null,
    message: 'Error al registrarse.'
  };

  try {
    const user = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Auth"].signUp(username.toLowerCase(), password); // if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
    //   const {requiredAttributes} = user.challengeParam;
    //   //const {username, email, phone_number} = getInfoFromUserInput();
    //   const loggedUser = await Auth.completeNewPassword(
    //     user, // the Cognito User Object
    //     password, // the new password
    //   );
    // }

    return {
      code: 'success',
      data: user,
      message: 'Ingreso Exitoso'
    };
  } catch (err) {
    if (err.code === 'UserNotConfirmedException') {
      ret.code = err.code;
      ret.message = 'Usuario no ha confirmado su correo electronico.'; // The error happens if the user didn't finish the confirmation step when signing up
      // In this case you need to resend the code and confirm the user
      // About how to resend the code and confirm the user, please check the signUp part
    } else if (err.code === 'PasswordResetRequiredException') {
      ret.code = err.code;
      ret.message = 'Debe Cambiar su contraseña actual.'; // The error happens when the password is reset in the Cognito console
      // In this case you need to call forgotPassword to reset the password
      // Please check the Forgot Password part.
    } else if (err.code === 'NotAuthorizedException') {
      ret.code = err.code;
      ret.message = 'Contraseña incorrecta.'; // The error happens when the incorrect password is provided
    } else if (err.code === 'UserNotFoundException') {
      ret.code = err.code;
      ret.message = 'Usuario o Correo Electronico incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else if (err.code === 'UsernameExistsException') {
      ret.code = err.code;
      ret.message = 'Usuario Existe Actualmente.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else {
      ret.message = 'Correo electronico no registrado.';
    }

    return ret;
  }
};

/***/ }),

/***/ "./src/handlers/federatedSignin.js":
/*!*****************************************!*\
  !*** ./src/handlers/federatedSignin.js ***!
  \*****************************************/
/*! exports provided: federatedSignin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "federatedSignin", function() { return federatedSignin; });
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");

const federatedSignin = async type => {
  let ret = {
    code: 'error',
    data: null,
    message: 'Error al registrarse.'
  };

  try {
    const user = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Auth"].federatedSignIn({
      provider: type
    }); // const user = await Auth.federatedSignIn();
    // if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
    //   const {requiredAttributes} = user.challengeParam;
    //   //const {username, email, phone_number} = getInfoFromUserInput();
    //   const loggedUser = await Auth.completeNewPassword(
    //     user, // the Cognito User Object
    //     password, // the new password
    //   );
    // }

    return {
      code: 'success',
      data: user,
      message: 'Ingreso Exitoso'
    };
  } catch (err) {
    // console.log('awsAuth, cognitoSignin, err: ' + JSON.stringify(err))
    console.log(err);

    if (err.code === 'UserNotConfirmedException') {
      ret.code = err.code;
      ret.message = 'Usuario no ha confirmado su correo electronico.'; // The error happens if the user didn't finish the confirmation step when signing up
      // In this case you need to resend the code and confirm the user
      // About how to resend the code and confirm the user, please check the signUp part
    } else if (err.code === 'PasswordResetRequiredException') {
      ret.code = err.code;
      ret.message = 'Debe Cambiar su contraseña actual.'; // The error happens when the password is reset in the Cognito console
      // In this case you need to call forgotPassword to reset the password
      // Please check the Forgot Password part.
    } else if (err.code === 'NotAuthorizedException') {
      ret.code = err.code;
      ret.message = 'Contraseña incorrecta.'; // The error happens when the incorrect password is provided
    } else if (err.code === 'UserNotFoundException') {
      ret.code = err.code;
      ret.message = 'Usuario o Correo Electronico incorrecto.'; // The error happens when the supplied username/email does not exist in the Cognito user pool
    } else {
      ret.message = 'Correo electronico no registrado.';
    }

    return ret;
  }
};

/***/ }),

/***/ "./src/handlers/index.js":
/*!*******************************!*\
  !*** ./src/handlers/index.js ***!
  \*******************************/
/*! exports provided: cognitoSignin, cognitoSignup, cognitoConfirm, cognitoForgot, federatedSignin, S3Upload, S3GetFile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cognitoSignin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cognitoSignin */ "./src/handlers/cognitoSignin.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cognitoSignin", function() { return _cognitoSignin__WEBPACK_IMPORTED_MODULE_0__["cognitoSignin"]; });

/* harmony import */ var _cognitoSignup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cognitoSignup */ "./src/handlers/cognitoSignup.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cognitoSignup", function() { return _cognitoSignup__WEBPACK_IMPORTED_MODULE_1__["cognitoSignup"]; });

/* harmony import */ var _cognitoConfirm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cognitoConfirm */ "./src/handlers/cognitoConfirm.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cognitoConfirm", function() { return _cognitoConfirm__WEBPACK_IMPORTED_MODULE_2__["cognitoConfirm"]; });

/* harmony import */ var _cognitoForgot__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cognitoForgot */ "./src/handlers/cognitoForgot.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cognitoForgot", function() { return _cognitoForgot__WEBPACK_IMPORTED_MODULE_3__["cognitoForgot"]; });

/* harmony import */ var _federatedSignin__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./federatedSignin */ "./src/handlers/federatedSignin.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "federatedSignin", function() { return _federatedSignin__WEBPACK_IMPORTED_MODULE_4__["federatedSignin"]; });

/* harmony import */ var _storageS3__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./storageS3 */ "./src/handlers/storageS3.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "S3Upload", function() { return _storageS3__WEBPACK_IMPORTED_MODULE_5__["S3Upload"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "S3GetFile", function() { return _storageS3__WEBPACK_IMPORTED_MODULE_5__["S3GetFile"]; });









/***/ }),

/***/ "./src/handlers/storageS3.js":
/*!***********************************!*\
  !*** ./src/handlers/storageS3.js ***!
  \***********************************/
/*! exports provided: S3Upload, S3GetFile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S3Upload", function() { return S3Upload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S3GetFile", function() { return S3GetFile; });
/* harmony import */ var aws_amplify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! aws-amplify */ "./node_modules/aws-amplify/lib-esm/index.js");

const S3Upload = async (file, username) => {
  let filename;

  if (username) {
    filename = `${username}-${file.name}-${Date.now()}`;
  } else {
    filename = `${file.name}-${Date.now()}`;
  }

  try {
    const stored = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Storage"].vault.put(filename, file, {
      contentType: file.type
    });
    return stored;
  } catch (err) {
    console.log('S3Upload Error');
    console.log(err);
  }
};
const S3GetFile = async filename => {
  try {
    const file = await aws_amplify__WEBPACK_IMPORTED_MODULE_0__["Storage"].vault.get(filename);
    return file;
  } catch (err) {
    console.log('S3Upload Error');
    console.log(err);
  }
};

/***/ }),

/***/ "./src/helpers/AssetsHelpers.js":
/*!**************************************!*\
  !*** ./src/helpers/AssetsHelpers.js ***!
  \**************************************/
/*! exports provided: removeCSSClass, addCSSClass, toAbsoluteUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeCSSClass", function() { return removeCSSClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addCSSClass", function() { return addCSSClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toAbsoluteUrl", function() { return toAbsoluteUrl; });
function removeCSSClass(ele, cls) {
  const reg = new RegExp("(\\s|^)" + cls + "(\\s|$)");
  ele.className = ele.className.replace(reg, " ");
}
function addCSSClass(ele, cls) {
  ele.classList.add(cls);
}
const toAbsoluteUrl = pathname => "" + pathname;

/***/ }),

/***/ "./src/helpers/index.js":
/*!******************************!*\
  !*** ./src/helpers/index.js ***!
  \******************************/
/*! exports provided: removeCSSClass, addCSSClass, toAbsoluteUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AssetsHelpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AssetsHelpers */ "./src/helpers/AssetsHelpers.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "removeCSSClass", function() { return _AssetsHelpers__WEBPACK_IMPORTED_MODULE_0__["removeCSSClass"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addCSSClass", function() { return _AssetsHelpers__WEBPACK_IMPORTED_MODULE_0__["addCSSClass"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "toAbsoluteUrl", function() { return _AssetsHelpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"]; });

 // export * from "./LocalStorageHelpers";
// export * from "./RouterHelpers";
// export * from "./TablePaginationHelpers";
// export * from "./TableRowSelectionHelpers";
// export * from "./TableSortingHelpers";

/***/ }),

/***/ "./src/helpers/onErrorAuth.js":
/*!************************************!*\
  !*** ./src/helpers/onErrorAuth.js ***!
  \************************************/
/*! exports provided: onErrorAuth, onError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onErrorAuth", function() { return onErrorAuth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onError", function() { return onError; });
const onErrorAuth = error => {
  let message = 'Error!!!';

  if (error) {
    message = error.toString();
  }

  alert(message);
};
const onError = error => {
  let message = 'Error!!!';

  if (error) {
    message = error.toString();
  } // Auth errors


  if (!(error instanceof Error) && error.message) {
    message = error.message;
  }

  alert(message);
};

/***/ }),

/***/ "./src/hooks/useInputValue.js":
/*!************************************!*\
  !*** ./src/hooks/useInputValue.js ***!
  \************************************/
/*! exports provided: useInputValue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useInputValue", function() { return useInputValue; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

const useInputValue = initialValue => {
  const [value, setValue] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(initialValue);

  const onChange = e => setValue(e.target.value);

  return {
    value,
    onChange
  };
};

/***/ }),

/***/ "./src/index.css":
/*!***********************!*\
  !*** ./src/index.css ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.css */ "./src/index.css");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_index_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var _contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contexts/AppContext */ "./src/contexts/AppContext.js");
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./App */ "./src/App.js");
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/index.js";








 //

react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__["AuthContextProvider"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 3
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Suspense, {
  fallback: "loading...",
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 5
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["LanguageContextProvider"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 15,
    columnNumber: 7
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_6__["AppContextProvider"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 9
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["BrowserRouter"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 17,
    columnNumber: 11
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_App__WEBPACK_IMPORTED_MODULE_7__["default"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 18,
    columnNumber: 13
  }
})))))), document.getElementById('app')); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_8__["register"]();

/***/ }),

/***/ "./src/pages/AuthenticatedRoute.js":
/*!*****************************************!*\
  !*** ./src/pages/AuthenticatedRoute.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AuthenticatedRoute; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contexts/AuthContext */ "./src/contexts/AuthContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/AuthenticatedRoute.js";



function AuthenticatedRoute({
  children,
  ...rest
}) {
  const {
    pathname,
    search
  } = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["useLocation"])();
  const {
    isAuthenticated
  } = Object(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__["useAuthContext"])();
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], Object.assign({}, rest, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 5
    }
  }), isAuthenticated ? children : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], {
    to: `/login?redirect=${pathname}${search}`,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }
  }));
}

/***/ }),

/***/ "./src/pages/NotFound.js":
/*!*******************************!*\
  !*** ./src/pages/NotFound.js ***!
  \*******************************/
/*! exports provided: NotFound */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFound", function() { return NotFound; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/NotFound.js";

const NotFound = () => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 4,
    columnNumber: 3
  }
}, "Esta p\xE1gina no existe! :(");

/***/ }),

/***/ "./src/pages/Routes.js":
/*!*****************************!*\
  !*** ./src/pages/Routes.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _landing_Home__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./landing/Home */ "./src/pages/landing/Home/index.js");
/* harmony import */ var _NotFound__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./NotFound */ "./src/pages/NotFound.js");
/* harmony import */ var _AuthenticatedRoute__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./AuthenticatedRoute */ "./src/pages/AuthenticatedRoute.js");
/* harmony import */ var _UnauthenticatedRoute__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./UnauthenticatedRoute */ "./src/pages/UnauthenticatedRoute.js");
/* harmony import */ var _login_Login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login/Login */ "./src/pages/login/Login/index.js");
/* harmony import */ var _login_Signup__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/Signup */ "./src/pages/login/Signup/index.js");
/* harmony import */ var _login_Confirm__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/Confirm */ "./src/pages/login/Confirm/index.js");
/* harmony import */ var _login_Forgot__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login/Forgot */ "./src/pages/login/Forgot/index.js");
/* harmony import */ var _store_Store__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./store/Store */ "./src/pages/store/Store/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/Routes.js";

 // Routes










/* harmony default export */ __webpack_exports__["default"] = (() => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    exact: true,
    path: "/",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_landing_Home__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_UnauthenticatedRoute__WEBPACK_IMPORTED_MODULE_5__["default"], {
    path: "/login",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_login_Login__WEBPACK_IMPORTED_MODULE_6__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_UnauthenticatedRoute__WEBPACK_IMPORTED_MODULE_5__["default"], {
    path: "/signup",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_login_Signup__WEBPACK_IMPORTED_MODULE_7__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_UnauthenticatedRoute__WEBPACK_IMPORTED_MODULE_5__["default"], {
    path: "/confirm",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_login_Confirm__WEBPACK_IMPORTED_MODULE_8__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_UnauthenticatedRoute__WEBPACK_IMPORTED_MODULE_5__["default"], {
    path: "/forgot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_login_Forgot__WEBPACK_IMPORTED_MODULE_9__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_AuthenticatedRoute__WEBPACK_IMPORTED_MODULE_4__["default"], {
    path: "/store",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_store_Store__WEBPACK_IMPORTED_MODULE_10__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NotFound__WEBPACK_IMPORTED_MODULE_3__["NotFound"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 9
    }
  })));
});

/***/ }),

/***/ "./src/pages/UnauthenticatedRoute.js":
/*!*******************************************!*\
  !*** ./src/pages/UnauthenticatedRoute.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UnauthenticatedRoute; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contexts/AuthContext */ "./src/contexts/AuthContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/UnauthenticatedRoute.js";




function querystring(name, url = window.location.href) {
  name = name.replace(/[[]]/g, "\\$&");
  const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i");
  const results = regex.exec(url);

  if (!results) {
    return null;
  }

  if (!results[2]) {
    return "";
  }

  return decodeURIComponent(results[2].replace(/\+/g, " "));
} //


function UnauthenticatedRoute({
  children,
  ...rest
}) {
  const {
    isAuthenticated
  } = Object(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__["useAuthContext"])();
  const redirect = querystring("redirect");
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], Object.assign({}, rest, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 5
    }
  }), !isAuthenticated ? children : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], {
    to: redirect === "" || redirect === null ? "/" : redirect,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 9
    }
  }));
}

/***/ }),

/***/ "./src/pages/landing/Help/index.js":
/*!*****************************************!*\
  !*** ./src/pages/landing/Help/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/pages/landing/Help/styles.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/landing/Help/index.js";

 // import { useAuthContext } from "contexts/AuthContext";

 //

const HelpBody = props => {
  // const { isAuthenticated } = useAuthContext();
  //Render
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["Body"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_2__["H1"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, "BOLET IDEAL SPORTS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_2__["Text"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }
  }, "Espacio para Ayuda"));
};

const Help = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(HelpBody);
/* harmony default export */ __webpack_exports__["default"] = (Help);

/***/ }),

/***/ "./src/pages/landing/Help/styles.js":
/*!******************************************!*\
  !*** ./src/pages/landing/Help/styles.js ***!
  \******************************************/
/*! exports provided: Stylesheet, Body */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Body", function() { return Body; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
 // Material UI

 // Global Styles


const Stylesheet = {
  text: {
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].app
  }
};
const Body = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Column.Center,
    height: '100%',
    width: '100%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/pages/landing/Home/index.js":
/*!*****************************************!*\
  !*** ./src/pages/landing/Home/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_layouts_LandingLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/layouts/LandingLayout */ "./src/components/layouts/LandingLayout/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var _HomeBody__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../HomeBody */ "./src/pages/landing/HomeBody/index.js");
/* harmony import */ var _Results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Results */ "./src/pages/landing/Results/index.js");
/* harmony import */ var _Help__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Help */ "./src/pages/landing/Help/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/landing/Home/index.js";






 // import { Stylesheet } from './styles'
//

const HomePage = props => {
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useHistory"])();
  const {
    isAuthenticated
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_3__["useAuthContext"])();
  const [body, setBody] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(props.body ? props.body : 'home'); //

  const handleNavBar = option => {
    switch (option) {
      case 'store':
        history.push("/store");
        break;

      case 'login':
        history.push("/login");
        break;

      default:
        setBody(option);
        break;
    }
  }; //RenderBody


  const renderBody = () => {
    console.log('body: ' + body);

    if (body === 'results') {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Results__WEBPACK_IMPORTED_MODULE_5__["default"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39,
          columnNumber: 16
        }
      });
    } else if (body === 'help') {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Help__WEBPACK_IMPORTED_MODULE_6__["default"], {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 16
        }
      });
    }

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_HomeBody__WEBPACK_IMPORTED_MODULE_4__["default"], {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 14
      }
    });
  }; //Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_layouts_LandingLayout__WEBPACK_IMPORTED_MODULE_1__["LandingLayout"], {
    id: 'LandingLayout',
    authenticated: isAuthenticated,
    onNavBar: props.onNavBar ? props.onNavBar : handleNavBar,
    balance: props.balance,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 5
    }
  }, renderBody());
};

const Home = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(HomePage);
/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./src/pages/landing/HomeBody/index.js":
/*!*********************************************!*\
  !*** ./src/pages/landing/HomeBody/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/pages/landing/HomeBody/styles.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/landing/HomeBody/index.js";

 // import { useAuthContext } from "contexts/AuthContext";

 //

const HomeBodyComponent = props => {
  // const { isAuthenticated } = useAuthContext();
  //Render
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["Body"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_2__["H1"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, "BOLET IDEAL SPORTS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_2__["Text"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }
  }, "Espacio para HOME"));
};

const HomeBody = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(HomeBodyComponent);
/* harmony default export */ __webpack_exports__["default"] = (HomeBody);

/***/ }),

/***/ "./src/pages/landing/HomeBody/styles.js":
/*!**********************************************!*\
  !*** ./src/pages/landing/HomeBody/styles.js ***!
  \**********************************************/
/*! exports provided: Stylesheet, Body */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Body", function() { return Body; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
 // Material UI

 // Global Styles


const Stylesheet = {
  body: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: '100%',
    height: '-webkit-fill-available'
  },
  text: {
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].app
  }
};
const Body = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Column.Center,
    height: '100%',
    width: '100%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/pages/landing/Results/index.js":
/*!********************************************!*\
  !*** ./src/pages/landing/Results/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/pages/landing/Results/styles.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/landing/Results/index.js";

 // import { useAuthContext } from "contexts/AuthContext";

 //

const ResultsBody = props => {
  // const { isAuthenticated } = useAuthContext();
  //Render
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_styles__WEBPACK_IMPORTED_MODULE_1__["Body"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_2__["H1"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, "BOLET IDEAL SPORTS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_2__["Text"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }
  }, "Espacio para Resultados"));
};

const Results = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(ResultsBody);
/* harmony default export */ __webpack_exports__["default"] = (Results);

/***/ }),

/***/ "./src/pages/landing/Results/styles.js":
/*!*********************************************!*\
  !*** ./src/pages/landing/Results/styles.js ***!
  \*********************************************/
/*! exports provided: Stylesheet, Body */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Body", function() { return Body; });
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js");
/* harmony import */ var styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles */ "./src/styles/index.js");
 // Material UI

 // Global Styles


const Stylesheet = {
  text: {
    color: styles__WEBPACK_IMPORTED_MODULE_2__["Colors"].app
  }
};
const Body = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_0__["withStyles"])({
  root: { ...styles__WEBPACK_IMPORTED_MODULE_2__["Styles"].Column.Center,
    height: '100%',
    width: '100%'
  }
})(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_1__["default"]);

/***/ }),

/***/ "./src/pages/login/Confirm/index.js":
/*!******************************************!*\
  !*** ./src/pages/login/Confirm/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/layouts/LoginLayout */ "./src/components/layouts/LoginLayout/index.js");
/* harmony import */ var components_forms_FormConfirm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/forms/FormConfirm */ "./src/components/forms/FormConfirm/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/login/Confirm/index.js";

/* eslint-disable react-hooks/exhaustive-deps */





 // Login Page

const ConfirmPage = props => {
  const [isLoading, setIsLoading] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const [username, setUsername] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  const [password, setPassword] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  const location = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useLocation"])();
  const {
    authConfirm
  } = Object(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_1__["useAuthContext"])();
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useHistory"])(); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    if (!location.state || !location.state.username) {
      history.push('/login');
      return;
    }

    setUsername(location.state.username ? location.state.username : null);

    if (location.state && location.state.password) {
      setPassword(location.state.password ? location.state.password : null);
    }
  }, [location]); //Component Will Unmount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    return function () {
      // Set IsLoading to False
      setIsLoading(false);
    };
  }, []); // Submit button on Login Form.

  const handleSubmit = async code => {
    setIsLoading(true);
    const result = await authConfirm(username, password, code);
    setIsLoading(false);

    if (result.authorized) {
      history.push("/store");
      return;
    }

    if (result.confirmed) {
      history.push("/login");
      return;
    }
  }; //Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_3__["LoginLayout"], {
    title: translate('LAYOUT.FORGOT.TITLE'),
    description: translate('LAYOUT.FORGOT.DESCRIPTION'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_FormConfirm__WEBPACK_IMPORTED_MODULE_4__["default"], {
    title: translate('FORM.FORGOT.TITLE'),
    loading: isLoading,
    disabled: false,
    error: null,
    onSubmit: handleSubmit,
    onCancel: () => {
      history.push('/login');
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 11
    }
  }));
};

const Confirm = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(ConfirmPage);
/* harmony default export */ __webpack_exports__["default"] = (Confirm);

/***/ }),

/***/ "./src/pages/login/Forgot/index.js":
/*!*****************************************!*\
  !*** ./src/pages/login/Forgot/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/layouts/LoginLayout */ "./src/components/layouts/LoginLayout/index.js");
/* harmony import */ var components_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/forms */ "./src/components/forms/index.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/login/Forgot/index.js";





 // Login Page

const ForgotPage = props => {
  const [isLoading, setIsLoading] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const [codeSent, setCodeSent] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const [confirmed, setConfirmed] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const {
    authForgot
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_4__["useAuthContext"])();
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["useHistory"])(); //Component Will Unmount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    return function () {
      // Set IsLoading to False
      setIsLoading(false);
    };
  }, []); // Submit button on Forgot Form.

  const handleSubmit = async (username, password, code) => {
    setIsLoading(true);

    if (codeSent && confirmed) {
      history.push("/login");
      return;
    }

    const result = await authForgot(username, password, code);
    setIsLoading(false);

    if (result.data) {
      if (!codeSent) {
        setCodeSent(true);
      } else {
        setConfirmed(true);
      }
    }
  }; //


  const handleClose = () => {
    history.push("/login");
    return;
  }; //Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_2__["LoginLayout"], {
    title: translate('LAYOUT.FORGOT.TITLE'),
    description: translate('LAYOUT.FORGOT.DESCRIPTION'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms__WEBPACK_IMPORTED_MODULE_3__["FormForgot"], {
    step: !codeSent ? 1 : !confirmed ? 2 : 3,
    loading: isLoading,
    disabled: false,
    error: null,
    title: !codeSent ? translate('FORM.FORGOT.TITLE') : !confirmed ? translate('FORM.FORGOT.BUTTON_CHANGE') : translate('FORM.FORGOT.BUTTON_RETURN'),
    onSubmit: handleSubmit,
    onCancel: handleClose,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 9
    }
  }));
};

const Forgot = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(ForgotPage);
/* harmony default export */ __webpack_exports__["default"] = (Forgot);

/***/ }),

/***/ "./src/pages/login/Login/index.js":
/*!****************************************!*\
  !*** ./src/pages/login/Login/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./src/pages/login/Login/styles.js");
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/layouts/LoginLayout */ "./src/components/layouts/LoginLayout/index.js");
/* harmony import */ var components_forms_FormLogin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! components/forms/FormLogin */ "./src/components/forms/FormLogin/index.js");
/* harmony import */ var helpers_onErrorAuth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! helpers/onErrorAuth */ "./src/helpers/onErrorAuth.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/login/Login/index.js";










 // Login Page

const LoginPage = props => {
  const [isLoading, setIsLoading] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const {
    authFederated,
    authLogin
  } = Object(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__["useAuthContext"])();
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_7__["useLanguageContext"])();
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["useHistory"])(); // const classes = useStyles();
  // Submit button on Login Form.

  const handleSubmit = async (username, password) => {
    setIsLoading(true);
    const result = await authLogin(username, password);

    if (result.code === 'success') {
      history.push('/store');
      return;
    } else if (result.code === 'UserNotConfirmedException') {
      history.push({
        pathname: "/confirm",
        state: {
          username: username,
          password: password
        }
      });
      return;
    }

    Object(helpers_onErrorAuth__WEBPACK_IMPORTED_MODULE_6__["onErrorAuth"])(result.message);
    setIsLoading(false);
  }; //Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_4__["LoginLayout"], {
    title: translate('LAYOUT.LOGIN.TITLE'),
    description: translate('LAYOUT.LOGIN.DESCRIPTION'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_FormLogin__WEBPACK_IMPORTED_MODULE_5__["default"], {
    title: translate('FORM.LOGIN.LOGIN'),
    loading: isLoading,
    disabled: false,
    error: null,
    onSubmit: handleSubmit,
    onSocial: authFederated,
    onForgot: () => {
      history.push('/forgot');
    },
    onSignup: () => {
      history.push('/signup');
    },
    onCancel: () => {
      history.push('/');
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 7
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_9__["Row"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_1__["Stylesheet"].viewSignup,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_8__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_1__["Stylesheet"].disclaimer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 9
    }
  }, translate('LAYOUT.LOGIN.DONT_HAVE_ACCOUNT')), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_10__["default"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_1__["Stylesheet"].link,
    onClick: () => {
      history.push('/signup');
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 9
    }
  }, translate('LAYOUT.LOGIN.SIGNUP'))));
};

const Login = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(LoginPage);
/* harmony default export */ __webpack_exports__["default"] = (Login);

/***/ }),

/***/ "./src/pages/login/Login/styles.js":
/*!*****************************************!*\
  !*** ./src/pages/login/Login/styles.js ***!
  \*****************************************/
/*! exports provided: Stylesheet, useStyles, Lander, H1, Text, Link */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useStyles", function() { return useStyles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lander", function() { return Lander; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "H1", function() { return H1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Text", function() { return Text; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Link", function() { return Link; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/esm/styles/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-device-detect */ "./node_modules/react-device-detect/main.js");
/* harmony import */ var react_device_detect__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_device_detect__WEBPACK_IMPORTED_MODULE_3__);




const Stylesheet = {
  disclaimer: {
    color: 'black',
    marginRight: 5
  },
  viewSignup: {
    marginTop: 20
  },
  link: {
    color: 'blue'
  }
};
const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_1__["makeStyles"])(theme => ({
  link: {
    color: 'blue'
  }
}));
const Lander = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  padding: 80px 0;
  text-align: center;
`;
const H1 = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].h1`
    font-family: "Roboto", sans-serif;
    font-weight: 600;
`;
const Text = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].p`
  color: #999;
`;
const Link = Object(styled_components__WEBPACK_IMPORTED_MODULE_0__["default"])(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"])`
  align-items: right;
  color: #FFF;
  display: inline-flex;
  height: 100%;
  justify-content: right;
  text-decoration: none;
  width: 100%;
  color: blue;

  ${react_device_detect__WEBPACK_IMPORTED_MODULE_3__["isMobile"] && styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`
  `};

  &[aria-current] {
    color: #5fb999;
  }
`;

/***/ }),

/***/ "./src/pages/login/Signup/index.js":
/*!*****************************************!*\
  !*** ./src/pages/login/Signup/index.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _contexts_AuthContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/layouts/LoginLayout */ "./src/components/layouts/LoginLayout/index.js");
/* harmony import */ var components_forms_FormSignup__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/forms/FormSignup */ "./src/components/forms/FormSignup/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/login/Signup/index.js";




 // import { onErrorAuth } from "helpers/onErrorAuth";

 // Login Page

const SignupPage = props => {
  const [isLoading, setIsLoading] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false);
  const {
    authSignup
  } = Object(_contexts_AuthContext__WEBPACK_IMPORTED_MODULE_1__["useAuthContext"])();
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  const history = Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["useHistory"])(); // Submit button on Signup Form.

  const handleSubmit = async (username, password) => {
    setIsLoading(true);
    const result = await authSignup(username, password);

    if (result.data) {
      history.push({
        pathname: '/confirm',
        state: {
          username: username,
          password: password
        }
      });
      return;
    }

    setIsLoading(false);
  }; //Render


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_layouts_LoginLayout__WEBPACK_IMPORTED_MODULE_3__["LoginLayout"], {
    title: translate('LAYOUT.SIGNUP.TITLE'),
    description: translate('LAYOUT.SIGNUP.DESCRIPTION'),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_forms_FormSignup__WEBPACK_IMPORTED_MODULE_4__["default"], {
    title: translate('FORM.SIGNUP.TITLE'),
    loading: isLoading,
    disabled: false,
    error: null,
    onSubmit: handleSubmit,
    onCancel: () => {
      history.push('/login');
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 9
    }
  })));
};

const Signup = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(SignupPage);
/* harmony default export */ __webpack_exports__["default"] = (Signup);

/***/ }),

/***/ "./src/pages/store/BodySport/index.js":
/*!********************************************!*\
  !*** ./src/pages/store/BodySport/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! contexts/AppContext */ "./src/contexts/AppContext.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles */ "./src/pages/store/BodySport/styles.js");
/* harmony import */ var components_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/view */ "./src/components/view/index.js");
/* harmony import */ var components_texts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/texts */ "./src/components/texts/index.js");
/* harmony import */ var contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! contexts/LanguageContext */ "./src/contexts/LanguageContext.js");
/* harmony import */ var components_buttons_Link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! components/buttons/Link */ "./src/components/buttons/Link/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/store/BodySport/index.js";

/* eslint-disable react-hooks/exhaustive-deps */
 // import PropTypes from 'prop-types'
//!Material UI
//!Contexts 

 //!Components
//!Global Styles
// import { } from 'styles';
//!Local Styles






 // import Link from '@material-ui/core/Link';
//

const BodySportComponent = ({
  sport
}) => {
  // const { user } = useAuthContext();
  const {
    ligues,
    matches,
    getLiguesBySportId,
    getMatchsByLigueId,
    liguesResult,
    matchsResult
  } = Object(contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__["useAppContext"])();
  const [ligue, setLigue] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(ligues ? ligues[0] : null);
  const [match, setMatch] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(matches ? matches[0] : null);
  const {
    translate
  } = Object(contexts_LanguageContext__WEBPACK_IMPORTED_MODULE_5__["useLanguageContext"])();
  const [isSelectingLigue, setSelectingLigue] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(true); //Will Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useLayoutEffect(() => {
    const onLoad = async () => {
      try {} catch (err) {
        console.log(err);
      }
    };

    onLoad(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    console.log('Component Did Mount');
  }, []); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    console.log('sportchange');
    console.log(sport);

    if (sport) {
      console.log('loadLigues');
      const x = getLiguesBySportId(sport.id);
      console.log(x);
      setLigue(null);
    }
  }, [sport, ligues]); //

  const handleSelectLigue = item => {
    console.log(item);
    setLigue(item);
  };

  const renderLigueMatches = () => {// console.log(ligueMatches);
  };

  const selectMatch = bet => {
    console.log(bet);
    setMatch(bet);
  }; //


  const renderLigues = () => {
    return liguesResult.map(item => {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_buttons_Link__WEBPACK_IMPORTED_MODULE_6__["default"], {
        key: item.id,
        onClick: () => handleSelectLigue(item),
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86,
          columnNumber: 9
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
        key: item.id,
        styles: item && item.local && item.local.url ? { ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueItemView,
          backgroundImage: `url(${item.local.url})`,
          backgroundRepeat: 'round'
        } : _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueItemView,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87,
          columnNumber: 11
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
        styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueTitleView,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 13
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
        styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].ligueTitle,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 15
        }
      }, item.local.title))));
    });
  }; //


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: sport && sport.local && sport.local.url ? { ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].body,
      backgroundImage: `url(${sport.local.url})`,
      backgroundRepeat: 'round'
    } : _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].body,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: isSelectingLigue ? { ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].container,
      ..._styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].containerSelectLigue
    } : _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].container,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 7
    }
  }, sport && sport.local && sport.local.id && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["H1"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].sportTitle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108,
      columnNumber: 11
    }
  }, translate(sport.id)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_texts__WEBPACK_IMPORTED_MODULE_4__["Text"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].disclaimer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109,
      columnNumber: 11
    }
  }, translate('STORE.LIGUES.INSTRUCTIONS'))), liguesResult && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_view__WEBPACK_IMPORTED_MODULE_3__["View"], {
    styles: _styles__WEBPACK_IMPORTED_MODULE_2__["Stylesheet"].liguesContainer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 11
    }
  }, renderLigues())));
};

const BodySport = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(BodySportComponent);
/* harmony default export */ __webpack_exports__["default"] = (BodySport);

/***/ }),

/***/ "./src/pages/store/BodySport/styles.js":
/*!*********************************************!*\
  !*** ./src/pages/store/BodySport/styles.js ***!
  \*********************************************/
/*! exports provided: Lander, H1, Text, Stylesheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lander", function() { return Lander; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "H1", function() { return H1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Text", function() { return Text; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Stylesheet", function() { return Stylesheet; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");

const Lander = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].div`
  padding: 80px 0;
  text-align: center;
`;
const H1 = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].h1`
    font-family: "Roboto", sans-serif;
    font-weight: 600;
`;
const Text = styled_components__WEBPACK_IMPORTED_MODULE_0__["default"].p`
  color: #999;
`;
const Stylesheet = {
  body: {
    backgroundColor: 'white',
    width: '100%',
    height: '90vh',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover'
  },
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    height: '100%',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  containerSelectLigue: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  sportTitle: {
    marginTop: 20,
    fontSize: 40,
    color: 'white'
  },
  liguesContainer: {
    marginTop: 20,
    height: 'auto',
    width: '80%',
    flexDirection: 'row'
  },
  ligueItemView: {
    height: 150,
    width: 150,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
    marginLeft: 10,
    marginRight: 10
  },
  ligueTitleView: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    width: '90%',
    height: '30%',
    paddingLeft: '5%',
    paddingRight: '5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  ligueTitle: {
    width: '100%',
    height: '100%',
    textAlign: 'center',
    color: 'white',
    fontSize: 18
  },
  disclaimer: {
    fontSize: 18,
    color: 'white',
    marginTop: 10
  }
};

/***/ }),

/***/ "./src/pages/store/Store/index.js":
/*!****************************************!*\
  !*** ./src/pages/store/Store/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! contexts/AppContext */ "./src/contexts/AppContext.js");
/* harmony import */ var contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! contexts/AuthContext */ "./src/contexts/AuthContext.js");
/* harmony import */ var pages_landing_Home__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pages/landing/Home */ "./src/pages/landing/Home/index.js");
/* harmony import */ var components_layouts_StoreLayout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/layouts/StoreLayout */ "./src/components/layouts/StoreLayout/index.js");
/* harmony import */ var _BodySport__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../BodySport */ "./src/pages/store/BodySport/index.js");
var _jsxFileName = "/Users/majiphaneer/Development/React/bolet-sports/src/pages/store/Store/index.js";
 // import PropTypes from 'prop-types'
//!Material UI
//!Contexts 


 //!Components


 //!Global Styles
// import { } from 'styles';
//!Local Styles
// import { Stylesheet } from './styles';

 //

const StorePage = props => {
  const {
    isAuthenticated
  } = Object(contexts_AuthContext__WEBPACK_IMPORTED_MODULE_2__["useAuthContext"])();
  const {
    sports,
    ligues,
    loadSports,
    loadLigues
  } = Object(contexts_AppContext__WEBPACK_IMPORTED_MODULE_1__["useAppContext"])();
  const [itemSelected, setItemSelected] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  const [sport, setSport] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(null);
  const [localLigues, setLocalLigues] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(ligues);
  const [storePage, setStorePage] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState('store');
  const [init, setInit] = react__WEBPACK_IMPORTED_MODULE_0___default.a.useState(false); ///Constructor

  const constructor = async () => {
    if (!init) {
      setInit(true);
    }
  };

  constructor(); //Will Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useLayoutEffect(() => {
    const onLoad = async () => {
      try {
        const items = await loadSports();
        console.log('Sports Loaded');
        return items;
      } catch (err) {
        console.log(err);
      }
    };

    onLoad(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); //Component Did Mount

  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    console.log('Store -> Component Did Mount');
  }, []);
  react__WEBPACK_IMPORTED_MODULE_0___default.a.useEffect(() => {
    const xSport = { ...itemSelected,
      image: ''
    };
    setSport(xSport);
  }, [itemSelected]); //

  const handleSelect = (menuLevel, optionId, item) => {
    setItemSelected(item);
  }; //


  const handleNavBar = option => {
    setStorePage(option);
  }; //


  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 5
    }
  }, storePage === 'home' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(pages_landing_Home__WEBPACK_IMPORTED_MODULE_3__["default"], {
    onNavBar: handleNavBar,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 33
    }
  }), storePage !== 'home' && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(components_layouts_StoreLayout__WEBPACK_IMPORTED_MODULE_4__["StoreLayout"], {
    authenticated: isAuthenticated,
    sports: sports,
    onSelect: handleSelect,
    itemSelected: itemSelected,
    bodyPage: storePage,
    onNavBar: handleNavBar,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 9
    }
  }, // Un Segundo NavBar para Seleccionar la liga.
  // Un poco Mas abajo el Titulo de la liga seleccionada.
  // Sin Seleccionar la liga, debera presentar una imagen de fondo con el mensaje.
  // Seleccione una liga.
  // all selecionar la liga presentara el mensaje titulo con el nombre de la liga.
  // Si hay imagen de la liga, se presentara la imagen de la liga.
  ligues && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_BodySport__WEBPACK_IMPORTED_MODULE_5__["default"], {
    sport: sport,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 15
    }
  })));
};

const Store = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(StorePage);
/* harmony default export */ __webpack_exports__["default"] = (Store);

/***/ }),

/***/ "./src/serviceWorker.js":
/*!******************************!*\
  !*** ./src/serviceWorker.js ***!
  \******************************/
/*! exports provided: register, unregister */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregister", function() { return unregister; });
// This optional code is used to register a service worker.
// register() is not called by default.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.
// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA
const isLocalhost = Boolean(window.location.hostname === 'localhost' || // [::1] is the IPv6 localhost address.
window.location.hostname === '[::1]' || // 127.0.0.0/8 are considered localhost for IPv4.
window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));
function register(config) {
  if (false) {}
}

function registerValidSW(swUrl, config) {
  navigator.serviceWorker.register(swUrl).then(registration => {
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;

      if (installingWorker == null) {
        return;
      }

      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the updated precached content has been fetched,
            // but the previous service worker will still serve the older
            // content until all client tabs are closed.
            console.log('New content is available and will be used when all ' + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.'); // Execute callback

            if (config && config.onUpdate) {
              config.onUpdate(registration);
            }
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.'); // Execute callback

            if (config && config.onSuccess) {
              config.onSuccess(registration);
            }
          }
        }
      };
    };
  }).catch(error => {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl, config) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl, {
    headers: {
      'Service-Worker': 'script'
    }
  }).then(response => {
    // Ensure service worker exists, and that we really are getting a JS file.
    const contentType = response.headers.get('content-type');

    if (response.status === 404 || contentType != null && contentType.indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(registration => {
        registration.unregister().then(() => {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl, config);
    }
  }).catch(() => {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    }).catch(error => {
      console.error(error.message);
    });
  }
}

/***/ }),

/***/ "./src/styles/animation.js":
/*!*********************************!*\
  !*** ./src/styles/animation.js ***!
  \*********************************/
/*! exports provided: fadeIn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fadeIn", function() { return fadeIn; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");

const fadeInKeyframes = styled_components__WEBPACK_IMPORTED_MODULE_0__["keyframes"]`
  from {
    filter: blur(5px);
    opacity: 0;
  }

  to {
    filter: blur(0);
    opacity: 1;
  }
`;
const fadeIn = ({
  time = '1s',
  type = 'ease'
} = {}) => styled_components__WEBPACK_IMPORTED_MODULE_0__["css"]`animation: ${time} ${fadeInKeyframes} ${type};`;

/***/ }),

/***/ "./src/styles/colors.js":
/*!******************************!*\
  !*** ./src/styles/colors.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/core/colors */ "./node_modules/@material-ui/core/esm/colors/index.js");

const Colors = {
  background: '#EBECF0',
  backgroundDark: '#8A8EA6',
  backgroundLight: '#FFFFFF',
  primary: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["orange"][900],
  secondary: '#0096E6',
  highlight: 'red',
  error: 'red',
  link: 'blue',
  app: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["orange"][900],
  text: '#000',
  textLight: '#FFF',
  white: '#FFF',
  black: '#000',
  MUIColors: {
    yellowAmber: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["amber"],
    blue: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["blue"],
    blueGrey: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["blueGrey"],
    brown: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["brown"],
    black: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["common"].black,
    white: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["common"].white,
    cyan: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["cyan"],
    orangeDark: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["deepOrange"],
    purpleDark: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["deepPurple"],
    green: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["green"],
    grey: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["grey"],
    indigo: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["indigo"],
    blueLight: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["lightBlue"],
    greenLight: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["lightGreen"],
    greenLime: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["lime"],
    orange: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["orange"],
    pink: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["pink"],
    purple: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["purple"],
    red: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["red"],
    teal: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["teal"],
    yellow: _material_ui_core_colors__WEBPACK_IMPORTED_MODULE_0__["yellow"]
  },
  Pallette: {
    red: 'red',
    blue: 'blue',
    yellow: 'yellow',
    salmon: 'salmon',
    gold: 'gold',
    Yellow: {
      lightyellow: '#FFFFE0',
      lemonchiffon: '#FFFACD',
      lightgoldenrodyellow: '#FAFAD2',
      papayawhip: '#FFEFD5',
      moccasin: '#FFE4B5',
      peachpuff: '#FFDAB9',
      palegoldenrod: '#EEE8AA',
      khaki: '#F0E68C',
      darkkhaki: '#BDB76B',
      yellow: '#FFFF00'
    },
    Gray: {
      brightgray: '#EBECF0'
    }
  },
  Presets: {
    SupraHeader: {
      Menu: {
        Text: {
          normal: '#6c7293',
          active: '#3699ff',
          hover: '#3699ff'
        },
        Button: {
          normal: 'transparent',
          active: 'rgba(77,89,149,.06)',
          hover: 'rgba(77,89,149,.06)'
        }
      },
      Initial: {
        background: '#1BC5BD',
        color: '#C9F7F5'
      }
    },
    AccountDrawer: {
      title: '#3699ff',
      subtitle: '#B5B5C3',
      button: '#3699ff'
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (Colors);

/***/ }),

/***/ "./src/styles/effects.js":
/*!*******************************!*\
  !*** ./src/styles/effects.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
const Effects = {
  SupraHeader: {
    Menu: {
      Button: {
        Hover: {
          '&:hover': {
            backgroundColor: '#D0D0D0',
            borderColor: '#A3ADD0',
            borderRadius: 5
          }
        },
        Active: {
          '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf'
          }
        },
        Focus: {
          '&:focus': {}
        }
      }
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (Effects);

/***/ }),

/***/ "./src/styles/fonts.js":
/*!*****************************!*\
  !*** ./src/styles/fonts.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const sizes = {
  title: 24,
  subtitle: 22,
  supraHeader: 14,
  header: 28,
  h1: 24,
  h2: 22,
  h3: 20,
  h4: 18,
  h5: 16,
  p100: '100%',
  p50: '50%',
  p25: '25%',
  p10: '10%',
  p150: '150%',
  p200: '200%',
  p250: '250%',
  normal: 16
};
const weights = {
  lightest: 100,
  light: 300,
  normal: 500,
  bold: 700,
  bolderst: 900
};
const Fonts = {
  p: {
    fontSize: sizes.normal,
    fontWeight: weights.normal
  },
  span: {
    fontSize: sizes.normal,
    fontWeight: weights.normal
  },
  h1: {
    fontSize: sizes.h1,
    fontWeight: weights.bold
  },
  h2: {
    fontSize: sizes.h2,
    fontWeight: weights.normal
  },
  supraHeader: {
    fontSize: sizes.supraHeader,
    fontWeight: 800
  },
  accountName: {
    fontSize: 15,
    fontWeight: 600,
    textDecoration: 'underline',
    textAlign: 'center'
  },
  acountInitial: {
    fontSize: 15,
    fontWeight: 500,
    lineHeight: 0
  },
  accountTitle: {
    fontSize: 15,
    fontWeight: 500
  },
  AccountSubtitle: {
    fontSize: 12,
    fontWeight: 400
  }
};
/* harmony default export */ __webpack_exports__["default"] = (Fonts);

/***/ }),

/***/ "./src/styles/images.js":
/*!******************************!*\
  !*** ./src/styles/images.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! helpers */ "./src/helpers/index.js");

const Images = {
  // "camera": (toAbsoluteUrl('/assets/svg/')),
  "public_svg_humans_custom1": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-1.svg'),
  "public_svg_humans_custom2": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-2.svg'),
  "public_svg_humans_custom3": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-3.svg'),
  "public_svg_humans_custom4": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-4.svg'),
  "public_svg_humans_custom5": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-5.svg'),
  "public_svg_humans_custom6": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-6.svg'),
  "public_svg_humans_custom7": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-7.svg'),
  "public_svg_humans_custom8": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-8.svg'),
  "public_svg_humans_custom9": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-9.svg'),
  "public_svg_humans_custom10": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-10.svg'),
  "public_svg_humans_custom11": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-11.svg'),
  "public_svg_humans_custom12": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-12.svg'),
  "public_svg_humans_custom13": Object(helpers__WEBPACK_IMPORTED_MODULE_0__["toAbsoluteUrl"])('/assets/svg/humans/custom-13.svg')
};
/* harmony default export */ __webpack_exports__["default"] = (Images);

/***/ }),

/***/ "./src/styles/index.js":
/*!*****************************!*\
  !*** ./src/styles/index.js ***!
  \*****************************/
/*! exports provided: Colors, Metrics, Styles, Fonts, Effects, Images */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _colors_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./colors.js */ "./src/styles/colors.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Colors", function() { return _colors_js__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _metrics_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./metrics.js */ "./src/styles/metrics.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Metrics", function() { return _metrics_js__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _styles_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles.js */ "./src/styles/styles.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Styles", function() { return _styles_js__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _fonts_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fonts.js */ "./src/styles/fonts.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Fonts", function() { return _fonts_js__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _effects_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./effects.js */ "./src/styles/effects.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Effects", function() { return _effects_js__WEBPACK_IMPORTED_MODULE_4__["default"]; });

/* harmony import */ var _images_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./images.js */ "./src/styles/images.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Images", function() { return _images_js__WEBPACK_IMPORTED_MODULE_5__["default"]; });









/***/ }),

/***/ "./src/styles/metrics.js":
/*!*******************************!*\
  !*** ./src/styles/metrics.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const SUPRAHEADER_HEIGHT = 35;
const SUPRAHEADER_BUTTON_HEIGHT = 30;
const SUPRAHEADER_BUTTON_WIDTH = 100;
const SUPRAHEADER_BUTTON_RADIUS = 5;
const HEADER_HEIGHT = 60;
const Metrics = {
  SupraHeader: {
    height: SUPRAHEADER_HEIGHT,
    width: '100%',
    Container: {
      height: SUPRAHEADER_HEIGHT,
      width: '100%'
    },
    Menu: {
      Container: {
        height: SUPRAHEADER_HEIGHT,
        width: '100%',
        paddingLeft: 10
      },
      Button: {
        height: SUPRAHEADER_BUTTON_HEIGHT,
        width: SUPRAHEADER_BUTTON_WIDTH,
        marginLeft: 5,
        marginRight: 5
      }
    },
    Account: {
      Container: {
        height: SUPRAHEADER_HEIGHT,
        width: '100%'
      },
      Button: {
        height: SUPRAHEADER_BUTTON_HEIGHT,
        width: 'auto',
        paddingTop: 0,
        paddingBottom: 0
      },
      Initial: {
        Container: {
          height: SUPRAHEADER_BUTTON_HEIGHT,
          width: SUPRAHEADER_BUTTON_HEIGHT,
          borderRadius: SUPRAHEADER_BUTTON_RADIUS
        }
      },
      Username: {
        Container: {
          height: SUPRAHEADER_BUTTON_HEIGHT,
          width: '100%',
          marginLeft: 10,
          marginRight: 10
        }
      }
    }
  },
  Header: {
    height: HEADER_HEIGHT,
    width: '100%'
  },
  Profile: {
    Logout: {
      Button: {
        width: '80%',
        height: '20%',
        borderRadius: 5
      },
      Text: {}
    }
  },
  TextInput: {
    minHeight: '5vh',
    height: '10%',
    width: '70%'
  }
};
/* harmony default export */ __webpack_exports__["default"] = (Metrics);

/***/ }),

/***/ "./src/styles/styles.js":
/*!******************************!*\
  !*** ./src/styles/styles.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
const Styles = {
  Row: {
    CenterVertical: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
    CenterHorizontal: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center'
    },
    Center: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    Start: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center'
    },
    End: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    SpaceBetween: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    SpaceAround: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    SpaceEvenly: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-evenly',
      alignItems: 'center'
    }
  },
  Column: {
    CenterVertical: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center'
    },
    CenterHorizontal: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
    Center: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    Start: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center'
    },
    End: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    SpaceBetween: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    SpaceAround: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    SpaceEvenly: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-evenly',
      alignItems: 'center'
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (Styles);

/***/ }),

/***/ 1:
/*!**************************************************************************************************************!*\
  !*** multi (webpack)/hot/dev-server.js ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/majiphaneer/Development/React/bolet-sports/node_modules/webpack/hot/dev-server.js */"./node_modules/webpack/hot/dev-server.js");
__webpack_require__(/*! /Users/majiphaneer/Development/React/bolet-sports/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /Users/majiphaneer/Development/React/bolet-sports/src/index.js */"./src/index.js");


/***/ })

},[[1,"runtime-main",1]]]);
//# sourceMappingURL=main.chunk.js.map