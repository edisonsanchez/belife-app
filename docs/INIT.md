REACT NATIVE BOILER PLATE
Remote: XXXXXXXXXXXXXXXX

# CREATE APP
npx react-native init BeLifeApp
cd BeLifeApp

# COPY DEPENDENCIES (Package JSON)
# RUN NPM INSTALL
npm install
npm install --save-dev eslint-config-prettier eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-prettier eslint-plugin-promise eslint-plugin-react eslint-plugin-react-native eslint-plugin-standard prettier
npm install @react-native-community/async-storage @react-native-community/masked-view @react-native-community/netinfo amazon-cognito-identity-js aws-amplify aws-amplify-react aws-amplify-react-native axios himalaya lodash.memoize lottie-ios lottie-react-native moment native-base prop-types react-native-elements react-native-fast-image react-native-gesture-handler react-native-gifted-chat react-native-i18n react-native-navigation-bar-color react-native-reanimated react-native-rss-parser react-native-safe-area-context react-native-screens react-native-splash-screen react-native-svg react-native-vector-icons react-navigation react-navigation-drawer react-navigation-material-bottom-tabs react-navigation-stack react-navigation-tabs react-redux redux redux-persist redux-saga reduxsauce styled-components

cd ios
pod install --repo-update
cd ..

# SET GIT
git init
git remote add origin git@gitlab.com:edisonsanchez/belife-app.git
git add -A
git commit -m "Initial Version"
git push origin master

# GET BOILERPLATE
mkdir boilerplate
cd boilerplate
git init
git remote add boilerplate git@gitlab.com:edisonsanchez/react-native-boilerplate.git
git config core.sparsecheckout true
( echo gitAllow ) >> .git/info/sparse-checkout
git pull boilerplate master

rm .git/info/sparse-checkout
cat gitAllow >> .git/info/sparse-checkout
git reset --hard boilerplate/master
git config core.sparsecheckout false
rm .git/info/sparse-checkout
cp -r ./* ../
cd ..
yes | rm -r boilerplate
mv app.json ./App
rm .eslintrc.js
mv ./bak/eslintrc.js .eslintrc.js
mv ./bak/prettierrc .prettierrc.js
yes | rm -r bak

# Change Imports in index.js to new routes.