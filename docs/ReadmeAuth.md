**_ GUIDE FOR AUTH WITH AMPLIFY _**

**_ FACEBOOK _**

- https://developers.facebook.com/docs/facebook-login
- Add New App
- Name and Email, then "Create App ID".
- On the left navigation bar, choose "Settings" and then "Basic".
- App ID and the App Secret

**_ GOOGLE _**
https://console.developers.google.com/

- Create a Project, left navigation bar, choose Credentials.
- Configurar Pantalla de Consentimiento,
- Create your OAuth2.0 credentials by choosing OAuth client ID
- Choose Web application
- Note the OAuth client ID and client secret

**_ AMPLIFY _**
amplify init

- Enter a name for the environment: local
- Choose your default editor: Visual Studio Code (or your text editor)
- Choose the type of app that you're building: javascript
- What javascript framework are you using: react-native
- Source Directory Path: /
- Distribution Directory Path: build
- Build Command: npm run-script build
- Start Command: npm run-script start
- Do you want to use an AWS profile? Y
- Please choose the profile you want to use: YOUR_USER_PROFILE

amplify add auth

- Default configuration with Social Provider (Federation)
- Username (or Email)
- Email
- rnamplify (use default or create custom prefix)
- Enter your redirect signin URI: rnamplify:// (this can be updated later for production environments)
- Do you want to add another redirect signin URI: N
- Enter your redirect signout URI: rnamplify://
- Do you want to add another redirect signout URI: N
- Select the social providers you want to configure for your user pool: Choose Facebook & Google
  amplify push

**_ SECOND LAP - Facebook _**

- Facebook app and click on Basic in the left hand menu.
- Scroll to the book & click Add Platform, then choose Website.
- Input the OAuth Endpoint URL with /oauth2/idpresponse appended into Site URL (Field Website, Site URL ) and Save changes.
- OAuth Endpoint into App Domains without https:// and end /. and Save changes.
- Privacy Policy and Terms and Conditions: Fill it.
- Navigation bar choose Products and then Set up from Facebook Login & choose Web.
- Valid OAuth Redirect URIs use the OAuth Endpoint + /oauth2/idpresponse
- Set Live

**_ SECOND LAP - Google _**

- https://console.developers.google.com/apis/credentials
- client ID to update the settings
- Authorized JavaScript origins, add the OAuth Endpoint.
- Authorized redirect URIs, add the OAuth Endpoint with /oauth2/idpresponse

**_ iOS - Xcode configuration _**

- Open info.plist
- Add:
  <key>CFBundleURLTypes</key>
  <array>
  <dict>
  <key>CFBundleURLSchemes</key>
  <array>
  <string>myapp</string>
  </array>
  </dict>
  </array>
- Entre ????? y CFBundleVersion.

In AppDelegate.m
Adicionar antes de @end:

- (BOOL)application:(UIApplication _)application
  openURL:(NSURL _)url
  options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> \*)options
  {
  return [RCTLinkingManager application:application openURL:url options:options];
  }

y en los import:
#import <React/RCTLinkingManager.h>

**_ Android - Android Studio configuration _**

- open android/app/main/AndroidManifest.xml
- add the following intent-filter inside <activity> Main Activity:
  <intent-filter android:label="filter_react_native">
  <action android:name="android.intent.action.VIEW" />
  <category android:name="android.intent.category.DEFAULT" />
  <category android:name="android.intent.category.BROWSABLE" />
  <data android:scheme="myapp" />
  </intent-filter>
  <intent-filter android:label="@string/app_name">
  <action android:name="android.intent.action.VIEW" />
  <category android:name="android.intent.category.DEFAULT" />
  <category android:name="android.intent.category.BROWSABLE" />
  <!-- <data android:scheme="app" android:host="deeplink" /> -->
  <data android:scheme="app" android:host="" />
  </intent-filter>
- antes de </activity>

**_ AWS _**

- Review Polcies of Password.
- Update Version of Facebook API.
