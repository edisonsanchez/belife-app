import React from 'react';

import AppNavigator from './AppNavigator';
import {createAppContainer} from 'react-navigation';
import i18 from '../src/localize/I18n';
import AsyncStorage from '@react-native-community/async-storage';

import Amplify from 'aws-amplify';
import aws_exports from './aws-exports';

import {HideNavigationBar} from 'react-native-navigation-bar-color';
import {YellowBox} from 'react-native';

Amplify.configure(aws_exports);

const AppContainer = createAppContainer(AppNavigator);

console.reportErrorsAsExceptions = false;

// YellowBox.ignoreWarnings([
//   //'Warning: Async Storage has been extracted',
//   "Battery state",
//   "componentWillMount",
//   "componentWillUpdate",
//   "componentWillReceiveProps",
//   "[location] ERROR - 0",
//   "Warning: DatePickerAndroid", // will be fixed with https://github.com/mmazzarolo/react-native-modal-datetime-picker/pull/262
//   "RCTRootView cancelTouches", // https://github.com/kmagiera/react-native-gesture-handler/issues/746
// ]);

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.configLanguage();
  }

  // Set Language
  configLanguage = async () => {
    let language = await AsyncStorage.getItem('language');

    i18.config(language);

    // HideNavigationBar();
  };

  render() {
    // HideNavigationBar();

    return <AppContainer />;
  }
}
