import React from 'react';
import {View, Text} from 'react-native';
// import Loading from '../src/Components/Loading';
import SplashScreen from 'react-native-splash-screen';

import AsyncStorage from '@react-native-community/async-storage';
import {Auth} from 'aws-amplify';
import {LoadingScreen} from '../src/containers/loadings';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    try {
      let sessionToken = await AsyncStorage.getItem('sessionToken');
      let session = await Auth.currentAuthenticatedUser();

      if (session) {
        await AsyncStorage.setItem(
          'sessionToken',
          session.signInUserSession.accessToken.jwtToken,
        );
        this.props.navigation.navigate('App');
      } else {
        this.props.navigation.navigate('Auth');
      }
    } catch (_err) {
      this.props.navigation.navigate('Auth');
    }
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <LoadingScreen />
      </View>
    );
  }
}
