import React from 'react';
import {View, Text, Image, Linking} from 'react-native';
import {Colors} from '../src/styles';

import Icon from 'react-native-vector-icons/FontAwesome';
//import IconI from 'react-native-vector-icons/Ionicons';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {StackActions, NavigationActions} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';

import AuthLoadingScreen from './AuthLoadingScreen';

import ScreenLogin from '../src/screens/Login/Login';
import ScreenSignup from '../src/screens/Login/Signup';
import ScreenForgotPassword from '../src/screens/Login/ForgotPassword';
import ScreenConfirmSignup from '../src/screens/Login/ConfirmSignup';
import ScreenHome from '../src/screens/Home/Home';
import ScreenStudy from '../src/screens/Studies/Studies';
import ScreenChat from '../src/screens/Login/Login';
import ScreenLive from '../src/screens/Live/Live';
import ScreenAccount from '../src/screens/Account/Account';

//import {HandleLogout} from '../../libs/awsAuth';

//
// Application Stack Logued Screens
const AuthStack = createStackNavigator(
  {
    Landing: {
      screen: ScreenLogin,
      navigationOptions: {
        headerTitle: 'Sign In',
        initialRouteName: 'Login',
        headerMode: 'none',
      },
    },
    Login: {
      screen: ScreenLogin,
      //screen: ScreenPhotoInfo,
      navigationOptions: {
        headerShown: false,
      },
    },
    SignUp: {
      screen: ScreenSignup,
      navigationOptions: {
        headerShown: false,
      },
    },
    ForgotPassword: {
      screen: ScreenForgotPassword,
      navigationOptions: {
        headerShown: false,
      },
    },
    ConfirmSignup: {
      screen: ScreenConfirmSignup,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'Login',
  },
);

//!
const HomeStack = createStackNavigator({
  Home: {
    screen: ScreenHome,
    //screen: ScreenPhotoInfo,
    navigationOptions: {
      headerShown: false,
    },
  },
  Screen2: {
    screen: ScreenHome,
    //screen: ScreenPhotoInfo,
    navigationOptions: {
      headerShown: false,
    },
  },
});

//!
const StudyStack = createStackNavigator({
  Home: {
    screen: ScreenStudy,
    navigationOptions: {
      headerShown: false,
    },
  },
  Screen2: {
    screen: ScreenStudy,
    navigationOptions: {
      headerShown: false,
    },
  },
});

//!
const ChatStack = createStackNavigator({
  Home: {
    screen: ScreenChat,
    navigationOptions: {
      headerShown: false,
    },
  },
  Screen2: {
    screen: ScreenChat,

    navigationOptions: {
      headerShown: false,
    },
  },
});

//!
const LiveStack = createStackNavigator({
  Home: {
    screen: ScreenLive,
    navigationOptions: {
      headerShown: false,
    },
  },
  Screen2: {
    screen: ScreenLive,

    navigationOptions: {
      headerShown: false,
    },
  },
});

//!
const AccountStack = createStackNavigator({
  Home: {
    screen: ScreenAccount,
    navigationOptions: {
      headerShown: false,
    },
  },
  Screen2: {
    screen: ScreenAccount,

    navigationOptions: {
      headerShown: false,
    },
  },
});

//!Home
const MainTabs = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" color={tintColor} size={24} />
        ),
        tabBarLabel: 'Noticias',
      }),
    },
    Study: {
      screen: StudyStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="book" color={tintColor} size={24} />
        ),
        tabBarLabel: 'Estudios',
      }),
    },
    Chat: {
      screen: ChatStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="whatsapp" color={tintColor} size={24} />
        ),
        tabBarOnPress: ({navigation, scene, jumpToIndex}) => {
          Linking.openURL(
            'https://api.whatsapp.com/send?phone=18298665388&text=Hola,%20mi%20consulta%20es...',
          );
        },

        tabBarLabel: 'Consulta',
      }),
    },
    Live: {
      screen: LiveStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="cloud" color={tintColor} size={24} />
        ),
        tabBarOnPress: ({navigation, scene, jumpToIndex}) => {
          Linking.openURL('http://belifebyca.com/');
        },
        tabBarLabel: 'Radio',
      }),
    },
    Account: {
      screen: AccountStack,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
          <Icon name="sliders" color={tintColor} size={24} />
        ),
        tabBarLabel: 'Cuenta',
      }),
    },
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      showLabel: true, // hide labels
      activeTintColor: '#5E9FB4', // active icon color
      inactiveTintColor: '#6D6D6D', // inactive icon color
      style: {
        backgroundColor: '#FFFFFF', // TabBar background
      },
    },
    //tabBarComponent: TabBarComponentLogout,
  },
);

const AppStack = createStackNavigator(
  {
    MainTabs: MainTabs,
  },
  {
    initialRouteName: 'MainTabs',
    headerMode: 'none',
  },
);

//
const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: {
      screen: AuthLoadingScreen,
    },
    App: AppStack,
    Auth: {
      screen: AuthStack,
      // screen: ScreenLogin,
    },
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
